import {LitElement, html, query} from 'lit-element';

class VersionElement extends LitElement {
    static get properties() {
        return {
            build: {type: String},  // 'dev', 'unstable', or 'stable'
            version: {type: String}, // semantic version
            git_hash: {type: String}, // git hash of latest commit
            branch: {type: String},  // current branch
        }
    }

    constructor() {
        super();
        this.build = window['totl_build'] || 'unknown';
        this.version = window['totl_version'];
        this.git_hash = window['totl_git_hash'];
        this.branch = window['totl_branch'];
    }

    gitinfo() {
        if(this.build == 'unknown' || this.build == 'stable') return '';
        let hash = this.git_hash.substring(0, 6);
        let branch = this.branch;
        return `-${branch}-${hash} `;
    }

    disclaimer() {
        if(this.build == 'unknown') {
            return html`<h1>THE BUILD IS BROKEN</h1>`;
        }
        if(this.build == 'dev') {
            return html`
<div>
    Thanks for contributing!
</div>
`;
        }
        if(this.build == 'unstable') {
            return html`
<div>
    Note that this version is <strong>highly unstable</strong> and may be outright unplayable.
    Your saves are not guaranteed to be forward compatible and are likely to break or permanently
    corrupt at any point, including while playing.
</div>
<div>
    <a href="https://atiaxi.itch.io/tyranny-of-the-labyrinth">Stable version</a> on itch.io 
</div>
`;
        }
        if(this.build == 'stable') {
            return html`
<div>
    <a href="https://gitlab.com/rostrander/tyranny-of-the-labyrinth">GitLab page</a>
</div>
            `
        }
        return html`<h1>UNSET BUILD</h1>`
    }

    render() {
        return html`
<div>
    Playing version ${this.version}-${this.build}${this.gitinfo()}
    ${this.disclaimer()}
</div>
        `;
    }
}

customElements.define('version-element', VersionElement);