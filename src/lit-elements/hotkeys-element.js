import {LitElement, html} from 'lit-element';
import {Duration} from "../backend/common";
import {Ability} from "../backend/abilities/ability";

class HotkeysElement extends LitElement {
    static get properties() {
        return {
            hotkeys: {type: Array}
        };
    }

    constructor() {
        super();
        this.hotkeys = [
            null,null,null,null,null,null,null,null,null,null,
        ];
    }

    render() {
        if(this.hotkeys.length === 0) {
            return html`(Awaiting gameplay)`;
        }

        return html`
<link href="https://stackpath.bootstrapcdn.com/bootswatch/4.1.1/cyborg/bootstrap.min.css" rel="stylesheet"
      integrity="sha384-iMvB1cubutqnCw/Xlf3A1lEXHojPMC7dETFR9CYfYENKn8yw6QlyL+BaYmPXEjmo" crossorigin="anonymous">

<div class="container">
    <div class="row">
        <div class="col">
            <h5>Hotkeys</h5>
        </div>
    </div>
    ${this.hotkeys.map(this.renderHotkey.bind(this))}
</div>
`;
    }

    renderHotkey(ability, index) {
        let abilityName = ability ? (ability.displayName || ability.name) : "";
        let cooldown = "";
        let abilitycls = "";
        if (ability) {
            if (!ability.isReady()) {
                if(ability.cooldown == Duration.Boss) {
                    cooldown = "(Boss)";
                } else {
                    cooldown = `(${ability.currentCooldown})`;
                }
                abilitycls = "text-secondary";
            } else {
                switch(ability.cooldown) {
                    case Duration.Boss:
                        abilitycls = "text-danger";
                        break;
                    case Duration.Encounter:
                        abilitycls = "text-warning";
                        break;
                    case Duration.Instant:
                        abilitycls = "text-success";
                        break;
                }
            }
        }

        return html`
            <div class="row">
                <div class="col-1">
                    ${index+1}
                </div>
                <div class="col-2">
                    ${cooldown}
                </div>
                <div class="col-7 ${abilitycls} text-left">
                    ${abilityName}
                </div>
            </div>
        `
    }

    update(changedProperties) {
        this.hotkeys = this.hotkeys.map((a) => {
            if(a) return new Ability(a)
            return null;
        });
        super.update(changedProperties);
    }
}

customElements.define('hotkeys-element', HotkeysElement);
