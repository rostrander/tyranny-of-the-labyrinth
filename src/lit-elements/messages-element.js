import {LitElement, html, query} from 'lit-element';

class MessagesElement extends LitElement {

    static get properties() {
        return {
            messages: {type: Array}
        };
    }

    constructor() {
        super();
        this.messages = [];
    }

    get content() {
        return this.shadowRoot.querySelector('#messages-content');
    }

    render() {
        return html`
<link href="https://stackpath.bootstrapcdn.com/bootswatch/4.1.1/cyborg/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-iMvB1cubutqnCw/Xlf3A1lEXHojPMC7dETFR9CYfYENKn8yw6QlyL+BaYmPXEjmo" crossorigin="anonymous">

<div id="messages" class="card w-50 mx-auto">
    <h5 class="card-header">
        Messages
    </h5>
    <div id="messages-content" style="height: 8rem; overflow-y: scroll;" class="card-body">
        ${this.messages.map(i => html`<p class="mb-0">${i}</p>`)}
    </div>
</div>
`;
    }

    update(changedProperties) {
        super.update(changedProperties);
        this.content.scrollTop = this.content.scrollHeight;
    }
}

customElements.define('messages-element', MessagesElement);
