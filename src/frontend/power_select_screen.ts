import * as ROT from 'rot-js';

import {State} from "./statemachine";
import {Character} from "../backend/character";
import {ABILITIES, Ability, IAbilityTemplate} from "../backend/abilities/ability";
import {Listable, ListComponent, makeRibbonInfo, RibbonComponent, SimpleWrapper} from "./components";
import {Action} from "./keybinds";
import {Duration, duration_to_color} from "../backend/common";

export class TierUpPowerSelectCoordinator extends State {
    protected activated: boolean;
    pending: IAbilityTemplate[];
    public ready: boolean;
    constructor(protected char: Character, public tier= 1, public allowNone=true) {
        super();
        this.activated = false;
        this.ready = false;
        this.pending = [];
    }

    async activate(prev: State) {
        super.activate(prev);
        if(this.activated) return;
        this.activated = true;

        let templates = this.tierAppropriateTemplates();

        let instants = this.selectableAbilities(templates, Duration.Instant);
        if(instants.length > 0) {
            let pow = await this.doScreen(instants, "Select an instant ability");
            if(!pow) return this.driver.pop();

            if(this.tier === 1) {
                instants = this.selectableAbilities(templates, Duration.Instant);
                pow = await this.doScreen(instants, "Select another instant ability");
                if (!pow) return this.driver.pop();
            }
        }

        let encounters = this.selectableAbilities(templates, Duration.Encounter);
        if(encounters.length > 0) {
            let pow = await this.doScreen(encounters, "Select an encounter power");
            if (!pow) return this.driver.pop();
        }

        let bosses = this.selectableAbilities(templates, Duration.Boss);
        if(bosses.length > 0) {
            let pow = await this.doScreen(bosses, "Select a boss power");
            if (!pow) return this.driver.pop();
        }
        this.ready = true;

        for(let template of this.pending) {
            this.char.gainAbilityFromTemplate(template);
        }
        this.driver.pop();
    }

    protected tierAppropriateTemplates(): IAbilityTemplate[] {
        return this.char.classAbilityNames.map((name) => {
            return ABILITIES.templateHolderFromName(name).template as IAbilityTemplate;
        }).filter((b) => {
            return (b.tier <= this.tier)
        });
    }

    protected async doScreen(templates:IAbilityTemplate[], caption:string) {
        let powerScreen = new PowerSelectScreen(templates, caption, this.allowNone);
        await this.driver.push(powerScreen);
        if(!powerScreen.ready) return null;
        if(powerScreen.selectedPower) {
            this.pending.push(powerScreen.selectedPower);
        }
        return powerScreen.ready;
    }

    protected selectableAbilities(templates: IAbilityTemplate[], cooldown: Duration) {
        return templates.filter((t) => {
            let cooldownMatch = (t.cooldown === cooldown ||
                (!t.cooldown && cooldown===Duration.Instant));
            let alreadyHave = this.char.abilityByName(t.name) ||
                this.pending.find(p => t.name === p.name);
            return cooldownMatch && !alreadyHave;
        });
    }
}

export class NoneItem extends SimpleWrapper {
    public static NAME = "None";

    public static DESCRIPTION = "Go forward without choosing a power. " +
        "WARNING: Choosing this option means you are giving up your power "+
        "choice!  You will not be able to change your mind later.";

    constructor() {
        // A hack, this is only a SimpleWrapper so it can easily be added to the list
        super(null, () => NoneItem.NAME);
        this.wrapped = null;
    }
}

export class AbilityTemplateWrapper extends SimpleWrapper {

    public static map(collection: IAbilityTemplate[]): AbilityTemplateWrapper[] {
        return collection.map(c => new AbilityTemplateWrapper(c));
    }

    constructor(item: IAbilityTemplate) {
        super(item, () => this.display());
    }

    public get fgcolor(): string {
        let cooldown = this.wrapped.cooldown || Duration.Instant;
        let result = duration_to_color(cooldown);
        return result;
    }

    protected display() {
        return this.wrapped.displayName || this.wrapped.name;
    }
}

// This is just the generic 'select a single power' screen,
// useful for picking new class powers, stolen powers from bosses, etc.
export class PowerSelectScreen extends State {

    protected powerList: ListComponent<SimpleWrapper>;
    protected ribbon: RibbonComponent;
    protected _selectedAsAbility: Ability;

    public ready: boolean;
    public selectedPower: IAbilityTemplate;

    constructor(protected choices: IAbilityTemplate[], protected caption?: string, allowNone = true) {
        super();
        this.ready = false;
        let loc = {x: 3, y: 3}
        let wrapped: SimpleWrapper[] = AbilityTemplateWrapper.map(choices);
        if(allowNone) wrapped.push(new NoneItem());
        this.powerList = new ListComponent<SimpleWrapper>(this, wrapped, loc)
        this.addComponent(this.powerList);

        this.ribbon = new RibbonComponent(this, [
            makeRibbonInfo(Action.Confirm, 'choose'),
            makeRibbonInfo(Action.Cancel, 'cancel'),
            makeRibbonInfo(Action.North, 'up'),
            makeRibbonInfo(Action.South, 'down'),
        ]);
        this.addComponent(this.ribbon);
    }

    public activate(prev: State) {
        super.activate(prev);
        this.ribbon.move({x: 0, y: this.driver.height - 1});
    }

    public handleInput(action: Action, ev: KeyboardEvent) {
        super.handleInput(action, ev);
        switch(action) {
            case Action.Confirm:
                this.ready = true;
                this.selectedPower = this.powerList.selectedItem.wrapped;
                this.driver.pop();
                break;
            case Action.Cancel:
                this.selectedPower = null;
                this.driver.pop();
                break;
        }
    }

    protected selectedAsAbility(template: IAbilityTemplate): Ability {
        if(!this._selectedAsAbility || this._selectedAsAbility.name !== template.name) {
            let item = this.powerList.selectedItem?.wrapped;
            if(item) {
                this._selectedAsAbility = new Ability(item);
            }
        }
        return this._selectedAsAbility;
    }

    render(display: ROT.Display) {
        super.render(display);

        display.drawText(5, 1, this.caption);
        let item = this.powerList.selectedItem?.wrapped;
        let description = "";
        if(item) {
            description = item.description;
        } else if(this.powerList.selectedItem) {
            description = NoneItem.DESCRIPTION;
        }
        let half = this.driver.width / 2;
        display.drawText(half, 3, description);
        if(item) {
            display.drawText(half, 6, this.selectedAsAbility(item).techDescription());
        }
    }
}
