import * as ROT from 'rot-js';
import {ListComponent, makeRibbonInfo, RibbonComponent, SimpleWrapper} from "./components";
import {IConditionTemplate} from "../backend/effects/condition";
import {State} from "./statemachine";
import {Action} from "./keybinds";

export class PassiveSelectScreen extends State {
    protected passiveList: ListComponent<SimpleWrapper>;
    protected ribbon: RibbonComponent;

    public ready: boolean;
    public selectedPassive: IConditionTemplate;

    constructor(protected choices: IConditionTemplate[]) {
        super();
        this.ready = false;
        let loc = {x: 3, y: 3}
        let wrapped = SimpleWrapper.map(choices, (t)=>t.displayName || t.name);
        this.passiveList = new ListComponent<SimpleWrapper>(this, wrapped, loc);
        this.addComponent(this.passiveList);

        this.ribbon = new RibbonComponent(this, [
            makeRibbonInfo(Action.Confirm, 'choose'),
            makeRibbonInfo(Action.Cancel, 'cancel'),
            makeRibbonInfo(Action.North, 'up'),
            makeRibbonInfo(Action.South, 'down'),
        ]);
        this.addComponent(this.ribbon);
    }

    public activate(prev: State) {
        super.activate(prev);
        this.ribbon.move({x: 0, y: this.driver.height - 1});
    }

    public handleInput(action: Action, ev: KeyboardEvent) {
        super.handleInput(action, ev);
        switch(action) {
            case Action.Confirm:
                this.ready = true;
                this.selectedPassive = this.passiveList.selectedItem.wrapped;
                this.driver.pop();
                break;
            case Action.Cancel:
                this.selectedPassive = null;
                this.driver.pop();
                break;
        }
    }

    render(display: ROT.Display) {
        super.render(display);

        display.drawText(5, 1, "Select a new passive ability");
        let item = this.passiveList.selectedItem?.wrapped;
        if(item) {
            let half = this.driver.width / 2;
            display.drawText(half, 3, item.description, half);
        }
    }
}
