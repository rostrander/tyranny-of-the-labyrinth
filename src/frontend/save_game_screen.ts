import * as ROT from 'rot-js';

import {State} from "./statemachine";
import {Campaign} from "../backend/campaign";
import {MapDrawer} from "./common";
import {Character} from "../backend/character";

export class SaveGameState extends State {
    constructor(protected campaign: Campaign,
                protected mapDraw: MapDrawer,
                protected player: Character,
    ) {
        super();
    }

    public render(display: ROT.Display) {
        this.mapDraw.drawEntireMap(display, this.player.loc);

        let saving = "[[[[[[-SAVING-]]]]]]";

        display.drawText(
            this.driver.width / 2 - saving.length / 2,
            this.driver.height / 2,
            saving
        );
    }

    public activate(prev: State) {
        super.activate(prev);
        window.requestAnimationFrame(() => {
            this.driver.refresh();
            this.frame(false);
        });
    }

    public frame(doSave: boolean) {
        if(doSave) {
            this.campaign.save();
            this.driver.pop();
            return;
        }
        // Basically ensuring we get a rendered frame before doing the extensive save
        window.requestAnimationFrame(() => {
            this.frame(true);
        });
    }

}