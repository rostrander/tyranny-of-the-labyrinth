import * as ROT from 'rot-js';

import {State} from "./statemachine";
import {ListComponent, TextWrapper} from "./components";
import {Action} from "./keybinds";
import {PlayingState} from "./playing";
import {DebugGameFactory, RegularGameFactory} from "../backend/newgame";
import {Campaign} from "../backend/campaign";
import {PointBuyState} from "./pointbuy_screen";
import {ClassSelectionScreen} from "./class_select_screen";
import {TierUpPowerSelectCoordinator} from "./power_select_screen";
import {TutorialScreen} from "./tutorial_screen";
import {PromptScreen} from "./prompt_screen";

export class TitleState extends State {
    protected menu: TextWrapper[];
    protected menuList: ListComponent<TextWrapper>;
    protected campaign: Campaign;
    protected maze: string[][];

    constructor() {
        super();
        this.campaign = new Campaign();
        this.menu = [];
        this.prepareMainMenu();
        this.menuList = new ListComponent<TextWrapper>(this, this.menu, {x: 2, y: 14});
        this.addComponent(this.menuList);
    }

    protected prepareMainMenu() {
        this.menu.splice(0,this.menu.length);
        let ng = new TextWrapper("New Game");
        let debug = new TextWrapper("New Game (debug)");
        let cont = new TextWrapper("Continue");
        if((window as any)['totl_build'] === 'dev') {
            this.menu.push(debug);
        }
        if(this.canContinue()) {
            this.menu.push(cont);
        }
        this.menu.push(
            ng,
        )
    }

    protected prepareMazeBg() {
        let w = this.driver.width;
        let h = this.driver.height;
        let maze = new ROT.Map.EllerMaze(w, h);
        this.maze = [];

        maze.create((x, y, v) => {
            let sigil = v ? "#" : ".";
            let row = this.maze[y];
            if(!row) this.maze[y] = [];

            if(y > 1 && y < 9) {
                let half = this.driver.width / 2;
                if(x > half - 10 && x < half + 10) {
                    this.maze[y][x] = '.'
                    return;
                }
            }

            if(y > 8 && y < this.driver.height - 1) {
                if(x > 0 && x < 19) {
                    this.maze[y][x] = '.'
                    return;
                }
            }

            if(y > 1 && 1 < this.driver.height && x == this.driver.width - 2) {
                // Maze generators seemingly want to have an odd width/height, and
                // default is 80x25, so we have to trim the double-wall the generator
                // creates
                let chosen = ROT.RNG.getUniform() < 0.6;
                sigil = chosen ? '.' : '#';
            }

            this.maze[y][x] = sigil;
        });
    }

    protected canContinue(): boolean {
        return !!this.campaign.playing;
    }

    public activate(prev: State) {
        super.activate(prev);
        this.prepareMainMenu();
        this.prepareMazeBg();
    }

    render(display: ROT.Display) {
        for(let y=0; y < this.driver.height; y++) {
            for(let x=0; x < this.driver.width; x++) {
                let sigil = this.maze[y][x];
                let fgcolor = sigil == "#" ? "#888" : "#444";
                display.draw(x, y, sigil, fgcolor, "#000");
            }
        }

        super.render(display);

        let tyrants = "T Y R A N N Y"
        this.drawCenter(
            display,
            `%c{#fa9848}${tyrants}%c{}`,
            3,
            tyrants
        );

        let ofthe = "of the"
        this.drawCenter(
            display,
            `%c{#888}${ofthe}%c{}`,
            5,
            ofthe
        );

        this.drawCenter(
            display,
            "L A B Y R I N T H",
            7
        );
    }

    public handleInput(action: Action, ev: KeyboardEvent) {
        super.handleInput(action, ev);
        if(action === Action.Confirm) {
            let selectedMenu = this.menuList.selectedItem;
            if (selectedMenu.displayName === "New Game (debug)") {
                this.newDebugGame();
            } else if(selectedMenu.displayName === 'New Game') {
                this.newGame();
            } else if(selectedMenu.displayName === "Continue") {
                this.continue();
            }
        }
    }

    protected continue() {
        let gm = this.campaign.loadGame();
        let playing = new PlayingState(this.campaign, gm);
        (window as any)['playing'] = playing;
        this.driver.push(playing);
    }

    protected newDebugGame() {
        let gm = DebugGameFactory.make();
        // for now we're just always using slot 0
        this.campaign.newGame(gm, true);
        let playing = new PlayingState(this.campaign, gm);
        (window as any)['playing'] = playing;
        this.driver.push(playing);
    }

    protected async newGame() {
        if(this.canContinue()) {
            let msg = "This will destroy your current playthrough!  Are you sure?";
            let doContinue = confirm(msg);
            if(!doContinue) return;
        }

        let prompt = new PromptScreen("Enter your character's name");
        await this.driver.push(prompt);
        let name = prompt.result;
        if(!name || name.length === 0) return;

        let classSelect = new ClassSelectionScreen(this.campaign);
        await this.driver.push(classSelect);
        if(!classSelect.ready) return;

        let character = classSelect.character;
        character.name = name;
        let gm = RegularGameFactory.make(character);

        if(gm.tutorialMode) {
            await this.driver.push(new TutorialScreen());
        }

        let powerCoord = new TierUpPowerSelectCoordinator(character, 1, !gm.tutorialMode);
        await this.driver.push(powerCoord);
        if(!powerCoord.ready) return;

        let nextScreen = new PointBuyState(gm.player);
        await this.driver.push(nextScreen);
        if(!nextScreen.ready) return;
        gm.player.heal(gm.player.maxHp);  // In case CON changed

        this.campaign.newGame(gm, true);
        let playing = new PlayingState(this.campaign, gm);
        (window as any)['playing'] = playing;  // For debug purposes
        this.driver.push(playing);
    }
}