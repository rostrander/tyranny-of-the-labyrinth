import * as ROT from 'rot-js';

import {State} from "./statemachine";
import {GameMaster} from "../backend/gamemaster";
import {Character} from "../backend/character";
import {PlayerController} from "../backend/control";
import {Action, actionToDirection} from "./keybinds";
import {GameOverState} from "./gameover";
import {InventoryState} from "./inventory";
import {CharacterState} from "./character_screen";
import {MapDrawer} from "./common";
import {Ability} from "../backend/abilities/ability";
import {TargetingState} from "./targeting_screen";
import {AbilitiesState} from "./ability_screen";
import {Campaign} from "../backend/campaign";
import {Faction, XY} from "../backend/interfaces";
import {Chain, Duration, Energy, Point, Targets} from "../backend/common";
import {Zone} from "../backend/world";
import {ChallengeScreen} from "./challenge_screen";
import {BossKillScreen} from "./boss_kill_screen";
import {TierUpPowerSelectCoordinator} from "./power_select_screen";
import {WinScreen} from "./win_screen";
import {BOSSES, CLASSES} from "../backend/classes";
import {SaveGameState} from "./save_game_screen";

export class PlayingState extends State implements MapDrawer {

    public player: Character;
    public ticksPerAutosave = Energy.TURN * 10;

    protected pc: PlayerController;
    protected cursorLoc: XY;
    protected targeting: TargetingState;

    private raf: () => void;
    private numMessages: number;

    private characterStatus: HTMLElement;

    constructor(public campaign: Campaign, public gm: GameMaster) {
        super();
        this.player = gm.player;
        this.pc = new PlayerController(this.player, this.gm);
        this.pc.onDeath(async () => {
            this.campaign.gameOver();
            await this.driver.push(new GameOverState());
            this.driver.pop();
        });
        this.pc.onDoor(() => {
            this.challengeBoss();
        });
        this.pc.onBossKill(() => {
            this.bossKill();
        });
        this.pc.onStairs(() => {
            this.stairs();
        });

        this.numMessages = 0;
        this.cursorLoc = null;

        // Start the loop!
        this.raf = () => {
            this.frame();
        };
        window.requestAnimationFrame(this.raf);
    }

    public activate(prev: State) {
        super.activate(prev);
        this.characterStatus = document.getElementById('character-status');
    }

    public render(display: ROT.Display) {
        this.drawEntireMap(display, this.player.loc);
    }

    public async challengeBoss() {
        let cs = new ChallengeScreen(this.pc, this.campaign);
        await this.driver.push(cs);
        let chosen = cs.chosen;
        if(chosen) {
            this.pc.challengeBoss(chosen);
        }
    }

    public async bossKill() {
        let defeatedName = this.pc.currentBoss;
        let tier = this.gm.tier;
        let tierNum = tier.tierNum;
        if(tier.wonGame()) {
            let clsName = BOSSES[defeatedName];
            let unlocked = this.campaign.unlockAndGameOver(clsName);
            return this.winScreen(defeatedName, unlocked);
        } else {
            let killScreen = new BossKillScreen(this.player, defeatedName, tierNum);
            await this.driver.push(killScreen);
            return this.saveGame();
        }
    }

    public async winScreen(defeatedName: string, unlock: string = null) {
        let winScreen = new WinScreen(defeatedName, unlock);
        await this.driver.push(winScreen);
        this.driver.pop();
    }

    public async stairs() {
        let nextTier = this.gm.tier.nextTier();
        let powerCoord = new TierUpPowerSelectCoordinator(this.player, nextTier.tierNum, !this.gm.tutorialMode);
        await this.driver.push(powerCoord);
        if(!powerCoord.ready) return;
        this.pc.nextTier();
        return this.saveGame();
    }

    public drawEntireMap(display: ROT.Display, center: XY) {
        let map = this.gm.map;
        for (let x = 0; x < map.width; x++) {
            for (let y = 0; y < map.height; y++) {
                this.drawTileAt(display, {x, y}, center);
            }
        }
        //display.drawText(1, 1, `Tick: ${this.gm.tickNum}`);
    }

    public mapCoordsToScreen(mapCoords: XY, center: XY): XY {
        let halfWidth = Math.floor(this.driver.width / 2);
        let halfHeight = Math.floor(this.driver.height / 2);

        let dx = mapCoords.x - center.x;
        let dy = mapCoords.y - center.y;
        return {
            x: halfWidth + dx,
            y: halfHeight + dy,
        };
    }

    public screenCoordsToMap(screenCoords: XY, center: XY): XY {
        let halfWidth = Math.floor(this.driver.width / 2);
        let halfHeight = Math.floor(this.driver.height / 2);
        let left = center.x - halfWidth;
        let top = center.y - halfHeight;

        return {
            x: left + screenCoords.x,
            y: top + screenCoords.y,
        }
    }

    public frame() {
        if(!this.driver.isAnActiveState(this)) return;
        this.updateExternalComponents();
        this.pc.tick();
        if(this.shouldSave()) this.saveGame();
        this.driver.refresh();
        window.requestAnimationFrame(this.raf);
    }

    protected shouldSave() {
        if(this.gm.waitingForPlayer()) return false;
        let ticks = this.gm.tickNum;
        if(ticks === 0) return false;
        return ticks % this.ticksPerAutosave === 0;
    }

    protected updateExternalComponents() {
        if(this.player.messages.length > this.numMessages) {
            let msgs = this.player.messages;
            let msgComponent = document.getElementById("messages-content");
            let msgsJson = JSON.stringify(msgs);
            msgComponent.setAttribute('messages', msgsJson);
            this.numMessages = this.player.messages.length;
        }

        let charJson = JSON.stringify(this.prepareStatusElement());
        this.characterStatus.setAttribute('character', charJson);
        let hotkeys = document.getElementById('hotkeys');
        let hotkeyJson = JSON.stringify(this.player.hotkeys);
        hotkeys.setAttribute('hotkeys', hotkeyJson);
    }

    protected prepareStatusElement() {
        let {
            name,
            level,
            className,
            hp,
            tempHp,
            maxHp,
            conditionString,
        } = this.player;
        let ac = this.player.calcDefense(Chain.ac).value;
        let fort = this.player.calcDefense(Chain.fort).value;
        let ref = this.player.calcDefense(Chain.ref).value;
        let will = this.player.calcDefense(Chain.will).value;

        return {
            name, level, className, hp, tempHp, maxHp, conditionString,
            ac, fort, ref, will,
        }
    }

    protected zoneColor(zone: Zone): string {
        if(!zone) return "#000";
        switch (zone.faction) {
            case Faction.Nobody:
                return "#400";
            case Faction.Player:
                return "#88F";
            case Faction.Enemy:
                return "#F22";
            default:
                return "#400";
        }
    }

    public tileAt(loc: XY) {
        return this.gm.map.tileAt(loc);
    }

    public drawTileAt(display: ROT.Display, tileLoc: XY, center: XY, bg="#000"): boolean {
        let m = this.mapCoordsToScreen(tileLoc, center);
        let map = this.gm.map;
        if (map.locationInBounds(tileLoc)) {
            let tile = map.tileAt(tileLoc);

            if(bg === "#000") {
                bg = this.zoneColor(tile.zone);
            }

            if(this.cursorLoc) {
                let {x:cx, y:cy} = this.cursorLoc
                if(cx === m.x && cy === m.y) {
                    bg = "#fff";
                }
            }

            let showTile = (tile.lit && this.player.canSee()) ||
                (tile.x == this.player.loc.x && tile.y == this.player.loc.y);

            if(showTile) {
                let color = bg === "#000" ? ROT.Color.toHex(tile.fgcolor) : "#000";
                display.draw(m.x, m.y, tile.displayedChar(),
                    color,
                    bg);
                return true;
            } else if(tile.viewed) {
                display.draw(m.x, m.y, tile.displayedChar(false),
                    "#222",
                    bg);
                return true;
            } else if(bg != "#000") {
                display.draw(m.x, m.y, " ", "#000", bg);
            }
            return false;
        }
    }

    mouseMove(loc: XY) {
        super.mouseMove(loc);
        if(loc.x < 0 || loc.y < 0) {
            this.cursorLoc = null;
        } else {
            this.cursorLoc = loc;
        }
    }

    mouseUp(loc: XY) {
        super.mouseUp(loc);
        let clicked = this.screenCoordsToMap(loc, this.player.loc);
        let tile = this.gm.map.tileAt(clicked);
        let player = this.gm.player;
        if(tile.viewed && tile.passable) {
            let dist = Point.distance(player.loc, tile);
            if(dist <= 1) {
                let dx = tile.x - player.loc.x;
                let dy = tile.y - player.loc.y;
                this.pc.bump({x: dx, y: dy});
            } else {
                let astar = new ROT.Path.AStar(
                    tile.x, tile.y,
                    this.gm.passableCallback(player.loc, true));
                let path: XY[] = [];
                let lastLoc = player.loc;
                astar.compute(player.loc.x, player.loc.y, (x, y) => {
                    if(player.loc.x != x || player.loc.y != y) {
                        let dx = x - lastLoc.x;
                        let dy = y - lastLoc.y;
                        path.push({x:dx,y:dy});
                        lastLoc = {x,y};
                    }
                });
                let firstMove = path.shift();
                this.pc.bump(firstMove, ...path);
            }
        }

    }

    handleInput(action: Action, ev: KeyboardEvent): void {
        // An exception to below, so we can cancel supermoves
        if(action === Action.StopMove) {
            this.pc.stopMovement();
        }

        // Even though the PlayerController enforces actions only taking place
        // on the player's turn, I want to do it here too so there's not a
        // weird state where e.g. they've opened up the inventory screen but
        // can't drop something because it's not their turn
        if(!this.gm.waitingForPlayer()) return;

        let dir = actionToDirection(action);
        if(dir) {
            let superMove = [];
            if(ev.shiftKey) {
                for(let i=0; i < 4; i++) {
                    superMove.push(dir);
                }
            }
            let moved = this.pc.bump(dir, ...superMove);
            if(moved) {
                return;
            }
        }
        switch(action) {
            case Action.SkipTurn:
                this.pc.wait();
                break;
            case Action.Pickup:
                this.pc.pickup();
                break;
            case Action.SaveGame:
                this.saveGame();
                break;
            case Action.Cancel:
                this.saveAndQuit();
                break;
            case Action.LoadGame:
                this.loadGame();
                break;
            // UI Actions
            case Action.InventoryScreen:
                this.driver.push(new InventoryState(this.pc));
                break;
            case Action.CharacterScreen:
                this.driver.push(new CharacterState(this.pc));
                break;
            case Action.AbilityScreen:
                this.openAbilityScreen();
                break;
            case Action.DefaultRangedScreen:
                this.invokePower(this.player.defaultRangedAbility);
                break;
            case Action.FarLooksScreen:
                this.target();
                break;
            case Action.Hotkey0:
            case Action.Hotkey1:
            case Action.Hotkey2:
            case Action.Hotkey3:
            case Action.Hotkey4:
            case Action.Hotkey5:
            case Action.Hotkey6:
            case Action.Hotkey7:
            case Action.Hotkey8:
            case Action.Hotkey9:
                let actionIndex = action-Action.Hotkey0;
                let ability = this.player.hotkeys[actionIndex];
                if(ability) {
                    this.invokePower(ability);
                }
                break;
        }
    }

    public invokePower(what: Ability) {
        if(what.needsExplicitTarget()) {
            this.target(what);
        } else {
            switch(what.targeting) {
                case Targets.Self:
                case Targets.None:
                    this.pc.selfAbility(what);
                    break;
                default:
                    this.pc.attack(what, this.player.loc);
            }
        }
    }

    public async target(what?: Ability) {
        this.targeting = this.targeting || new TargetingState(this.pc, what, this);
        this.targeting.setAbility(what);
        this.cursorLoc = null;  // So it won't be drawn alongside targeting cursor
        await this.driver.push(this.targeting);
        if(this.targeting.targetLoc) {
            if(what) {
                this.pc.attack(what, this.targeting.targetLoc);
            } else {
                let tile = this.tileAt(this.targeting.targetLoc);
                let contents = [];
                this.player.message("You see:");
                for(let item of tile.contents) {
                    contents.push(`A ${item.displayName}`);
                }
                if(tile.character) {
                    let display = tile.character.displayName || tile.character.name;
                    contents.push(`A ${display}`);
                }
                if(contents.length === 0) {
                    contents.push("Nothing of note");
                }
                this.player.message(...contents);
            }
        }
    }

    public async openAbilityScreen() {
        await this.driver.push(new AbilitiesState(this.player, this.gm));
    }

    protected async saveGame() {
        this.player.message("Saving...");
        await this.driver.push(
            new SaveGameState(this.campaign, this, this.player)
        );
    }

    protected async saveAndQuit() {
        await this.saveGame();
        this.driver.pop();
    }

    protected loadGame() {
        let newGm = this.campaign.loadGame();
        let newPlay = new PlayingState(this.campaign, newGm);
        this.driver.pop();
        this.driver.push(newPlay);
    }
}
