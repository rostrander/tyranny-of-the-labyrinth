import * as ROT from 'rot-js';
import {DisplayOptions} from "rot-js/lib/display/types";
import {Action, KeyBindings} from "./keybinds";
import {XY} from "../backend/interfaces";

export abstract class Component {
    public loc: XY;

    constructor(public parent: State, where?: XY) {
        this.loc = where || {x: 0, y: 0};
    }

    public abstract handleInput(action: Action, ev: KeyboardEvent): void;

    public abstract render(display: ROT.Display): void;

    public move(dest: XY): void {
        this.loc = {x: dest.x, y: dest.y};
    }
}

export abstract class State {
    public driver: StateDriver;

    protected components: Array<Component>;

    public name: string;

    constructor() {
        this.components = [];
        this.name = `State ${ROT.RNG.getUniformInt(10000,100000)}`;
    }

    public addComponent(component: Component) {
        this.components.push(component);
    }

    public handleInput(action: Action, ev: KeyboardEvent): void {
        for(let component of this.components) {
            component.handleInput(action, ev);
        }
    }

    public rawKey(ev: KeyboardEvent): void {

    }

    public render(display: ROT.Display): void {
        for(let component of this.components) {
            component.render(display);
        }
    }

    protected drawCenter(display: ROT.Display, msg: string, yloc: number, noformat?: string) {
        noformat = noformat ?? msg;
        let halfWidth = Math.floor(this.driver.width / 2);
        display.drawText(
            halfWidth - Math.floor(noformat.length / 2),
            yloc,
            msg
        );
    }

    public mouseDown(loc: XY) {
    }

    public mouseMove(loc: XY) {
    }

    public mouseUp(loc: XY) {
    }

    public activate(prev: State) {
    };

    public deactivate(nxt: State) {
    };
}

export interface StateSnapshot {
    state: State;
    resolveFunc: (value?: State | PromiseLike<State>) => void;
}

export class StateDriver {
    public display: ROT.Display;
    public state: State;

    public width: number;
    public height: number;

    protected states: Array<StateSnapshot>;
    public keybinds:KeyBindings;

    constructor(displayOptions?: DisplayOptions) {
        this.display = new ROT.Display(displayOptions);
        this.state = null;
        this.states = [];

        this.width = this.display.getOptions().width;
        this.height = this.display.getOptions().height;

        let container = document.getElementById("content");
        container.appendChild(this.display.getContainer());

        this.keybinds = KeyBindings.createDefaultBindings();

        // Bind keyboard input events
        this.bindEvent('keydown');
        this.bindEvent('keyup');
        this.bindEvent('keypress');

        // Mouse events
        this.bindMouseEvent('mousemove');
        this.bindMouseEvent('mouseup');
    }

    public isAnActiveState(state: State): boolean {
        for(let snapshot of this.states) {
            if(state === snapshot.state) return true;
        }
        return false;
    }

    public pop() {
        let snapshot = this.states.pop();
        let nextState = this.states[this.states.length - 1];
        if (nextState) {
            this.transitionToState(nextState.state, snapshot.state);
        } else {
            this.transitionToState(null, snapshot?.state);
        }
        snapshot.resolveFunc(snapshot.state);
    }

    public push<T extends State>(state: T): Promise<T> {
        state.driver = this;
        let resolver = null;
        let promise = new Promise<T>((resolvefunc) => {
            resolver = resolvefunc;
        });
        this.states.push({state: state, resolveFunc: resolver});
        this.transitionToState(state, this.state);
        return promise;
    }

    public refresh() {
        this.display.clear();
        if (this.state !== null) {
            this.state.render(this.display);
        }
    }

    protected bindEvent(event: string) {
        window.addEventListener(event, (e) => {
            // When an event is received, send it to the
            // screen if there is one
            if (this.state !== null && event === 'keyup') {
                // Look up
                let curState = this.state;
                let ev = <KeyboardEvent>e;
                curState.rawKey(ev);
                let actions = this.keybinds.keyToActions(ev.key);
                for(let action of actions) {
                    curState.handleInput(action, ev);
                }
                this.refresh();
            }
        });
    }

    protected bindMouseEvent(mouseEvent: string) {
        window.addEventListener(mouseEvent, (e) => {
            let me = e as MouseEvent;
            let [x, y] = this.display.eventToPosition(me);
            let loc = {x, y};
            switch(mouseEvent) {
                case 'mousedown':
                    if (x < 0 || y < 0) return;
                    this.state.mouseDown(loc);
                    break;
                case 'mouseup':
                    if (x < 0 || y < 0) return;
                    this.state.mouseUp(loc);
                    break;
                case 'mousemove':
                    this.state.mouseMove(loc);
                    break;
            }
        });
    }

    protected transitionToState(toState: State, fromState: State) {
        let prev = fromState;
        if (prev) {
            prev.deactivate(toState);
        }
        this.state = toState;
        if (this.state) {
            this.state.activate(fromState);
        }
        this.refresh();
    }


}
