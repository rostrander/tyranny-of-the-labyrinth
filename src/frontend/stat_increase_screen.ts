import * as ROT from 'rot-js';
import {Listable, ListComponent, makeRibbonInfo, RibbonComponent} from "./components";
import {Character} from "../backend/character";
import {State} from "./statemachine";
import {Action} from "./keybinds";
import {classesForStat, statToClasses, statToDesc} from "./pointbuy_screen";

class StatWrapper implements Listable {
    constructor(public statName: string, public char: Character) {
    }

    public get displayName(): string {
        let statName = this.statName;
        let stat = this.char.stats[statName];
        let statStr = stat < 10 ? ` ${stat}` : `${stat}`;
        let oneMore = stat < 9 ? ` ${stat+1}` : `${stat+1}`;
        let bonus = this.char.statBonus(statName);
        let bonusStr = bonus >= 0 ? `+${bonus}` : `${bonus}`;
        let nextBonus = this.char.statBonus(statName, stat + 1);
        let nextBonusStr = nextBonus >= 0 ? `+${nextBonus}` : `${nextBonus}`;
        return `${statName}: ${statStr} -> ${oneMore} (${bonusStr} -> ${nextBonusStr})`;
    }
}

export class StatIncreaseState extends State {
    protected statList: ListComponent<StatWrapper>;
    protected ribbon: RibbonComponent;

    public ready: boolean;
    public chosenStat: string;

    constructor(protected char: Character) {
        super();

        this.ready = false;
        this.chosenStat = null;
        let wrapped = [];
        for(let statName in this.char.stats) {
            wrapped.push(new StatWrapper(statName, this.char));
        }
        this.statList = new ListComponent<StatWrapper>(this, wrapped, {x: 2, y: 3});
        this.addComponent(this.statList);

        this.ribbon = new RibbonComponent(this, [
            makeRibbonInfo(Action.Confirm, 'choose'),
            makeRibbonInfo(Action.Cancel, 'cancel'),
            makeRibbonInfo(Action.North, 'up'),
            makeRibbonInfo(Action.South, 'down'),
        ]);
        this.addComponent(this.ribbon);
    }

    public activate(prev: State) {
        super.activate(prev);
        this.ribbon.move({x: 0, y: this.driver.height - 1});
    }

    public render(display: ROT.Display) {
        super.render(display);

        let wrapped = this.statList.selectedItem;

        display.drawText(1, 1, "Select the stat to increase by 1");

        let desc = statToDesc[wrapped.statName];
        display.drawText(2, 17, desc, 38);

        let prologue = statToClasses[wrapped.statName];
        display.drawText(41, 2, prologue, 38);
        display.drawText(41, 5, classesForStat(wrapped.statName));
    }

    public handleInput(action: Action, ev: KeyboardEvent) {
        super.handleInput(action, ev);
        switch (action) {
            case Action.Confirm:
                this.ready = true;
                this.chosenStat = this.statList.selectedItem.statName;
                this.driver.pop();
                break;
            case Action.Cancel:
                this.ready = false;
                this.driver.pop();
        }
    }

}