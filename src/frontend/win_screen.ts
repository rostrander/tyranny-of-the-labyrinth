import * as ROT from 'rot-js';
import {State} from "./statemachine";
import {Action} from "./keybinds";

export class WinScreen extends State {
    constructor(protected bossName: string, protected unlock: string = null) {
        super();
    }

    render(display: ROT.Display) {
        super.render(display);

        let txt = `You have defeated ${this.bossName}!`
        display.drawText(2, 1, txt);

        txt = "Their power broken, their ancient feud ended, the " +
            "Tyrants of the Labyrinth rule no more.  The nexus of energy "+
            "and potential that is this place now belongs solely to you.";
        display.drawText(2, 3, txt, this.driver.width - 4);

        txt = "You are now the only...";
        display.drawText(2, 10, txt);

        let tyrant = "T Y R A N T";
        this.drawCenter(
            display,
            `%c{#fa9848}${tyrant}%c{}`,
            12,
            tyrant,
        );

        let ofthe = "of the";
        this.drawCenter(
            display,
            `%c{#888}${ofthe}%c{}`,
            13,
            ofthe,
        );

        this.drawCenter(
            display,
            "L A B Y R I N T H",
            14,
        );

        if(this.unlock) {
            txt = "Of course, this is only one such place of power.  Many "+
                "more await!"
            display.drawText(2, 17, txt);
            txt = `[You have unlocked the ${this.unlock} class for future `+
                "playthroughs]"
            display.drawText(2, 18, txt);
        }

        let confirmKey = this.driver.keybinds.actionToFirstKey(Action.Confirm);
        txt = `Press ${confirmKey} to return to the title`;
        display.drawText(1, this.driver.height - 1, txt);
    }

    public handleInput(action: Action, ev: KeyboardEvent) {
        super.handleInput(action, ev);
        if(action == Action.Confirm || action == Action.Cancel) {
            this.driver.pop();
        }
    }
}