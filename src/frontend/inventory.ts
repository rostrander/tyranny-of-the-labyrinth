import * as ROT from 'rot-js';

import {State} from "./statemachine";
import {Character} from "../backend/character";
import {EquippableItem, Item} from "../backend/items/item";
import {Listable, ListComponent, makeRibbonInfo, RibbonComponent} from "./components";
import {Action} from "./keybinds";
import {PlayerController} from "../backend/control";
import {displayEquipmentTag, EquipmentTag} from "../backend/common";

class ItemWrapper implements Listable {
    constructor(public item: Item, protected char: Character) {
    }

    public get displayName(): string {
        let displayStr = this.item.displayName;
        if (this.item.equippable) {
            let proficient = this.char.isProficientWith(this.item as EquippableItem);
            if (!proficient) displayStr = `%c{#8c000f}${displayStr}%c{}`;  // crimson

            let equipped = this.char.isEquipped(this.item as EquippableItem);
            if (equipped) displayStr = `%c{#0485d1}${displayStr} (E)%c{}`; // cerulean
        }
        return displayStr;
    }
}

export class InventoryState extends State {

    public tookAction: boolean;

    protected inventoryList: ListComponent<ItemWrapper>;
    protected ribbon: RibbonComponent;
    protected char: Character;

    constructor(protected pc: PlayerController) {
        super();
        this.char = pc.player;
        let wrapped = this.char.inventory.map(i => new ItemWrapper(i, this.char));
        this.inventoryList = new ListComponent<ItemWrapper>(this, wrapped, {x: 1, y: 2});
        this.addComponent(this.inventoryList);
        this.ribbon = new RibbonComponent(this, [
            makeRibbonInfo(Action.Cancel, 'quit'),
            makeRibbonInfo(Action.Drop, 'drop'),
            makeRibbonInfo(Action.Use, 'use', true),
            makeRibbonInfo(Action.Equip, 'equip', true),
            makeRibbonInfo(Action.North, 'up'),
            makeRibbonInfo(Action.South, 'down'),
        ]);
        this.addComponent(this.ribbon);
        this.tookAction = false;
    }

    public activate(prev: State) {
        super.activate(prev);
        this.ribbon.move({x: 0, y: this.driver.height - 1});
    }

    public handleInput(action: Action, ev: KeyboardEvent): void {
        super.handleInput(action, ev);
        switch(action) {
            case Action.Drop:
                this.dropSelectedItem();
                break;
            case Action.Use:
                this.useSelectedItem();
                break;
            case Action.Equip:
                this.equipSelectedItem();
                break;
            case Action.Cancel:
                this.driver.pop();
        }
    }

    public render(display: ROT.Display) {
        display.drawText(0, 0, "You are carrying:");

        let item = this.inventoryList.selectedItem?.item;
        if(item) {
            this.ribbon.setEnabled(Action.Use, item.usable);
            let equippable = item.equippable && this.char.isProficientWith(item as EquippableItem);
            this.ribbon.setEnabled(Action.Equip, equippable);
        }

        super.render(display);

        if(item) {
            this.renderSelectedInfo(display);
        }
    }

    public renderSelectedInfo(display: ROT.Display) {
        let selectedItem = this.inventoryList.selectedItem?.item;
        if(!selectedItem) return;

        let halfWidth = this.driver.width / 2;
        display.drawText(halfWidth, 1, selectedItem.description(), halfWidth);
        if(selectedItem.equippable) {
            let equ = <EquippableItem>selectedItem;

            let y = 5;
            if(this.char.isProficientWith(equ)) {
                display.drawText(halfWidth, y, "(You are proficient with this item)");
            } else {
                let tags = equ.equipmentTags.map(t => displayEquipmentTag(t)).join(" or ");
                display.drawText(halfWidth, y,
                    `(Cannot equip, requires proficiency: ${tags})`,
                    halfWidth);
            }

            let height = Math.floor(this.driver.height / (equ.slots.length+1));
            y = height;
            for(let key of equ.slots) {
                display.drawText(halfWidth, y, `Equipped in ${key} slot:`);
                let equipped = this.char.equipment[key];
                display.drawText(halfWidth, y + 1, equipped.displayName);
                display.drawText(halfWidth, y+2, equipped.description(), halfWidth);
                y += height;
            }
        }
    }

    protected dropSelectedItem() {
        let item = this.inventoryList.selectedItem?.item;
        if(item) {
            this.pc.drop(item);
            // Can no longer ask the list to refresh itself, as it's wrappers
            // around items instead of the item list itself, and above has put
            // them out of sync.  Easiest to just pop out.
            this.driver.pop();
        }
    }

    protected useSelectedItem() {
        // For now, all items are just usable on the player
        let item = this.inventoryList.selectedItem?.item;
        if(!item || !item.usable) return false;
        let used = this.pc.use(item);
        if(used) {
            // Since this used an action, we're done here.
            this.driver.pop();
        }
    }

    protected equipSelectedItem() {
        let item = this.inventoryList.selectedItem?.item;
        if(!item || !item.equippable) return false;
        let equipped = this.pc.equip(item);
        if(equipped) {
            this.driver.pop();
        }
    }
}
