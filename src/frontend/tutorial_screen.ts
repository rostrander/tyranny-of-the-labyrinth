import * as ROT from 'rot-js';
import {State} from "./statemachine";
import {Action} from "./keybinds";

export class TutorialScreen extends State {

    render(display: ROT.Display) {
        super.render(display);

        let txt = "Welcome to Tyranny of the Labyrinth!";
        this.drawCenter(display,txt, 1);

        txt = "In this game, your choices will determine how the rest of the "+
            "dungeon plays out.  Specifically, every floor has a number of "+
            "challenge chambers, the doors to which are marked with a "+
            "%c{#ff0}+%c{}.  Upon entering, you will be able to choose and fight a "+
            "Tyrant.  Each one is as strong as you, so be prepared for "+
            "a challenge!\n\n"+
            "If you defeat the tyrant, you can gain some of its power, either "+
            "directly by taking one of its abilities or indirectly via "+
            "passive improvements to your character.  You will level up, "+
            "and if you've defeated half of the remaining tyrants you will "+
            "be able to move further down into the labyrinth.  You will gain "+
            "more powers, but the remaining monsters and tyrants will also "+
            "be that much more powerful.\n\n"+
            "Challenging and defeating the final Tyrant has two rewards. "+
            "First, naturally, you'll have won the game.  But secondly, the power of the "+
            "entire labyrinth will be yours:  You'll have unlocked that Tyrant's "+
            "class as a playable class for future runs!  So keep that in mind "+
            "when choosing what order to challenge your opponents in.\n\n"+
            "And finally, good luck!";
        display.drawText(2, 3, txt, this.driver.width - 4);

        let confirmKey = this.driver.keybinds.actionToFirstKey(Action.Confirm);
        txt = `Press ${confirmKey} to continue`;
        display.drawText(1, this.driver.height - 1, txt);
    }

    public handleInput(action: Action, ev: KeyboardEvent) {
        super.handleInput(action, ev);
        if(action == Action.Confirm || action == Action.Cancel) {
            this.driver.pop();
        }
    }
}