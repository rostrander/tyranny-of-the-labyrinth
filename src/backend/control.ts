import {GameMaster} from "./gamemaster";
import {Character, EquipmentSlotName} from "./character";
import {Item} from "./items/item";
import {Ability} from "./abilities/ability";
import {EventCallback, XY} from "./interfaces";

// courtesy https://www.logicbig.com/tutorials/misc/typescript/method-decorator.html
function playerturnOnly(target: any, propertyKey: string, descriptor: PropertyDescriptor) {
    let originalMethod = descriptor.value;
    //wrapping the original method
    descriptor.value = function (...args: any[]) {
        if (!this.gm.waitingForPlayer()) return false;
        let result = originalMethod.apply(this, args);
        return result;
    }
}

// This is the main reason I wanted to start over:
// The original MotL codebase had a lot of connections between the frontend
// and the backend, to the point where it was getting increasingly difficult
// to test.  While the frontend does know a lot about the backend, it's treating
// it as strictly read-only *except* for this class below, which abstracts
// out everything the player might want to do.
// Tests can now also create an instance of this and anything the player can do,
// tests can now do.
// We still won't be able to test UI stuff like e.g. listing items to quaff,
// but we will be able to test that the `quaff` backend functionality works.
export class PlayerController {

    // Callbacks for those rare occurrences where GM needs to
    // get the frontend's attention for something
    protected deathCallback: EventCallback

    constructor(public player: Character, public gm: GameMaster) {
        gm.onDeath(() => { if(this.deathCallback) this.deathCallback();});
    }

    //region Combat
    @playerturnOnly
    public bump(direction: XY, ...superMove: XY[]): boolean {
        return this.gm.bump(this.player, direction, ...superMove);
    }

    @playerturnOnly
    public attack(what: Ability, targetLoc?: XY): boolean {
        return this.gm.attack(this.player, what, targetLoc);
    }

    @playerturnOnly
    public selfAbility(what: Ability): boolean {
        return this.gm.selfAbility(this.player, what);
    }

    public canCenterAbility(what: Ability, where: XY): boolean {
        return this.gm.canCenterAbility(this.player, what, where);
    }

    @playerturnOnly
    public challengeBoss(bossName: string): boolean {
        return !!this.gm.challengeBoss(bossName);
    }

    public canChallengeBoss(bossName?: string): boolean {
        return this.gm.canChallengeBoss(bossName);
    }

    //endregion

    @playerturnOnly
    public nextTier() {
        return this.gm.nextTier();
    }

    //region Items
    @playerturnOnly
    public pickup(): boolean {
        return this.gm.pickup(this.player);
    }

    @playerturnOnly
    public drop(item: Item): boolean {
        return this.gm.drop(this.player, item);
    }

    @playerturnOnly
    public use(item: Item): boolean {
        return this.gm.useItem(this.player, item);
    }

    @playerturnOnly
    public equip(item: Item, slot?: EquipmentSlotName): boolean {
        return this.gm.equipItem(this.player, item, slot);
    }

    @playerturnOnly
    public unequip(slot: EquipmentSlotName): boolean {
        return this.gm.unequipItem(this.player, slot);
    }
    //endregion

    public tick(numTurns = 1): number {
        if (this.gm.waitingForPlayer()) return 0;
        return this.gm.tick(numTurns);
    }

    @playerturnOnly
    public wait(): boolean {
        return this.gm.wait(this.player);
    }

    public stopMovement() {
        this.gm.stopMovement();
    }

    public get currentBoss(): string {
        return this.gm.currentBoss;
    }

    // Callbacks
    public onDeath(cb: EventCallback): void {
        this.deathCallback = cb;
    }

    public onDoor(cb: EventCallback): void {
        this.gm.onDoor(cb);
    }

    public onBossKill(cb: EventCallback): void {
        this.gm.onBossKill(cb);
    }

    public onStairs(cb: EventCallback): void {
        this.gm.onStairs(cb);
    }

}
