

interface AddableResultEntry {
    amount: number;
    reason: string;
    longReason?: string;
    fail?: boolean;
}

export class AddableResult {
    public originalValue: number;
    public originalPass: boolean;
    public additions: AddableResultEntry[];
    public multipliers: AddableResultEntry[];
    public minimum: number;
    public maximum: number;

    constructor(original = 0) {
        this.originalValue = original;
        this.originalPass = true;
        this.additions = [];
        this.multipliers = [];
    }

    public get value(): number {
        // Why not just inline this?  Because then subclasses can't call `super`
        // See https://github.com/microsoft/TypeScript/issues/338
        let result =  this.totalValue();
        if(this.minimum !== undefined && result < this.minimum) return this.minimum;
        if(this.maximum !== undefined && result > this.maximum) return this.maximum;
        return result;
    }

    public get passes(): boolean {
        let result = this.originalPass;
        for(let added of this.additions) {
            result = result && (!added.fail);
        }
        return result;
    }

    protected totalValue(): number {
        let total = this.originalValue;
        for (let added of this.additions) {
            total += added.amount;
        }
        let mult = 1.0;
        for(let mults of this.multipliers) {
            mult += mults.amount;
        }
        if(mult < 0) mult = 0;
        total *= mult;
        return total;
    }

    public add(
        amount: number,
        reason: string,
        longReason = '',
        forceZeroAddition = false,
    ): this {
        if (amount === 0 && !forceZeroAddition) { return; }
        const addition = {
            amount,
            reason,
            longReason,
        };
        this.additions.push(addition);
        return this;
    }

    public addDice(
        dice: AddableResult,
        reason: string,
        longReason = '',
    ) {
        const addition = {
            amount: dice.value,
            reason,
            longReason,
        };
        this.additions.push(addition);
    }

    public addMultiplier(
        amount: number,
        reason: string,
        longReason = '',
    ): this {
        const addition = {
            amount,
            reason,
            longReason,
        };
        this.multipliers.push(addition);
        return this;
    }

    public addOrAdopt(
        amount: number,
        reason: string,
        longReason = '',
        forceZeroAddition = false,
    ): this {
        if(this.originalValue !== 0) return this.add(amount, reason, longReason, forceZeroAddition);
        this.originalValue = amount;
        return this;
    }

    public addBoolean(
        pass: boolean,
        reason: string,
        longReason = '',
    ): this {
        const addition = {
            amount: 0,
            reason,
            longReason,
            fail: !pass,
        }
        this.additions.push(addition);
        return this;
    }

    public fail(reason: string, longReason = '') {
        this.addBoolean(false, reason, longReason);
    }

    public explanation(): string {
        let prologue = `${this.originalValue}`
        let additionStr = '';
        let plusifier = (add: AddableResultEntry) => {
            if(add.amount >= 0) return `+${add.amount} <${add.reason}>`;
            return `${add.amount}`;
        }
        let additions = this.additions.map(plusifier);
        if(additions.length > 0) {
            additionStr = additions.join(" ");
        }
        let multStr = '';
        let multipliers = this.multipliers.map(plusifier);
        if(multipliers.length > 0) {
            let justAdds = multipliers.join(" ");
            multStr = `* (1 ${justAdds})`
        }
        return `${prologue} ${additionStr} ${multStr}`;
    }

}

export interface IDieRollTemplate {
    n?: number;  // default 1
    d: number;
    plus?: number; // default 0
}

export class Dice extends AddableResult implements IDieRollTemplate {
    public additions: AddableResultEntry[];

    public n: number;
    public d: number;
    public plus: number;

    public static get zero(): Dice {
        return Dice.fixed(0);
    }

    public static fixed(value: number): Dice {
        return new Dice().fixValue(value);
    }

    public static roll(n=1, d=20, plus=0): Dice {
        let result = new this({n, d, plus});
        return result.roll();
    }

    constructor(template?: IDieRollTemplate) {
        super(null);
        this.n = 1;
        this.d = 20;
        this.plus = 0;
        if(template !== undefined) {
            Object.assign(this, JSON.parse(JSON.stringify(template)));
        }
    }

    public get hasAdditions(): boolean {
        return this.additions.length > 0;
    }

    public description(): string {
        let plus = '';
        if(this.plus > 0) {
            plus = `+${this.plus}`;
        } else if(this.plus < 0) {
            plus = `${this.plus}`;
        }
        return `${this.n}d${this.d}${plus}`;
    }

    protected totalValue(): number {
        this.roll();
        return super.totalValue();
    }

    public fixValue(value: number): this {
        this.originalValue = value;
        return this;
    }

    public roll(): this {
        if (this.originalValue === null) {
            let total = 0;
            for (let i = 0; i < this.n; i++) {
                total += Math.floor(Math.random() * this.d) + 1;
            }
            this.originalValue = total + this.plus;
        }
        return this;
    }
}
