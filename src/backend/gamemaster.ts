import {Character, EquipmentSlotName} from "./character";
import {BoundingBox, Energy, exportJsonExcept, LastAction, Point, removeFromArray, Targets} from "./common";
import {BOSS_ROOM_TEMPLATE_CARVED, IMapTemplate, Tile, TileMap, Zone} from "./world";
import {Dice} from "./dice";
import {Ability} from "./abilities/ability";
import {EquippableItem, Item, NothingEquipment, UsableItem, Weapon} from "./items/item";
import {Spawner} from "./monsters/spawner";
import {EventCallback, Faction, ICharacterTemplate, SpawnTarget, TreasureTarget, XY} from "./interfaces";
import {Treasurer} from "./items/treasurer";
import * as ROT from "rot-js";

export interface ITierTemplate {
    tierNum: number;  // Importantly, this is 1-based
    bossesThisTier: number;
    topTier: number;  // The highest tier for this run
    bossesKilled?: number;
}

export class Tier implements ITierTemplate {
    tierNum: number;
    bossesThisTier: number;
    topTier: number;
    bossesKilled: number;

    constructor(template: ITierTemplate) {
        this.bossesKilled = 0;
        Object.assign(this, template);
    }

    bossKilled() {
        this.bossesKilled += 1;
    }

    nextTierReady(): boolean {
        return this.bossesKilled >= this.bossesThisTier;
    }

    oneBossLeft(): boolean {
        let thisTierLastBoss = this.bossesKilled === this.bossesThisTier - 1;
        let lastTier = this.tierNum === this.topTier;
        return thisTierLastBoss && lastTier;
    }

    wonGame(): boolean {
        return this.nextTierReady() && this.tierNum === this.topTier;
    }

    nextTier(): Tier {
        let template = Object.assign({}, this);
        template.tierNum = this.tierNum + 1;
        // Using `ciel` so that the tiers with 1 boss correctly beget tiers with 1 boss
        template.bossesThisTier = Math.ceil(this.bossesThisTier / 2);
        template.bossesKilled = 0;
        return new Tier(template);
    }
}

export interface IGameMasterTemplate {
    characters?: ICharacterTemplate[];

    tickNum?: number;
    locked?: number;
    currentBoss?: string;
    defeatedBosses?: string[];
    tutorialMode?: boolean;

    tier?: ITierTemplate;

    // Testing only
    skipRoomGeneration?: boolean

    // currentTurn and ready will be re-populated
    // fixedRolls is testing only
    // I don't care if queued moves don't get saved
    // wiring up the death callback is the frontend's problem
    player: ICharacterTemplate;
    map: IMapTemplate;
}

export class GameMaster implements IGameMasterTemplate, SpawnTarget, TreasureTarget {
    public characters: Array<Character>;
    public zones: Zone[];

    public tickNum: number;
    public locked: number;
    public currentBoss: string;
    public defeatedBosses: string[];
    public tutorialMode: boolean;

    public tier: Tier;

    public skipRoomGeneration: boolean;

    public currentTurn: Character;
    public ready: Character[];

    protected fixedRolls: number[];
    protected queuedMoves: XY[];

    // Callbacks for those rare occurrences where GM needs to
    // get the frontend's attention for something
    protected deathCallback: EventCallback;
    protected doorCallback: EventCallback;
    protected stairsCallback: EventCallback;
    protected bossCallback: EventCallback;

    public player: Character;
    public map: TileMap;

    // not persisted
    public spawner: Spawner;
    public treasurer: Treasurer;

    constructor(template: IGameMasterTemplate) {
        this.characters = [];
        this.zones = [];
        this.tickNum = 0;
        this.fixedRolls = [];
        this.queuedMoves = [];
        this.defeatedBosses = [];
        this.tier = new Tier({
            tierNum: 1,
            bossesThisTier: 1,
            topTier: 2,
        });

        Object.assign(this, template);
        this.setup();

        if(this.characters.length > 0) {
            this.characters = [];
            for(let char of template.characters) {
                this.addCharacter(new Character(char));
            }
        }

        this.finalSetup();
        this.autoRealize();
    }

    public get running(): boolean {
        return this.player.alive;
    }

    public get spawnCount(): number {
        return Math.max(this.characters.length - 1, 0);
    }

    public toJSON(): IGameMasterTemplate {
        let result = exportJsonExcept(this,
            'characters',
            'spawner',
            'treasurer',
        );
        result.characters = this.characters.filter((c) => c!==this.player);
        return result;
    }

    public autoRealize() {
        this.zones = this.zones.map((z) => new Zone(z));
        for(let zone of this.zones) {
            this.postAddZone(zone);
        }
        this.tier = new Tier(this.tier);
    }

    public onDeath(cb: EventCallback) {
        this.deathCallback = cb;
    }

    public onDoor(cb: EventCallback) {
        this.doorCallback = cb;
    }

    public onStairs(cb: EventCallback) {
        this.stairsCallback = cb;
    }

    public onBossKill(cb: EventCallback) {
        this.bossCallback = cb;
    }

    public broadcast(src: Character, ...msgs: string[]): void {
        for(let char of this.characters) {
            if(char !== src) {
                char.message(...msgs);
            }
        }
    }

    public closestEnemyTo(src: Character): Character {
        let chosen = null;
        let closestDist = 99999;
        for(let char of this.characters) {
            if(char.faction !== src.faction &&
                this.map.tileAt(char.loc).lit
            ) {
                let dist = Point.distance(char.loc, src.loc);
                if(dist < closestDist) {
                    closestDist = dist;
                    chosen = char;
                }
            }
        }
        return chosen;
    }

    public waitingForPlayer() {
        if (!this.player.alive) return false;  // Not waiting on the dead
        return this.locked > 0 && this.currentTurn === this.player;
    }

    public addCharacter(who: Character) {
        if(!(who instanceof Character)) {
            who = new Character(who);
        }
        if(who.behavior) {
            // gotta check because the player doesn't have a behavior
            who.behavior.gm = this;
        }
        this.characters.push(who);
        this.map.tileAt(who.loc).character = who;
        who.map = this.map;
    }

    public addZone(what: Zone) {
        this.zones.push(what);
        this.postAddZone(what);
    }

    public postAddZone(what: Zone) {
        for(let tile of this.tilesInBurst(what.center, what.radius)) {
            tile.zone = what;
        }
    }

    public removeZone(what: Zone) {
        removeFromArray(what, this.zones);

        for(let tile of this.tilesInBurst(what.center, what.radius)) {
            tile.zone = null;
        }
    }

    public canMoveInto(destination: XY, swapFaction=Faction.Nobody): boolean {
        let occupied = this.map.blockingTileAt(destination, swapFaction);
        return !occupied;
    }

    public passableCallback(ignoreLocation: XY, ignoreChars = false):(x:number, y:number) => boolean {
        return (x: number,y: number) => {
            if (x == ignoreLocation.x && y == ignoreLocation.y) return true;
            let faction = ignoreChars ? Faction.Everybody : Faction.Nobody;
            return this.canMoveInto({x, y}, faction);
        };
    }

    public lock(): number {
        this.locked += 1;
        return this.locked;
    }

    public unlock(): number {
        this.locked = Math.max(0, this.locked - 1);
        return this.locked;
    }

    protected unlockAll(): number {
        this.locked = 0;
        return this.locked;
    }

    protected moveCharacterAndEndTurn(who: Character, destination: XY): boolean {
        if(!who.canMove()) {
            // Struggling against immobile ends turn
            this.endTurn(who, Energy.MOVE);
            return true;
        }

        if (!this.moveCharacter(who, destination)) return false;
        this.endTurn(who, Energy.MOVE);

        return true;
    }

    public moveCharacter(who: Character, destination: XY): boolean {
        if(!this.canMoveInto(destination, who.faction)) {
            return false;
        }
        this.forceMoveCharacter(who, destination);
        return true;
    }

    public forceMoveCharacter(who: Character, destination: XY) {
        let swappedWith = this.map.moveCharacter(who, destination);
        if(swappedWith) {
            swappedWith.loc.x = who.loc.x;
            swappedWith.loc.y = who.loc.y;
        }
        who.loc.x = destination.x;
        who.loc.y = destination.y;
    }

    public damageCharacter(fromWho: Character, toWho: Character, amount: Dice) {
        let actualDmg = toWho.takeDamage(amount, fromWho);
        let dispFrom = fromWho.displayName || fromWho.name;
        let dispTo = toWho.displayName || toWho.name;
        let broadcastStr = `${dispFrom} does ${actualDmg.value} damage to ${dispTo}.`;
        // See https://gitlab.com/rostrander/tyranny-of-the-labyrinth/-/issues/51
        // ongoing damage caused double-messages as 'fromWho' and 'toWho' were the same.
        if(fromWho !== toWho) {
            fromWho.message(`You do ${actualDmg.value} (${actualDmg.explanation()}) damage.`);
        }
        toWho.message(`You take ${actualDmg.value} (${actualDmg.explanation()}) damage.`);
        fromWho.broadcast(this, broadcastStr);
        toWho.broadcast(this, broadcastStr);

        if(!toWho.alive) {
            let broadcastStr = `${toWho.name} dies!`;
            fromWho.message(`You have slain the ${dispTo}!`);
            toWho.message("You die!");
            fromWho.broadcast(this, broadcastStr);
            toWho.broadcast(this, broadcastStr);
            this.characterDied(toWho);
        }
    }

    public characterDied(who: Character): void {
        who.youDied(this);
        this.map.tileAt(who.loc).character = null;
        removeFromArray(who, this.characters);
        this.treasurer.creatureDied(who);

        if(who !== this.player && who.isMaster) {
            this.bossDied(who);
        }
    }

    protected bossDied(who: Character): void {
        this.player.defeatedBoss();
        this.defeatedBosses.push(who.name);
        this.tier.bossKilled();
        // It's the callback's responsibility to check for winning
        this.doCallback(this.bossCallback);
        this.currentBoss = null;

        if(this.tier.nextTierReady()) {
            // We don't go up a tier right now because that'd affect `bossCallback`,
            // Instead we'll do it when the player goes down the stairs
            let grave = this.map.tileAt(who.loc);
            grave.char = '>';
            grave.passable = false;
        }
    }

    public endTurn(who: Character, expendEnergy: number) {
        who.endOfTurn(this);
        who.expend(expendEnergy);
        if(who === this.player) {
            this.recalculateLighting();
            if(this.locked) this.unlock();
        }
        this.currentTurn = null;
    }

    public roll(dice?: Dice): Dice {
        dice = dice ?? new Dice();
        if(this.fixedRolls.length > 0) {
            let value = this.fixedRolls.shift();
            dice.fixValue(value);
        }
        return dice.roll();
    }

    public tickWithoutAct(numTicks = 1): void {
        this.tickNum += numTicks;
        for(let character of this.characters) {
            character.tick(numTicks);
            if(character.isReady()) {
                this.ready.push(character);
            }
        }
    }

    public act(): void {
        if(this.ready.length === 0) return;
        let current = this.ready.pop();
        this.currentTurn = current;
        current.beginningOfTurn(this);

        if(!current.canAct()) {
            this.endTurn(current, Energy.MINOR);
            return;
        }

        if(current === this.player) {
            if(this.queuedMoves.length > 0) {
                if(!this.playerCanSeeDanger()) {
                    let moved = this.bump(this.player, this.queuedMoves.shift());
                    if (moved) return;
                }
                // If we couldn't move, or if we saw danger, wipe out the queue
                this.queuedMoves = [];
            }
            this.lock();
            return;
        }

        current.behavior.act();
    }

    public attemptInvokeAbility(char: Character, what: Ability, where: XY): boolean {
        if(!this.running) return false;
        if(!this.canCenterAbility(char, what, where)) return false;

        const result = what.invokeAt(char, where, this);
        if(result) {
            if(result.shouldExpend) what.startCooldown();
            if(result.shouldEndTurn) this.endTurn(char, what.cost);
        }
        return !!result;
    }

    public *tilesInBurst(where: XY, radius: number): IterableIterator<Tile> {
        for(let dx=-radius; dx <= radius; dx++) {
            for(let dy=-radius; dy <= radius; dy++) {
                let there = {x: where.x + dx, y: where.y + dy};
                if(this.map.locationInBounds(there)) {
                    yield this.map.tileAt(there);
                }
            }
        }
    }

    public carveBossRoom(near: XY): BoundingBox {
        let room = this.map.findRoomNear(near);
        if(!room) return null;
        let bb = BoundingBox.fromXYWH(room).inset(2);
        this.map.paintRoom(bb, BOSS_ROOM_TEMPLATE_CARVED);

        if(this.player) {
            this.recalculateLighting(this.player.loc)
        }
        return bb;
    }

    //region Items
    public placeItemAt(what: Item, where: XY): boolean {
        return this.map.placeItem(what, where);
    }
    //endregion

    //region PlayerController destinations
    public bump(who: Character, dir: XY, ...superMove: XY[]): boolean {
        if(!this.running) return false;
        let result = false;

        let proposed = Point.add(who.loc, dir);

        let tile = this.map.tileAt(proposed);
        if(tile.character && tile.character.faction !== who.faction) {
            result = this.attack(who, who.defaultMeleeAbility, proposed);
        } else if(tile.isDoorTile()) {
            if (this.canChallengeBoss()) {
                this.doCallback(this.doorCallback);
            } else {
                let msg = `You must first defeat ${this.currentBoss} before challenging another`;
                this.player.message(msg);
            }
        } else if(tile.isStairTile()) {
            this.doCallback(this.stairsCallback);
        } else {
            result = this.moveCharacterAndEndTurn(who, proposed);
            if(result) who.recordAction(LastAction.Moved);
        }
        if(result && superMove.length > 0) {
            if(who !== this.player) throw "Only player can supermove, this is a bug";
            this.queuedMoves.push(...superMove);
        }
        return result;
    }

    public stopMovement() {
        this.queuedMoves = [];
    }

    protected swapCharPlaces(fromWho: Character, toWho: Character): boolean {
        return true;
    }

    public attack(who: Character, what: Ability, targetLoc?: XY): boolean {
        if(!this.running) return false;
        if(!what.isReady()) {
            let display = what.displayName || what.name;
            who.message(`${display} is not ready!`);
            return false;
        }
        targetLoc = targetLoc ?? who.loc;

        return this.attemptInvokeAbility(who, what, targetLoc);
    }

    public selfAbility(who: Character, what: Ability): boolean {
        if(!what.isReady()) {
            let display = what.displayName || what.name;
            who.message(`${display} is not ready!`);
            return false;
        }
        return this.attemptInvokeAbility(who, what, who.loc);
    }

    public canCenterAbility(who: Character, what: Ability, where: XY): boolean {
        let tile = this.map.tileAt(where);
        if(!tile.lit) return false; // for now

        let srcTile = this.map.tileAt(who.loc);
        if(!srcTile.lit) return false;

        let dist = Point.fromLoc(srcTile).distance(where);
        if(dist > what.range) return false;

        if(!tile.passable) return false;

        let dangerClose = Point.fromLoc(who.loc).sameAs(where);
        if(dangerClose && !this.canSelfCenter(what)) return false;

        if(!tile.character && this.needsCharacterCenter(what)) return false;

        if(what.targeting === Targets.Move && tile.character) return false;

        if(what.targeting !== Targets.Self &&
            what.targeting !== Targets.None &&
            what.targeting !== Targets.PBAoE &&
            !what.friendlyFire &&
            tile.character &&
            tile.character.faction === who.faction) return false;

        return true;
    }

    public canTarget(fromWho: Character, what: Ability, toWho: Character): boolean {
        if(!toWho) return false;
        if(fromWho.faction === toWho.faction) {
            return what.targeting === Targets.Self || what.friendlyFire;
        }
        switch (what.targeting) {
            case Targets.None:
            case Targets.Self:
                return false;
            default:
                return true;
        }
    }

    protected canSelfCenter(what: Ability) {
        switch(what.targeting) {
            case Targets.Self:
            case Targets.PBAoE:
            case Targets.TAoE:
            case Targets.None:
                return true;
            default:
                return false;
        }
    }

    protected needsCharacterCenter(what: Ability) {
        switch (what.targeting) {
            case Targets.Ranged:
            case Targets.Melee:
            case Targets.PBAoE:
            case Targets.Self:
                return true;
            default:
                return false;
        }
    }

    // If bossName isn't provided, this is generic "Can I challenge a boss?"
    // i.e. should the door callback be called
    public canChallengeBoss(bossName?: string): boolean {
        if(this.currentBoss) return false;
        if(!bossName) return true;

        if(this.defeatedBosses.includes(bossName)) return false;

        if(this.tutorialMode) {
            switch (bossName) {
                case "Fighter Moxie":
                case "Wizard Zankar":
                    return true;
                default:
                    return false;
            }
        }

        return true;
    }

    public challengeBoss(bossName: string): Character {
        let bb = this.carveBossRoom(this.player.loc);
        let bossLoc = bb.center;
        let boss = this.spawner.createBoss(bossName);
        this.spawner.spawnMonsterAt(boss, bossLoc);
        this.currentBoss = bossName;
        return boss;
    }

    public nextTier() {
        this.tier = this.tier.nextTier();
        this.treasurer.adoptNextTier();
        this.spawner.adoptNextTier();

        this.reset();
    }

    public pickup(who: Character): boolean {
        let picked = this.map.takeItems(who.loc);
        if(picked.length === 0) return false;
        for(let i=0; i < picked.length; i++) {
            who.receiveItem(picked[i]);
            who.message(`You pick up a ${picked[i].displayName}`);
        }
        // For now, picking up 1000 things is exactly as costly as picking up one thing.
        this.endTurn(who, Energy.MOVE);
        return true;
    }

    public drop(who: Character, what: Item): boolean {
        let where = this.map.tileAt(who.loc);
        if(!where.passable) return false;

        let removed = who.removeItem(what);
        if(!removed) return false;

        for(let key in who.equipment) {
            let equip = who.equipment[key];
            if(equip === what) {
                who.equipment[key] = new NothingEquipment();
            }
        }

        return this.map.placeItem(what, who.loc);
    }

    public useItem(who: Character, what: Item): boolean {
        if(!what.usable) return false;
        if(!who.hasItem(what)) return false;

        const item = what as UsableItem;
        // Currently the only way to use items is on yourself:
        const result = item.invoke(who, who, this);
        if (result) {
            // And currently they're all consumable
            who.removeItem(what);
            this.endTurn(who, Energy.MINOR);
        }
        return result;
    }

    public equipItem(who: Character, what: Item, where?: EquipmentSlotName): boolean {
        if(!what.equippable) return false;
        if(!who.hasItem(what)) return false;

        const item = what as EquippableItem;
        if (where === undefined) {
            where = item.slots[0];
        }
        if(!item.slots.includes(where)) return false;
        if(!who.isProficientWith(item)) return false;

        if(where === "offHand") {
            let main = who.equipment["mainHand"] as Weapon;
            if(!main.isNothing() && main.isTwoHanded) return false;
        } else if(where === "mainHand") {
            let main = what as Weapon;
            if(main.isTwoHanded) this.unequipItem(who, "offHand");
        } else if(where === "ring1") {
            if(!who.equipment.ring1.isNothing() && what !== who.equipment.ring1) {
                if(who.equipment.ring2) {
                    this.unequipItem(who,'ring2');
                }
                who.equipment.ring2 = who.equipment.ring1
            }
        }

        who.equipment[where] = item;
        this.endTurn(who, Energy.MOVE);
        who.message(`You equip the ${what.displayName}`);
        return true;
    }

    public unequipItem(who: Character, where: EquipmentSlotName): boolean {
        let what = who.equipment[where];
        if(!what.isUnequppable()) return false;

        who.equipment[where] = new NothingEquipment();
        who.message(`You unequip the ${what.displayName}`);
        return true;
    }

    public processReadyQueue() {
        while(this.ready.length > 0) {
            if(this.locked > 0) return;
            this.act();
        }
    }

    public tick(numTurns = 1): number {
        let ticks = 0;
        if(this.running) {
            // If the player acted, there may be others who didn't get to act;
            // go through all of those now
            this.processReadyQueue();
            for (let i = 0; i < numTurns; i++) {
                ticks += 1;
                this.tickWithoutAct();
                this.processReadyQueue();
                this.spawner.tick();
            }

            // Zones
            let expired = this.zones.filter((zone) => {
                zone.ttl -= 1;
                return zone.ttl <= 0;
            });
            for(let zone of expired) {
                this.removeZone(zone);
            }
        } else {
            this.doCallback(this.deathCallback);
            this.deathCallback = null;
        }
        return ticks;
    }

    public doCallback(cb: EventCallback) {
        if(cb) cb();
    }

    public wait(who: Character): boolean {
        this.endTurn(who, Energy.MINOR);
        return true;
    }

    // endregion

    protected playerCanSeeDanger(): boolean {
        for(let char of this.characters) {
            if(char === this.player) continue;
            let tile = this.map.tileAt(char.loc);
            if(tile.lit) return true;
        }
        return false;
    }

    protected reset() {
        this.characters = [];
        this.zones = [];
        this.map = null;  // So it'll be created
        this.player.loc = null;  // So it'll be placed somewhere else
        this.setup();
        this.finalSetup();
        this.player.heal(this.player.maxHp);
    }

    protected setup() {
        // A bit of a hack; if the game is saved on an enemy's turn,
        // this array will have templates instead of the real thing
        // when restored.  Blanking it means it'll be rebuilt next tick.
        this.ready = [];

        // TODO: create a map if none given (for tier up)
        if(!this.map) {
            // This means it's a tier-up and we should make the map
            let map = new TileMap(100, 100);
            let mapGen = new ROT.Map.Digger(100, 100)
            mapGen.create(map.rotjsMapCallback());
            this.map = map;
        }
        if(!(this.map instanceof TileMap)) {
            this.map = TileMap.fromJson(this.map);
        }

        // If this is a new game / tier, we'll need to carve out
        // rooms on this floor for the boss fights:
        if(this.map.placedRooms === 0 && !this.skipRoomGeneration) {
            let placedrooms = this.map.placeRooms(this.tier.bossesThisTier);
            if(placedrooms !== this.tier.bossesThisTier) {
                // TODO: Deal with invalid layouts like this one
                //       For now, though:
                let msg = `Need ${this.tier.bossesThisTier} rooms but only placed ${placedrooms}`;
                console.error(msg);
                console.log(this.map.debugPrint());
                alert(msg)
            }
        }

        if(!(this.player instanceof Character)) {
            this.player = new Character(this.player);
        } else if (!this.player.loc) {
            // It's already an instance, meaning this is a new game or tier
            let loc = this.map.randomSpawnableTile();
            this.player.loc = loc;
        }

        // To make testing easier (and so players don't get instantly murdered by
        // poor spawn RNG) the player always goes first.
        this.locked = 1;
        this.currentTurn = this.player;

        this.spawner = new Spawner(this, this.tier.tierNum);
        this.spawner.tickNum = this.tickNum;
        this.treasurer = new Treasurer(this, this.tier.tierNum);
    }

    protected finalSetup() {
        this.addCharacter(this.player);
        this.recalculateLighting();
    }

    /* Lighting */

    public recalculateLighting(forWhere: XY = null, radius = -1): void {
        if(forWhere === null) {
            forWhere = this.player.loc;
        }
        if(radius == -1) {
            radius = this.player.lightRadius;
        }
        this.map.recalculateLightingAt(forWhere, radius);
    }

    /* Testing only */

    public fixRolls(...rolls: number[]) {
        this.fixedRolls.push(...rolls);
    }
}
