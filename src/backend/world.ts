import * as ROT from 'rot-js';
import {Color} from "rot-js/lib/color";


import {BoundingBox, Chain, Energy, Point} from "./common";
import {Character} from "./character";
import {Item} from "./items/item";
import {Faction, ITileTemplate, IZoneTemplate, SpawnQueryable, XY, XYWH} from "./interfaces";
import {PassiveEffect} from "./effects/effect";
import {PASSIVEEFFECTS} from "./effects/passive";
import {AddableResult} from "./dice";

export const BOSS_ROOM_TEMPLATE_CARVED = [
    "##### #####",
    "#         #",
    "# #     # #",
    "           ",
    "# #     # #",
    "#         #",
    "##### #####",
]
export const BOSS_ROOM_TEMPLATE_FILLED = [
    "#####+#####",
    "###########",
    "###########",
    "+#########+",
    "###########",
    "###########",
    "#####+#####",
]
type RoomTemplate = typeof BOSS_ROOM_TEMPLATE_CARVED;

export class Tile extends Point implements ITileTemplate {
    /* Fields that constructor creates:
    public char: string
    public location: XY
    public passable: boolean
    */

    static readonly NULL = new Tile('X', {x: 0, y: 0}, false);

    public character: Character;
    public zone: Zone;  // zones overwrite, so only one
    public lit: number;
    public viewed: boolean;

    public contents: Array<Item>;

    static rotjsValueToTile(value: number, where: XY): Tile {
        switch(value) {
            case 0:
                return new Tile('.', where, true);
            case 1:
                return new Tile('#', where, false);
            default:
                return this.NULL;
        }
    }

    static glyphToTile(glyph: string, where: XY): Tile {
        let glyphIsPassable = false;
        switch(glyph) {
            case '.':
                glyphIsPassable = true;
                break;
            case ' ':
                // Blank space is not a valid glyph, but it makes typing in
                // the rooms manually a lot easier.
                glyphIsPassable = true;
                glyph = '.';
                break;
            default:
                glyphIsPassable = false;
        }
        return new Tile(glyph, where, glyphIsPassable);
    }

    static fromJson(jsonnable: ITileTemplate) {
        let {char, x, y, passable, lit, viewed, contents} = jsonnable
        let result = new Tile(
            char,
            new Point(x, y),
            passable
        );
        result.lit = lit;
        result.viewed = viewed;
        result.contents = jsonnable.contents.map((t) => Item.fromTemplate(t));
        return result;
    }

    constructor(public char: string, location: XY, public passable = true) {
        super(location.x, location.y);
        this.lit = 0;
        this.viewed = false;
        this.char = char;
        this.contents = [];
        this.character = null;
    }

    public isNullTile(): boolean {
        return this.char === "X";
    }

    public isDoorTile(): boolean {
        return this.char === "+";
    }

    public isStairTile(): boolean {
        return this.char === ">";
    }

    public toJSON(): ITileTemplate {
        let {char, x, y, passable, lit, viewed, contents} = this;
        return {
            char,
            x,
            y,
            passable,
            lit,
            viewed,
            contents,
        }
    }

    displayedChar(includeCharacter = true): string {
        if(this.character && includeCharacter) return this.character.char;
        if(this.contents.length > 0) {
            return this.contents[this.contents.length-1].char;
        }
        return this.char;
    }

    public get fgcolor(): Color {
        let targetColor: Color = [255, 255, 255];
        if(this.character) {
            targetColor = this.character.fgcolor || targetColor;
        } else if (this.contents.length === 0) {
            switch (this.char) {
                case ".":
                    targetColor = [128, 128, 128];
                    break;
                case '+':
                    targetColor = [255, 255, 0];
                    break;
                case '>':
                    targetColor = ROT.Color.fromString("#f8481c"); // reddish orange
                    break;
                case '#':
                    targetColor = ROT.Color.fromString("#985e2b"); // Sepia
                    break;
            }

        }
        return ROT.Color.interpolate([32, 32, 32], targetColor, this.lit)
    }

    receive(item: Item) {
        this.contents.push(item);
    }

    isSpawnable(): boolean {
        return !(
            this.lit ||
            this.character
        )
    }

    canSpawnFriendly(): boolean {
        return (
            this.passable &&
            this.lit > 0
        )
    }
}

export class Zone implements IZoneTemplate {
    public center: XY;
    public radius: number;
    public ttl: number;
    public passiveEffects: PassiveEffect[];
    public faction: Faction;  // Faction that is immune to this zone

    constructor(template: IZoneTemplate) {
        this.ttl = Energy.TURN * 5;
        this.passiveEffects = [];
        this.faction = Faction.Nobody;
        Object.assign(this, template);
        this.autoRealize();
    }

    protected autoRealize() {
        this.passiveEffects = PASSIVEEFFECTS.mapped(this.passiveEffects);
    }

    // Despite what the IDE says, this is in fact called (by Character#chain)
    public recvChain(topic: Chain, result: AddableResult, onBehalfOf: Character, args?: any): boolean {
        for(let effect of this.passiveEffects) {
            effect.recvChain(topic, result, onBehalfOf, args);
        }
        return true;
    }
}

export interface IMapTemplate {
    width: number;
    height: number;
    tiles: Array<Array<ITileTemplate>>;
    placedRooms: number;
    rooms: XYWH[];
}

export class TileMap implements IMapTemplate, SpawnQueryable {
    /* Fields that constructor creates:
    public width: number;
    public height: number;
    */

    public tiles: Array<Array<Tile>>;

    public lit: Set<Tile>;

    public open: Set<string>;

    // We can't rely on rooms being empty to determine if rooms have been
    // placed, because we remove from the rooms array as they're used.
    public placedRooms: number;
    public rooms: Array<XYWH>;

    static fromJson(template: IMapTemplate) {
        let {width, height, tiles} = template;
        let result = new TileMap(width, height);
        for(let x=0; x < result.width; x++) {
            for(let y=0; y < result.height; y++) {
                const xy = {x, y}
                let tilemplate = template.tiles[x][y];0
                let tile = Tile.fromJson(tilemplate);
                result.setTile(tile, xy);
            }
        }
        result.placedRooms = template.placedRooms;
        result.rooms = template.rooms;
        return result;
    }

    constructor(public width = 40, public height = 40) {
        this.placedRooms = 0;
        this.rooms = [];
        let tiles = [];
        for(let y = 0; y < height; y++) {
            let row = [];
            for(let x = 0; x < width; x++) {
                row.push(Tile.NULL);
            }
            tiles.push(row);
        }
        this.tiles = tiles;
        this.lit = new Set<Tile>();
        this.open = new Set<string>();
    }

    public rotjsMapCallback() {
        return (x: number, y: number, value: number) => {
            const where = {x, y};
            const newTile = Tile.rotjsValueToTile(value, where)
            this.setTile(newTile, where);
        }
    }

    public placeRooms(amount: number, attempts=100): number {
        let placed = 0;
        while(attempts > 0 && placed < amount) {
            let spot = this.randomOpenTile()
            if(this.placeAndAddRoomCentered(spot, BOSS_ROOM_TEMPLATE_FILLED)) {
                placed += 1;
            }
            attempts -= 1;
        }
        return placed;
    }

    public paintRoom(paintLoc: BoundingBox, template: RoomTemplate) {
        let roomHeight = template.length;
        let roomWidth = template[0].length;
        // paint the room
        for(let y=0; y < roomHeight; y++) {
            for(let x=0; x < roomWidth; x++) {
                let glyph = template[y][x];
                let curLoc = {
                    x: x + paintLoc.x,
                    y: y + paintLoc.y,
                }
                let tile = Tile.glyphToTile(glyph, curLoc);
                if(glyph === '+') tile.viewed = true;
                this.setTile(tile);
            }
        }
        let left = paintLoc.x - 1;
        let right = paintLoc.right + 1;

        // Carve out a surrounding passable border
        for(let y=paintLoc.y - 1; y <= paintLoc.bottom + 1; y++) {
            let curLoc = {
                x: left,
                y,
            }
            this.clearTile(curLoc);
            curLoc = {x: right, y};
            this.clearTile(curLoc);

            // Check the correction zone to the left and right
            this.correctTile({x: left -1, y});
            this.correctTile({x: right+1, y});
        }

        let top = paintLoc.y - 1;
        let bottom = paintLoc.bottom + 1;

        // Overlap with above is intentional so correction zones
        // are placed above and below the corners
        for(let x = paintLoc.x-1; x <= paintLoc.right+1; x++) {

            let curLoc = {x, y: top};
            this.clearTile(curLoc);
            curLoc = {x, y: bottom};
            this.clearTile(curLoc);

            // Upper and lower correction zones
            this.correctTile({x, y: top-1});
            this.correctTile({x, y: bottom+1});
        }

        // These are literally corner cases
        this.correctTile({x: left - 1,  y: top - 1});
        this.correctTile({x: right + 1, y: top - 1});
        this.correctTile({x: left - 1,  y: bottom + 1});
        this.correctTile({x: right + 1, y: bottom + 1});
    }

    public placeRoomCentered(center: XY, template: RoomTemplate): XYWH {
        let roomHeight = template.length;
        let roomWidth = template[0].length;
        let actualHeight = roomHeight + 4;
        let actualWidth = roomWidth + 4;

        let loc = BoundingBox.centered({
            x: center.x, y: center.y,
            w: actualWidth, h: actualHeight
        });
        if(!this.canPlaceRoomAt(loc)) return null;

        let paintLoc = BoundingBox.centered({
            x: center.x, y: center.y,
            w: roomWidth, h: roomHeight,
        });
        this.paintRoom(paintLoc, template);

        return loc;
    }

    public placeAndAddRoomCentered(center: XY, template: RoomTemplate): boolean {
        let loc = this.placeRoomCentered(center, template);
        if(!loc) return false;
        this.rooms.push(loc);
        this.placedRooms += 1;
        return true;
    }

    protected correctTile(loc: XY): boolean {
        let tile = this.tileAt(loc);
        if(tile.isNullTile()) {
            let tile = Tile.glyphToTile('#', loc);
            this.setTile(tile);
            return true;
        }
        return false;
    }

    protected clearTile(loc: XY): boolean {
        let tile = this.tileAt(loc);
        if(tile.passable) return false;
        tile = Tile.glyphToTile(' ', loc);
        this.setTile(tile);
    }

    public canPlaceRoomAt(where: XYWH): boolean {
        let bb = BoundingBox.fromXYWH(where);
        // Has to be on the map
        if(!this.locationInBounds(bb.upperLeft)) return false;
        if(!this.locationInBounds(bb.upperRight)) return false;
        if(!this.locationInBounds(bb.lowerLeft)) return false;
        if(!this.locationInBounds(bb.lowerRight)) return false;

        for(let other of this.rooms) {
            if(bb.intersects(other)) {
                return false;
            }
        }

        return true;
    }

    public findRoomNear(near: XY): XYWH {
        for(let room of this.rooms) {
            let bb = BoundingBox.fromXYWH(room);
            if(bb.contains(near)) return bb;
        }
        return null;
    }

    public randomOpenTile(): Tile {
        if(this.open.size === 0) return null;
        // horribly inefficient, but we're choosing random tiles a lot less
        // often than setting tiles
        let openAry = new Array(...this.open);
        let chosen = ROT.RNG.getItem(openAry);
        let loc = JSON.parse(chosen);
        return this.tileAt(loc);
    }

    public randomSpawnableTile(attempts=10): Tile {
        let chosenTile: Tile = null;
        while(!chosenTile && attempts > 0) {
            let candidate = this.randomOpenTile();
            if(candidate === null) return null;
            if(candidate.isSpawnable()) chosenTile = candidate;
            attempts -= 1;
        }
        return chosenTile;
    }

    public setTile(val: Tile, where: XY = null) {
        let x, y;
        if(where === null) {
            ({x, y} = val);
        } else {
            ({x, y} = where);
            if(val !== Tile.NULL) {
                val.x = x;
                val.y = y;
            }
        }

        this.tiles[x][y] = val;
        if(val.lit && !this.lit.has(val)) {
            this.lit.add(val);
        }
        let openkey = JSON.stringify(val.asXY());
        if(val.passable) {
            this.open.add(openkey);
        } else {
            this.open.delete(openkey);
        }
    }

    public tileAt(where: XY): Tile {
        if(!this.locationInBounds(where)) return Tile.NULL;
        return this.tiles[where.x][where.y];
    }

    public blockingTileAt(loc: XY, swapFaction=Faction.Nobody) : Tile {
        let result = this.tileAt(loc);
        if(!result.passable) return result;
        if(result.character && swapFaction !== Faction.Everybody) {
            if(result.character.faction !== swapFaction) return result;
        }
        return null;
    }

    public locationInBounds(loc: XY): boolean {
        const {x, y} = loc;
        if(x >= 0 && x < this.width) {
            if(y >= 0 && y < this.height) {
                return true;
            }
        }
        return false;
    }

    public recalculateLightingAt(loc: XY, r: number) {
        // Decompute lighting for currently lit tiles
        for(let tile of this.lit) {
            tile.lit = 0;
        }
        this.lit.clear();

        let inputCallback = (x: number, y: number) => {
            if(!this.locationInBounds({x, y})) return false;
            return this.tiles[x][y].passable;
        };
        let fov = new ROT.FOV.PreciseShadowcasting(inputCallback);
        fov.compute(loc.x, loc.y, r, (x: number, y: number, r: number, visibility: number) => {
            let tile = this.tileAt({x, y});
            if(tile === Tile.NULL) return;
            this.lit.add(tile);
            tile.lit = visibility;
            tile.viewed = true;
        });
    }

    public placeItem(what: Item, where: XY): boolean {
        let tile = this.tileAt(where);
        if (!tile.passable) return false;
        this.tileAt(where).receive(what);
        return true;
    }

    public takeItems(where: XY): Item[] {
        let tile = this.tileAt(where);
        if(tile.contents.length === 0) return [];
        let oldContents = tile.contents;
        tile.contents = [];
        return oldContents;
    }

    // To be called *before* the actual move, since we rely on `who` to have
    // the original position
    public moveCharacter(who: Character, where: XY): Character {
        let tile = this.tileAt(who.loc);
        let destTile = this.tileAt(where);
        let swappedWith = destTile.character;

        destTile.character = who;
        tile.character = swappedWith;
        return swappedWith;
    }

    public debugPrint(): string {
        let rows = [];
        for(let y=0; y<this.height; y++) {
            let xtiles = [];
            for(let x=0; x<this.width; x++) {
                xtiles.push(this.tileAt({x, y}).displayedChar(false));
            }
            rows.push(xtiles.join(''));
        }
        return rows.join('\n');
    }
}
