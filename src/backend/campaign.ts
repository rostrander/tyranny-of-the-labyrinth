import {GameMaster, IGameMasterTemplate} from "./gamemaster";
// Courtesy of https://pieroxy.net/blog/pages/lz-string/index.html
import * as LZString from "lz-string";

const SAVEGAME_VERSION = 1;

export interface IStorage {
    getItem(key: string): string;
    setItem(key: string, value: string): void;
}

export class Campaign {
    public static readonly SAVE_KEY = "campaign";

    public playing: GameMaster;
    public unlocks: string[];

    constructor(public storageBackend: IStorage = globalThis.localStorage) {
        this.bootstrap();
    }

    public save(): void {
        let oldPlaying = this.playing;
        if(this.playing) {
            // Round-trip to make sure the GM (and player, and map) are
            // definitely not the same.  This dates back to the slot system
            // so I'm not entirely sure it's necessary, but I'm keeping it.
            this.playing = new GameMaster(JSON.parse(JSON.stringify(this.playing)));
        }

        let campaignJson = JSON.stringify(this);
        let compressed = LZString.compressToUTF16(campaignJson);
        this.storageBackend.setItem(Campaign.SAVE_KEY, compressed);
        // However, the above would cause the PlayingState's GM and the campaign's
        // GM to diverge, resulting in future saves just re-saving the old GM.
        // As such, I'm restoring the reference GM to keep them in sync.
        // Keep an eye out for aliasing issues as mentioned in d11fd4
        this.playing = oldPlaying;
    }

    public toJSON() {
        return {
            playing: this.playing,
            unlocks: this.unlocks,
            version: SAVEGAME_VERSION,
        };
    }

    public loadGame(): GameMaster {
        this.bootstrap();
        return this.playing;
    }

    public newGame(gm: GameMaster, force = false): boolean {
        if(!this.playing || force) {
            this.playing = gm;
            return true;
        } else {
            return false;
        }
    }

    public gameOver() {
        if(!this.playing) return;
        this.playing = null;
        this.save();
    }

    public unlock(className: string, andSave = true): boolean {

        if(this.unlocks.indexOf(className) === -1) {
            this.unlocks.push(className);
            if(andSave) this.save();
            return true;
        }
        return false;
    }

    public isUnlocked(className: string): boolean {
        return this.unlocks.indexOf(className) !== -1;
    }

    public unlockAndGameOver(className: string): string {
        let unlocked = this.unlock(className, false);
        this.gameOver();
        return unlocked ? className : null;
    }

    protected bootstrap() {
        let campaignJson = this.storageBackend.getItem(Campaign.SAVE_KEY);
        if(campaignJson) {
            let uncompressed = LZString.decompressFromUTF16(campaignJson)
            let template = JSON.parse(uncompressed);
            // In the future, this is where we'd check version and do migrations
            this.playing = template.playing ? new GameMaster(template.playing) : null;
            this.unlocks = template.unlocks;
        } else {
            this.bootstrap_create();
        }
    }

    protected bootstrap_create() {
        this.playing = null;
        this.unlocks = [];
        this.save();
    }

}