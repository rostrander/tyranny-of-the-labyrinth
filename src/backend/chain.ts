import {AddableResult, Dice, IDieRollTemplate} from "./dice";
import {Chain} from "./common";
import {Character} from "./character";
import {GameMaster} from "./gamemaster";

export interface CombatChainArgs {
    fromWho: Character;
    toWho: Character;
    gm: GameMaster;
}

export interface ExpendEnergyArgs {
    amount: number;
}

export interface StatArgs {
    statName: string;
}

export enum DieCategory {
    WEAPON = 1,
    EXPLICIT = 2, // e.g. 1d8
}

export interface DieRollArgs {
    toRoll: IDieRollTemplate;
    gm: GameMaster; // so we can roll dice correctly
    category: DieCategory;
}

export interface IChainReceiver {
    onBehalfOf: Character;
    recvChain(topic: Chain, result: AddableResult, onBehalfOf: Character, args?: any): boolean

    chainGeneral?(topic: Chain, result: AddableResult, args?: any): boolean

    chainAC?(result: AddableResult, args?: CombatChainArgs): boolean
    chainFort?(result: AddableResult, args?: CombatChainArgs): boolean
    chainRef?(result: AddableResult, args?: CombatChainArgs): boolean
    chainWill?(result: AddableResult, args?: CombatChainArgs): boolean
    chainAllDefenses?(result: AddableResult, args?: CombatChainArgs): boolean

    chainMaxHp?(result: AddableResult): boolean

    chainExpendEnergy?(result: AddableResult, args?: ExpendEnergyArgs): boolean

    chainDamageModify?(result: AddableResult, args?: CombatChainArgs): boolean
    chainHealModify?(result: AddableResult): boolean

    chainIncomingDamage?(result: AddableResult, args?: CombatChainArgs): boolean;

    chainSaveModify?(result: AddableResult): boolean;

    chainCanMove?(result: AddableResult): boolean
    chainCanAct?(result: AddableResult): boolean
    chainCanSee?(result: AddableResult): boolean

    chainDieRoll?(result: AddableResult, args?: DieRollArgs): boolean
    chainAdvantage?(result: AddableResult): boolean

    chainToHit?(result: AddableResult, args?: CombatChainArgs): boolean

    chainOnHitEnemy?(result: AddableResult, args?: CombatChainArgs): boolean
    chainOnWasHit?(result: AddableResult, args?: CombatChainArgs): boolean

    chainStatBonus?(result: AddableResult, args?: StatArgs): boolean
}

export function chainDispatch(
    dispatchee: IChainReceiver,
    topic: Chain,
    result: AddableResult,
    onBehalfOf: Character,
    args?: any): boolean
{
    dispatchee.onBehalfOf = onBehalfOf;
    dispatchee.chainGeneral?.(topic, result, args);
    switch (topic) {
        case Chain.ac:
            dispatchee.chainAC?.(result, args);
            dispatchee.chainAllDefenses?.(result, args);
            break;
        case Chain.fort:
            dispatchee.chainFort?.(result, args);
            dispatchee.chainAllDefenses?.(result, args);
            break;
        case Chain.ref:
            dispatchee.chainRef?.(result, args);
            dispatchee.chainAllDefenses?.(result, args);
            break;
        case Chain.will:
            dispatchee.chainWill?.(result, args);
            dispatchee.chainAllDefenses?.(result, args);
            break;
        case Chain.AllDefenses:
            dispatchee.chainAllDefenses?.(result, args);
            break;
        case Chain.expendEnergy:
            dispatchee.chainExpendEnergy?.(result, args);
            break;
        case Chain.damageModify:
            dispatchee.chainDamageModify?.(result, args);
            break;
        case Chain.canMove:
            dispatchee.chainCanMove?.(result);
            break;
        case Chain.canAct:
            dispatchee.chainCanAct?.(result);
            break;
        case Chain.dieRoll:
            dispatchee.chainDieRoll?.(result, args);
            break;
        case Chain.toHit:
            dispatchee.chainToHit?.(result, args);
            break;
        case Chain.maxHp:
            dispatchee.chainMaxHp?.(result);
            break;
        case Chain.healModify:
            dispatchee.chainHealModify?.(result);
            break;
        case Chain.incomingDamage:
            dispatchee.chainIncomingDamage?.(result, args);
            break;
        case Chain.saveModify:
            dispatchee.chainSaveModify?.(result);
            break;
        case Chain.onHitEnemy:
            dispatchee.chainOnHitEnemy?.(result, args);
            break;
        case Chain.onWasHit:
            dispatchee.chainOnWasHit?.(result, args);
            break;
        case Chain.canSee:
            dispatchee.chainCanSee?.(result);
            break;
        case Chain.statBonus:
            dispatchee.chainStatBonus?.(result, args);
            break;
        case Chain.advantage:
            dispatchee.chainAdvantage?.(result);
            break;
    }
    return true;
}
