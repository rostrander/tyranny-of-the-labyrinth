import * as ROT from "rot-js";

import {Character} from "../character";
import {CLASSES} from "../classes";
import {RegularGameFactory} from "../newgame";

describe("Expected new character", () => {
    test("All stats are 10", () => {
        let baseChar = new Character(CLASSES.Fighter);
        expect(CLASSES.Fighter.stats.str).toStrictEqual(12);

        let gm = RegularGameFactory.make(baseChar);

        for(let stat of ["str", "dex", "con", "int", "wis", "cha"]) {
            expect(gm.player.stats[stat]).toStrictEqual(10);
        }

        // Didn't mess up the original
        expect(CLASSES.Fighter.stats.str).toStrictEqual(12);
    })
})