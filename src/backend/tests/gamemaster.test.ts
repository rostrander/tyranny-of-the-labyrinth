import * as ROT from "rot-js";

import {Character} from "../character";
import {BOSS_ROOM_TEMPLATE_FILLED, Tile, TileMap, Zone} from "../world";
import {GameMaster, Tier} from "../gamemaster";
import {PlayerController} from "../control";
import {Chain, Duration, Energy, Targets} from "../common";
import {StandThereBehavior} from "../behavior";
import {Dice} from "../dice";
import {Armor, Item, UsableItem, Weapon} from "../items/item";
import {POTIONS} from "../items/potions";
import {ARMOR} from "../items/armors";
import {Ability} from "../abilities/ability";
import {jsonRoundTrip, TestGameFactory} from "./testutils";
import {WEAPONS} from "../items/weapons";
import {BOSSES} from "../classes";
import {Faction} from "../interfaces";
import {masterLootTableByTier} from "../items/treasurer";

describe("GameMaster", () => {
    let tgf: TestGameFactory;
    let gm: GameMaster;
    let player: Character;

    beforeEach(() => {
        tgf = new TestGameFactory();
        ({player, gm} = tgf.testGameData());
    });

    test("Initial player added", () => {
        expect(gm.characters).not.toHaveLength(0);
        expect(gm.characters[0]).toStrictEqual(player);
        expect(player.loc).not.toBeFalsy();

        let tile = gm.map.tileAt(player.loc);
        expect(tile.character).toStrictEqual(player);
    });

    test("Test GM should place no rooms", () => {
        // Rooms appearing randomly has caused problems with other
        // tests that assume they have an empty field to play with
        expect(gm.map.placedRooms).toStrictEqual(0);
    });

    test("GM places rooms if they haven't already been placed", () => {
        let map = new TileMap(40, 40);
        let mapGen = new ROT.Map.Arena(40, 40);
        mapGen.create(map.rotjsMapCallback());

        expect(map.placedRooms).toStrictEqual(0);
        expect(map.rooms).toHaveLength(0);
        let newGm = new GameMaster({player, map});

        expect(map.placedRooms).toStrictEqual(1);
        expect(map.rooms).toHaveLength(newGm.tier.bossesThisTier);

        // doesn't do it again, though:
        let newerGm = new GameMaster({player, map});
        expect(map.placedRooms).toStrictEqual(1);
        expect(map.rooms).toHaveLength(newGm.tier.bossesThisTier);
    });

    describe("roll", () => {
        test("Can fix rolls", () => {
            gm.fixRolls(8,0,9000);

            let result = gm.roll(new Dice());
            expect(result.value).toStrictEqual(8);
            result = gm.roll(new Dice());
            expect(result.value).toStrictEqual(0);
            result = gm.roll(new Dice());
            expect(result.value).toStrictEqual(9000);
        });

    });

    test("Turn order", () => {
        let npc = tgf.testMonster( {
            loc: {x: 3, y: 3},
            energy: -2,
        });

        // Starts as the player's turn
        expect(gm.currentTurn).toStrictEqual(player);
        gm.endTurn(player, 4);
        // It is now nobody's turn
        expect(gm.currentTurn).toBeNull();

        gm.tick();
        // Still nobody's turn, nobody's ready
        expect(gm.currentTurn).toBeNull();
        expect(gm.ready).toHaveLength(0);

        gm.tickWithoutAct();
        // Now it should be NPC's turn
        expect(gm.ready).toHaveLength(1);
        expect(gm.ready[0]).toStrictEqual(npc);
        gm.act();

        // Nobody again
        expect(gm.currentTurn).toBeNull();
        expect(gm.ready).toHaveLength(0);

        gm.tick();
        // Still nobody
        expect(gm.currentTurn).toBeNull();
        expect(gm.ready).toHaveLength(0);

        let beginningOfTurn = jest.fn().mockImplementation();
        player.beginningOfTurn = beginningOfTurn;
        gm.tick();
        // Finally, it's the player's turn once more
        expect(gm.currentTurn).toStrictEqual(player);
        expect(beginningOfTurn).toHaveBeenCalled();
    });

    test("spawnCount", () => {
        expect(gm.spawnCount).toStrictEqual(0);

        let npc = tgf.testMonster( {
            loc: {x: 3, y: 3},
            energy: -2,
        });
        expect(gm.spawnCount).toStrictEqual(1);
        gm.characterDied(npc);
        expect(gm.spawnCount).toStrictEqual(0);
    });

    describe("TestPlayerController", () => {
        let pc: PlayerController;

        beforeEach(() => {
            pc = new PlayerController(player, gm);
        });

        describe("Bump movement", () => {

            test("Normal movement", () => {
                let origXY = {x: player.loc.x, y: player.loc.y }
                let moved = pc.bump({x: 1, y: 1});
                expect(moved).toBeTruthy();
                expect(player.loc.x).toStrictEqual(origXY.x + 1);
                expect(player.loc.y).toStrictEqual(origXY.y + 1);

                let oldTile = gm.map.tileAt(origXY);
                expect(oldTile.character).toBeNull();
                let newTile = gm.map.tileAt({x: origXY.x + 1, y: origXY.y + 1});
                expect(newTile.character).toStrictEqual(player);
            });

            describe("Supermove", () => {

                test("Normal supermove", () => {
                    let path = [{x: 1, y: 0}, {x: 0, y: 1}];
                    expect(pc.bump({x: 1, y: 1}, ...path)).toBeTruthy();

                    expect(player.loc).toEqual({x: 2, y: 2});
                    expect(player.isReady()).toBeFalsy(); // cuz their turn ended

                    tgf.nextTurn();
                    expect(player.isReady()).toBeFalsy(); // Because they automatically moved
                    expect(player.loc).toEqual({x: 3, y: 2});

                    tgf.nextTurn();
                    expect(player.isReady()).toBeFalsy(); // Because they automatically moved again
                    expect(player.loc).toEqual({x: 3, y: 3});

                    tgf.nextTurn();
                    expect(player.isReady()).toBeTruthy(); // Done moving
                });

                test("If supermove spots an enemy, it stops", () => {
                    let path = [{x: 1, y: 0}, {x: 0, y: 1}];
                    expect(pc.bump({x: 1, y: 1}, ...path)).toBeTruthy();

                    expect(player.loc).toEqual({x: 2, y: 2});
                    expect(player.isReady()).toBeFalsy(); // cuz their turn ended

                    // Enemy ambush!
                    let npc = tgf.testMonster( {
                        loc: {x: 3, y: 2},
                    });

                    tgf.nextTurn();
                    gm.fixRolls(19);  // We shouldn't attack, but if we do I want it to hit
                    expect(npc.hp).toStrictEqual(10000);  // ... so that this will fail
                    expect(player.isReady()).toBeTruthy();  // Instead it should be our turn because we didn't move

                    expect(pc.bump({x: -1, y: -1}, ...path)).toBeTruthy();
                    expect(player.isReady()).toBeFalsy(); // cuz their turn ended
                    expect(player.loc).toEqual({x: 1, y: 1});

                    tgf.nextTurn();
                    expect(player.isReady()).toBeTruthy();  // move canceled due to hostile
                });

                test("If supermove runs into something, it stops", () => {
                    let path = [{x: 1, y: 0}, {x: 0, y: 1}];
                    expect(pc.bump({x: 1, y: 1}, ...path)).toBeTruthy();

                    expect(player.loc).toEqual({x: 2, y: 2});
                    expect(player.isReady()).toBeFalsy(); // cuz their turn ended

                    // put up a roadblock
                    gm.map.setTile(Tile.rotjsValueToTile(1, {x: 3, y: 2}));
                    tgf.nextTurn();
                    expect(player.isReady()).toBeTruthy();  // Give up supermove because we can't move

                    // remove roadblock
                    gm.map.setTile(Tile.rotjsValueToTile(0, {x: 3, y: 2}));
                    expect(pc.bump({x: 1, y: 0})).toBeTruthy();
                    expect(player.isReady()).toBeFalsy(); // cuz their turn ended
                    expect(player.loc).toEqual({x: 3, y: 2});

                    tgf.nextTurn();
                    expect(player.isReady()).toBeTruthy();  // Old supermove canceled, no queued moves
                    expect(player.loc).toEqual({x: 3, y: 2}); // we didn't go anywhere
                });
            })

            test("Can't normal move unless the GM is locked for you", () => {
                gm.unlock();
                let origXY = {x: player.loc.x, y: player.loc.y }
                let moved = pc.bump({x: 1, y: 1});
                expect(moved).toBeFalsy();
                expect(player.loc).toStrictEqual(origXY);
            });

            test("Movement expends MOVE energy and unlocks", () => {
                let origEnergy = player.energy;
                expect(player.isReady()).toBeTruthy();
                pc.bump({x: 1, y: 1});
                expect(player.isReady()).toBeFalsy();
                expect(origEnergy - player.energy).toBe(Energy.MOVE);
                expect(gm.waitingForPlayer()).toBeFalsy();
            });

            test("Can't move into another creature", () => {
                let orig = {x: player.loc.x, y: player.loc.y}
                let npcLoc = {x: orig.x+1, y: orig.y+1};

                let npc = tgf.testMonster( {
                    loc: npcLoc,
                    hp: 10000,
                });
                npc.setBehavior(new StandThereBehavior(), gm);
                npc.energy = -1;
                gm.addCharacter(npc);

                pc.bump({x: 1, y: 1});
                // We're just going to ignore the angry fighting that probably
                // just happened there.  The point is, the player doesn't move
                expect(player.loc).toStrictEqual(orig);
                expect(gm.map.tileAt(player.loc).character).toStrictEqual(player);
                expect(gm.map.tileAt(npcLoc).character).toStrictEqual(npc);
            });

            test("Moving into a friendly creature swaps places", () => {
                let orig = {x: player.loc.x, y: player.loc.y}
                let npcLoc = {x: orig.x+1, y: orig.y+1};

                let npc = tgf.testMonster( {
                    loc: npcLoc,
                    hp: 10000,
                    faction: Faction.Player,
                });
                npc.setBehavior(new StandThereBehavior(), gm);
                gm.addCharacter(npc);

                expect(pc.bump({x: 1, y: 1})).toBeTruthy();
                expect(player.isReady()).toBeFalsy();
                expect(player.loc).toStrictEqual(npcLoc);
                expect(gm.map.tileAt(npcLoc).character).toStrictEqual(player);
                expect(gm.map.tileAt(orig).character).toStrictEqual(npc);
            });
        });

        describe("Centering", () => {
            let npc: Character;
            let ability: Ability;
            beforeEach(() => {
                npc = tgf.testMonster( {
                    loc: {x: 5, y: 5},
                });
                ability = player.defaultRangedAbility;
            });

            test("Basic ranged checks centering", () => {
                let canTarget = jest.fn().mockImplementation(() => {
                    return true;
                });
                gm.canCenterAbility = canTarget;

                expect(pc.attack(ability, npc.loc)).toBeTruthy();
                expect(canTarget).toHaveBeenCalled();
            });

            test("Can center normal in-range monsters", () => {
                expect(pc.canCenterAbility(ability, npc.loc)).toBeTruthy();
            });

            test("Monsters can't center you if you can't see them", () => {
                let onTile = gm.map.tileAt(npc.loc);
                let dra = npc.defaultRangedAbility;
                expect(gm.canCenterAbility(npc, dra, player.loc)).toBeTruthy();
                onTile.lit = 0;
                expect(gm.canCenterAbility(npc, dra, player.loc)).toBeFalsy();
            });

            test("Can't center empty tile (for now)", () => {
                let other = {x: 2, y: 4};
                expect(gm.map.tileAt(other).character).toBeFalsy();
                expect(pc.canCenterAbility(ability, other)).toBeFalsy();
            });

            test("Can't center out of range", () =>{
                let other = {x: 30, y: 30};
                gm.moveCharacter(npc, other);
                expect(gm.map.tileAt(other).character).toBeTruthy();
                expect(pc.canCenterAbility(ability, other)).toBeFalsy();
            });

            test("Can't center unlit", () => {
                // If targeting empty tiles gets changed, I may change this too
                let playerTile = gm.map.tileAt(gm.player.loc);
                let oneOver = playerTile.add({x: 1, y: 1});
                gm.map.setTile(Tile.rotjsValueToTile(1, oneOver));
                gm.recalculateLighting();

                expect(gm.map.tileAt(npc.loc).lit).toBeFalsy();
                expect(pc.canCenterAbility(ability, npc.loc)).toBeFalsy();
            });

            test("Can't center unpassable", () => {
                gm.map.tileAt(npc.loc).passable = false;
                expect(pc.canCenterAbility(ability, npc.loc)).toBeFalsy();
            });
        });

        describe("Targeting", () => {
            let npc: Character;
            let ability: Ability;
            beforeEach(() => {
                npc = tgf.testMonster( {
                    loc: {x: 5, y: 5},
                });
                ability = player.defaultRangedAbility;
            });

            test("Targeting yourself is not okay for anything but Self", () => {
                ability.targeting = Targets.None;
                expect(gm.canTarget(player, ability, player)).toBeFalsy();

                ability.targeting = Targets.Self;
                expect(gm.canTarget(player, ability, player)).toBeTruthy();

                ability.targeting = Targets.Melee;
                expect(gm.canTarget(player, ability, player)).toBeFalsy();

                ability.targeting = Targets.Ranged;
                expect(gm.canTarget(player, ability, player)).toBeFalsy();

                ability.targeting = Targets.PBAoE;
                expect(gm.canTarget(player, ability, player)).toBeFalsy();

                ability.targeting = Targets.TAoE;
                expect(gm.canTarget(player, ability, player)).toBeFalsy();
            });

            test("Targeting enemy okay for everything but Self/None", () => {
                ability.targeting = Targets.None;
                expect(gm.canTarget(player, ability, npc)).toBeFalsy();

                ability.targeting = Targets.Self;
                expect(gm.canTarget(player, ability, npc)).toBeFalsy();

                ability.targeting = Targets.Melee;
                expect(gm.canTarget(player, ability, npc)).toBeTruthy();

                ability.targeting = Targets.Ranged;
                expect(gm.canTarget(player, ability, npc)).toBeTruthy();

                ability.targeting = Targets.PBAoE;
                expect(gm.canTarget(player, ability, npc)).toBeTruthy();

                ability.targeting = Targets.TAoE;
                expect(gm.canTarget(player, ability, npc)).toBeTruthy();
            });
        });

        describe("Self abilities", () => {
            test("Self-targeted abilities can center on yourself", () => {
                let heal = player.gainAbility("BasicHeal");
                expect(pc.canCenterAbility(heal, player.loc)).toBeTruthy();
            });

        });

        describe("pickup", () => {
            let item: Item;

            beforeEach(() => {
                item = tgf.junkItem();
                gm.placeItemAt(item, player.loc);
            });

            test("Normal pickup", () => {
                let placedTile = gm.map.tileAt(player.loc);
                let origEnergy = player.energy;
                let origMessages = player.messages.length;

                expect(placedTile.contents).toHaveLength(1);
                expect(player.inventory).toHaveLength(0);
                expect(pc.pickup()).toBeTruthy();

                expect(placedTile.contents).toHaveLength(0);
                expect(player.inventory).toHaveLength(1);
                expect(player.inventory[0]).toStrictEqual(item);
                expect(origEnergy - player.energy).toBe(Energy.MOVE);
                expect(player.messages).toHaveLength(origMessages + 1);
            });

            test("Can't pickup if it's not your turn", () => {
                pc.wait();
                expect(player.isReady()).toBeFalsy();
                expect(pc.pickup()).toBeFalsy();
            });

            test("Can't pick something up if nothing's there", () => {
                gm.map.tileAt(player.loc).contents = [];  // rudely empty tile
                expect(pc.pickup()).toBeFalsy();
                expect(player.isReady()).toBeTruthy();
            });
        });

        describe("drop", () => {
            let item: Item;

            beforeEach(() => {
                item = tgf.junkItem();
                player.receiveItem(item);
            });

            test("Normal dropping", () => {
                expect(player.isReady()).toBeTruthy();
                let playerTile = gm.map.tileAt(player.loc);
                let origEnergy = player.energy;

                expect(playerTile.contents).toHaveLength(0);
                expect(player.inventory).toHaveLength(1);

                expect(pc.drop(item)).toBeTruthy();

                expect(playerTile.contents).toHaveLength(1);
                expect(player.inventory).toHaveLength(0);
                expect(playerTile.contents[0]).toBe(item);
                // Dropping stuff is free
                // Less due to game balancing and more due to the fact that I
                // don't want players dropping a bunch of things, being
                // at -9000 energy, and getting murdered by everything while
                // they catch up.
                expect(player.energy).toStrictEqual(origEnergy);
                // In fact, it's still your turn!
                expect(player.isReady()).toBeTruthy();
            });

            test("Can't drop if it's not your turn", () => {
                let playerTile = gm.map.tileAt(player.loc);
                expect(playerTile.contents).toHaveLength(0);
                expect(player.inventory).toHaveLength(1);

                pc.wait();
                expect(player.isReady()).toBeFalsy();

                expect(pc.drop(item)).toBeFalsy();
                expect(playerTile.contents).toHaveLength(0);
                expect(player.inventory).toHaveLength(1);
            });

            test("Can't drop on impassible location", () => {
                let playerTile = gm.map.tileAt(player.loc);
                playerTile.passable = false;

                expect(playerTile.contents).toHaveLength(0);
                expect(player.inventory).toHaveLength(1);

                expect(pc.drop(item)).toBeFalsy();
                expect(playerTile.contents).toHaveLength(0);
                expect(player.inventory).toHaveLength(1);
            });

            test("Can't drop item you don't have", () => {
                let playerTile = gm.map.tileAt(player.loc);
                let item2 = tgf.junkItem({name: 'more junk'});

                expect(playerTile.contents).toHaveLength(0);
                expect(player.inventory).toHaveLength(1);

                expect(pc.drop(item2)).toBeFalsy();
                expect(playerTile.contents).toHaveLength(0);
                expect(player.inventory).toHaveLength(1);
            });

            test("Dropping an equipped item unequips it", () => {
                let armor = new Armor(ARMOR.PaddedArmor);
                player.receiveItem(armor);
                player.equipment.body = armor;
                let originalAc = player.calcDefense(Chain.ac).value;

                expect(pc.drop(armor)).toBeTruthy();
                expect(player.equipment.body.isNothing()).toBeTruthy();
                let newAc = player.calcDefense(Chain.ac).value;
                expect(newAc).toStrictEqual(originalAc - 1);
            });
        });

        describe("Using items", () => {
            let item: UsableItem;

            beforeEach(() => {
                item = new UsableItem(POTIONS.MinorHealingPotion);
                player.receiveItem(item);
                player.hp = 1;
            });

            test("Normal use", () => {
                let hpDiff = item.effectTemplates[0].amount;
                let origEnergy = player.energy;
                let origInvenLength = player.inventory.length;
                expect(player.isReady()).toBeTruthy();

                expect(pc.use(item)).toBeTruthy(); // used

                // Did the thing
                expect(player.hp - 1).toStrictEqual(hpDiff);
                // Used MINOR energy
                expect(player.isReady()).toBeFalsy();
                expect(origEnergy - player.energy).toStrictEqual(Energy.MINOR);
                // Consumed the potion
                expect(player.inventory.length).toStrictEqual(origInvenLength - 1);
            });

            test("Can't use if it's not your turn", () => {
                let origInvenLength = player.inventory.length;
                pc.wait();
                expect(player.isReady()).toBeFalsy();

                expect(pc.use(item)).toBeFalsy();
                expect(player.hp).toStrictEqual(1);
                expect(player.inventory.length).toStrictEqual(origInvenLength);
            });

            test("Can't use an item that's not usable", () => {
                let item2 = tgf.junkItem({name: 'more junk'});
                player.receiveItem(item2);
                let origInvenLength = player.inventory.length;
                let origEnergy = player.energy;

                expect(pc.use(item2)).toBeFalsy();
                expect(player.hp).toStrictEqual(1);
                expect(player.inventory.length).toStrictEqual(origInvenLength);
                expect(player.energy).toStrictEqual(origEnergy);
            });

            test("Can't use an item you don't have", () => {
                let origInvenLength = player.inventory.length;
                let item2 = new UsableItem(POTIONS.MinorHealingPotion);
                expect(pc.use(item2)).toBeFalsy();
                expect(player.hp).toStrictEqual(1);
                expect(player.inventory.length).toStrictEqual(origInvenLength);
            });
        });

        describe("Equipping Items", () => {
            let armor: Armor;

            beforeEach(() => {
                armor = new Armor(ARMOR.PaddedArmor);
                player.receiveItem(armor);
            });
            test("Armor", () => {
                expect(player.equipment.body.isNothing()).toBeTruthy();
                let originalAc = player.calcDefense(Chain.ac).value;
                let energy = player.energy;
                expect(pc.equip(armor)).toBeTruthy();

                let newAc = player.calcDefense(Chain.ac).value;
                expect(newAc).toBeGreaterThan(originalAc);
                let curEnergy = player.energy
                expect(curEnergy).toStrictEqual(energy - Energy.MOVE);
                expect(player.equipment.body).toBe(armor);
            });

            test("Can't equip if it's not your turn", () => {
                pc.wait();
                let originalAc = player.calcDefense(Chain.ac).value;
                expect(pc.equip(armor)).toBeFalsy();

                let newAc = player.calcDefense(Chain.ac).value;
                expect(newAc).toStrictEqual(originalAc);
            });

            test("Can't equip an item that's not equippable", () => {
                let originalAc = player.calcDefense(Chain.ac).value;
                for(let slot in player.equipment) {
                    let equipped = player.equipment[slot];
                    expect(equipped.isNothing()).toBeTruthy();
                }
                let item2 = tgf.junkItem({name: 'more junk'});
                player.receiveItem(item2);
                let origEnergy = player.energy;

                expect(pc.equip(item2)).toBeFalsy();
                let newAc = player.calcDefense(Chain.ac).value;
                expect(newAc).toStrictEqual(originalAc);
                for(let slot in player.equipment) {
                    let equipped = player.equipment[slot];
                    expect(equipped.isNothing()).toBeTruthy();
                }
            });

            test("Can't equip an item you don't have", () => {
                player.removeItem(armor);
                let originalAc = player.calcDefense(Chain.ac).value;
                for(let slot in player.equipment) {
                    let equipped = player.equipment[slot];
                    expect(equipped.isNothing()).toBeTruthy();
                }

                expect(pc.equip(armor)).toBeFalsy();

                let newAc = player.calcDefense(Chain.ac).value;
                expect(newAc).toStrictEqual(originalAc);
                for(let slot in player.equipment) {
                    let equipped = player.equipment[slot];
                    expect(equipped.isNothing()).toBeTruthy();
                }
            });

            test("Can't equip an item to the wrong slot", () => {
                expect(player.equipment.mainHand.isNothing()).toBeTruthy();
                let originalAc = player.calcDefense(Chain.ac).value;
                expect(pc.equip(armor, "mainHand")).toBeFalsy();

                let newAc = player.calcDefense(Chain.ac).value;
                expect(newAc).toStrictEqual(originalAc);
                expect(player.equipment.mainHand.isNothing()).toBeTruthy();
            });

            test("Regular 2hander", () => {
                let wpn = new Weapon(WEAPONS.Morningstar);
                player.receiveItem(wpn);
                expect(pc.equip(wpn)).toBeTruthy();
                tgf.nextTurn();

                // Can't wear a shield in offhand or something
                let shield = new Armor(ARMOR.Shield);
                player.receiveItem(shield);
                expect(pc.equip(shield)).toBeFalsy();
                expect(player.equipment.offHand.isNothing()).toBeTruthy();

                // Can re-equip though
                let mace = new Weapon(WEAPONS.Mace);
                player.receiveItem(mace);
                expect(pc.equip(mace)).toBeTruthy();
            });

            test("Equipping a 2hander unequips offhand", () => {
                let shield = new Armor(ARMOR.Shield);
                player.receiveItem(shield);
                expect(pc.equip(shield)).toBeTruthy();
                tgf.nextTurn();

                let mace = new Weapon(WEAPONS.Mace);
                player.receiveItem(mace);
                expect(pc.equip(mace)).toBeTruthy();
                tgf.nextTurn();

                let wpn = new Weapon(WEAPONS.Morningstar);
                player.receiveItem(wpn);
                expect(pc.equip(wpn)).toBeTruthy();
                expect(player.equipment.offHand.isNothing()).toBeTruthy();
            });

            describe("Two Rings", () => {
                let ring1: Armor;
                let ring2: Armor;
                let ring3: Armor;

                beforeEach(() => {
                    ring1 = new Armor({
                        name: 'Ring1',
                        char: '=',
                        ac: 0,
                        slots: ['ring1', 'ring2'],
                        desc: 'Ring1',
                    });
                    ring2 = new Armor({
                        name: 'Ring2',
                        char: '=',
                        ac: 0,
                        slots: ['ring1', 'ring2'],
                        desc: 'Ring2',
                    });
                    ring3 = new Armor({
                        name: 'Ring3',
                        char: '=',
                        ac: 0,
                        slots: ['ring1', 'ring2'],
                        desc: 'Ring3',
                    });
                    player.receiveItem(ring1,ring2,ring3);
                });

                test("Equipping two rings", () => {
                    expect(player.equipment.ring1.isNothing()).toBeTruthy();
                    expect(player.equipment.ring2.isNothing()).toBeTruthy();

                    expect(pc.equip(ring1)).toBeTruthy();
                    expect(player.equipment.ring1).toEqual(ring1);
                    expect(player.equipment.ring2.isNothing()).toBeTruthy();

                    tgf.nextTurn();
                    expect(pc.equip(ring2)).toBeTruthy();
                    expect(player.equipment.ring1).toEqual(ring2);
                    expect(player.equipment.ring2).toEqual(ring1);

                    tgf.nextTurn();
                    expect(pc.equip(ring3)).toBeTruthy();
                    expect(player.equipment.ring1).toEqual(ring3);
                    expect(player.equipment.ring2).toEqual(ring2);
                });

                test("Equipping already worn rings", () => {
                    expect(pc.equip(ring1)).toBeTruthy();
                    expect(player.equipment.ring1).toEqual(ring1);
                    expect(player.equipment.ring2.isNothing()).toBeTruthy();

                    tgf.nextTurn();
                    expect(pc.equip(ring1)).toBeTruthy();
                    expect(player.equipment.ring1).toEqual(ring1);
                    expect(player.equipment.ring2.isNothing()).toBeTruthy();

                    tgf.nextTurn();
                    expect(pc.equip(ring2)).toBeTruthy();
                    expect(player.equipment.ring1).toEqual(ring2);
                    expect(player.equipment.ring2).toEqual(ring1);

                    tgf.nextTurn();
                    expect(pc.equip(ring1)).toBeTruthy();
                    expect(player.equipment.ring1).toEqual(ring1);
                    expect(player.equipment.ring2).toEqual(ring2);
                });

                test("Equipping into empty slot", () => {
                    expect(pc.equip(ring1, "ring2")).toBeTruthy();
                    expect(player.equipment.ring1.isNothing()).toBeTruthy();
                    expect(player.equipment.ring2).toEqual(ring1);

                    tgf.nextTurn();
                    expect(pc.equip(ring2)).toBeTruthy();
                    expect(player.equipment.ring1).toEqual(ring2);
                    expect(player.equipment.ring2).toEqual(ring1);
                });
            });
        });

        describe("Unequipping items", () => {
            let armor: Armor;

            beforeEach(() => {
                armor = new Armor(ARMOR.PaddedArmor);
                player.receiveItem(armor);
                player.equipment.body = armor;
            });

            test("Unequipping armor", () => {
                expect(player.equipment.body.isNothing()).toBeFalsy();
                let originalAc = player.calcDefense(Chain.ac).value;
                let energy = player.energy;
                expect(pc.unequip("body")).toBeTruthy();

                let newAc = player.calcDefense(Chain.ac).value;
                expect(newAc).toBeLessThan(originalAc);
                let curEnergy = player.energy
                expect(curEnergy).toStrictEqual(energy);
                expect(player.equipment.body.isNothing()).toBeTruthy();
            });

            test("Can't unequip if it's not your turn", () => {
                pc.wait();
                let originalAc = player.calcDefense(Chain.ac).value;
                expect(pc.unequip("body")).toBeFalsy();

                let newAc = player.calcDefense(Chain.ac).value;
                expect(newAc).toStrictEqual(originalAc);
            });

            test("Can't unequip nothing", () => {
                expect(player.equipment.neck.isNothing()).toBeTruthy();
                let energy = player.energy;

                expect(pc.unequip("neck")).toBeFalsy();

                let curEnergy = player.energy
                expect(curEnergy).toStrictEqual(energy);
                expect(player.equipment.neck.isNothing()).toBeTruthy();
            });
        });

        describe("Tick", () => {
            test("Moves the tick counter one tick forward", () => {
                gm.unlock();
                let origTicks = gm.tickNum;
                pc.tick();
                expect(gm.tickNum).toStrictEqual(origTicks + 1);
            });

            test("Ticks do nothing if we're waiting on player input", () => {
                // GM is locked by default so nothing to do here really
                let origTicks = gm.tickNum;
                pc.tick();
                expect(gm.tickNum).toStrictEqual(origTicks);
            });

            test("Ticking someone from unready to ready starts their turn", () => {
                gm.unlock();
                expect(gm.waitingForPlayer()).toBeFalsy();
                player.energy = -1;
                pc.tick();
                expect(player.isReady()).toBeTruthy();
                expect(gm.waitingForPlayer()).toBeTruthy();
            });
        });

        describe("Waiting", () => {  // The hardest part
            test("Ends the player's turn", () => {
                pc.wait();
                expect(gm.waitingForPlayer()).toBeFalsy();
            });

            test("Spends MINOR energy", () => {
                let origEnergy = player.energy;
                expect(player.isReady()).toBeTruthy();
                pc.wait();
                expect(player.isReady()).toBeFalsy();
                expect(origEnergy - player.energy).toBe(Energy.MINOR);
            })
        });

        describe("Callbacks", () => {
            test("Deaaaaaaath", () => {
                let cbCalled = false;
                player.hp = 0;
                pc.onDeath(() => { cbCalled = true;});
                pc.tick();
                expect(cbCalled).toBeTruthy();
            });
        });
    });

    // These tests aren't of PlayerController functionality, but rather
    // are GameMaster related and need a PlayerController to do their thing
    describe("Needs PlayerController", () => {
        let pc: PlayerController;

        beforeEach(() => {
            pc = new PlayerController(player, gm);
        });

        describe("Death", () => {
            test("0HP ends the game", () => {
                expect(gm.running).toBeTruthy();
                player.hp = 0;
                expect(player.alive).toBeFalsy();
                expect(gm.running).toBeFalsy();
            });

            describe("Can't do anything if game is over", () => {
                beforeEach(() => {
                    player.hp = 0;
                });

                test("Ticks don't tick", () => {
                    gm.unlock();
                    let origTicks = gm.tickNum;
                    pc.tick();
                    expect(gm.tickNum).toStrictEqual(origTicks);
                });

                test("Callback is called", () => {
                    let cbCalled = false;
                    gm.onDeath(() => { cbCalled = true;});
                    pc.tick();
                    expect(cbCalled).toBeTruthy();
                });

                test("Can't move player", () => {
                    let origXY = {x: player.loc.x, y: player.loc.y }
                    let moved = pc.bump({x: 1, y: 1});
                    expect(moved).toBeFalsy();
                    expect(player.loc).toStrictEqual(origXY);
                });
            });
        });

        // Lighting itself is on the World layer and ROT.js is doing
        // all the work; this is mainly to verify that the GM is recalculating
        // everything correctly.
        describe("Lighting", () => {
            test("Player's vicinity lit by default", () => {
                let playerTile = gm.map.tileAt(gm.player.loc);
                expect(playerTile.lit).toBeGreaterThan(0);
                let oneDown = gm.map.tileAt(playerTile.add({x: 0, y: 1}));
                expect(oneDown.lit).toBeTruthy();
            });

            test("Movement recalculates lighting", () => {
                let playerTile = gm.map.tileAt(gm.player.loc);
                let oneOver = playerTile.add({x: 1, y: 0});
                let twoOver = playerTile.add({x: 2, y: 0});
                gm.map.setTile(Tile.rotjsValueToTile(1, oneOver));
                gm.recalculateLighting();
                let blockedTile = gm.map.tileAt(twoOver);
                // Should be blocked by that thing we just put there
                expect(blockedTile.lit).toBeFalsy();

                // Move so it's in view
                pc.bump({x: 1, y: 1});
                // Should be visible
                expect(blockedTile.lit).toBeTruthy();
            });
        });
    });

    test("Dead monsters drop loot", () => {
        gm.treasurer.lootTable = {
            "test": 1,
        };
        let npc = tgf.basicTestMonster();
        let tile = gm.map.tileAt(npc.loc);
        expect(tile.contents).toHaveLength(0);

        gm.characterDied(npc);
        tile = gm.map.tileAt(npc.loc);
        // the monster is gone
        expect(tile.character).toBeFalsy();
        // but the treasure remains
        expect(tile.contents).toHaveLength(1);
    });

    describe("Zones", () => {
        let nilZone: Zone;

        beforeEach(() => {
            nilZone = new Zone({
                center: {x: 3, y: 3},
                radius: 1,
            });
        });

        test("addZone happy path", () => {
            expect(gm.zones).toHaveLength(0);
            for(let tile of gm.tilesInBurst({x: 3, y: 3}, 1)) {
                expect(tile.zone).toBeFalsy();
            }

            gm.addZone(nilZone);

            expect(gm.zones).toHaveLength(1);
            // Tiles should reflect that:
            for(let tile of gm.tilesInBurst({x: 3, y: 3}, 1)) {
                expect(tile.zone).toBeTruthy();
            }
        });

        test("Adding zones out of bounds doesn't explode", () => {
            nilZone.center = {x: 0, y: 0};
            nilZone.radius = 2;
            expect(() => {
                gm.addZone(nilZone);
            }).not.toThrow();
        });

        test("removeZone happy path", () => {
            gm.addZone(nilZone);
            expect(gm.zones).toHaveLength(1);

            gm.removeZone(nilZone);
            expect(gm.zones).toHaveLength(0);
            for(let tile of gm.tilesInBurst({x: 3, y: 3}, 1)) {
                expect(tile.zone).toBeFalsy();
            }
        });

        test("Zones die after TTL runs out", () => {
            nilZone.ttl = 2;
            gm.addZone(nilZone);
            expect(gm.zones).toHaveLength(1);

            gm.endTurn(player, Energy.STANDARD);
            gm.tick();
            expect(nilZone.ttl).toStrictEqual(1);
            expect(gm.zones).toHaveLength(1);

            gm.tick();
            expect(nilZone.ttl).toStrictEqual(0);
            expect(gm.zones).toHaveLength(0);

            for(let tile of gm.tilesInBurst({x: 3, y: 3}, 1)) {
                expect(tile.zone).toBeFalsy();
            }
        });

        test("Zones overwrite each other", () => {
            gm.addZone(nilZone);
            expect(gm.zones).toHaveLength(1);

            let otherZone = new Zone({center: {x: 4, y: 4}, radius: 1});
            gm.addZone(otherZone);
            expect(gm.zones).toHaveLength(2);

            let oldZoneTile = gm.map.tileAt({x: 2, y: 2});
            expect(oldZoneTile.zone).toBe(nilZone);
            let newZoneTile = gm.map.tileAt({x: 3, y: 3});
            expect(newZoneTile.zone).toBe(otherZone);
        });
    });

    describe("Bosses", () => {
        let pc: PlayerController;
        let doorTile: Tile;
        const testBoss = Object.keys(BOSSES)[0];

        beforeEach(() => {
            pc = new PlayerController(player, gm);
            let spot = gm.map.tileAt({x: 15, y: 15});
            gm.map.placeAndAddRoomCentered(spot, BOSS_ROOM_TEMPLATE_FILLED);
            gm.forceMoveCharacter(player, {x: 15, y: 11});
            doorTile = gm.map.tileAt({x: 15, y: 12});
        });

        test("Trying to walk into a door prompts dialog", () => {
            expect(doorTile.char).toStrictEqual("+");

            let doorCallback = jest.fn();
            gm.onDoor(doorCallback);
            expect(pc.bump({x: 0, y: 1})).toBeFalsy();
            expect(doorCallback).toHaveBeenCalled();
        });

        test("Turning closed boss room into open boss room", () => {
            let playerTile = gm.map.tileAt(player.loc)
            expect(gm.carveBossRoom(playerTile)).toBeTruthy();

            // Door's open
            let newDoorTile = gm.map.tileAt(doorTile);
            expect(doorTile.passable).toBeFalsy();
            expect(newDoorTile.passable).toBeTruthy();
            expect(newDoorTile.lit).toBeGreaterThan(0);

            // Player is on the new tile
            let newPlayerTile = gm.map.tileAt(playerTile);
            expect(newPlayerTile.passable).toBeTruthy();
            expect(newPlayerTile.character).toBe(player);
        });

        test("Challenging a boss", () => {
            expect(gm.challengeBoss(testBoss)).toBeTruthy();

            // Door's open
            let newDoorTile = gm.map.tileAt(doorTile);
            expect(newDoorTile.passable).toBeTruthy();

            // There's a boss in the middle
            let bossLoc = {
                x: player.loc.x,
                y: player.loc.y + 4,
            }
            let bossTile = gm.map.tileAt(bossLoc);
            expect(bossTile.passable).toBeTruthy();
            expect(bossTile.character).toBeTruthy();
        });

        test("Can't challenge another boss while first is underway", () => {
            // Make another room to challenge from
            let newSpot = {x: 30, y: 30};
            let placed = gm.map.placeAndAddRoomCentered(newSpot, BOSS_ROOM_TEMPLATE_FILLED);
            expect(placed).toBeTruthy();

            expect(gm.challengeBoss(testBoss)).toBeTruthy();
            gm.forceMoveCharacter(player, {x: 30, y: 26});

            // Trying to open the door will not result in callback
            let msgLength = player.messages.length;
            let newDoorTile = gm.map.tileAt({x: 30, y: 27});
            expect(newDoorTile.char).toStrictEqual('+');

            let doorCallback = jest.fn();
            gm.onDoor(doorCallback);
            expect(player.isReady()).toBeTruthy();
            expect(pc.canChallengeBoss()).toBeFalsy();
            expect(pc.bump({x: 0, y: 1})).toBeFalsy();
            expect(doorCallback).not.toHaveBeenCalled();
            // Should have got a message about how you can't do that
            expect(player.messages.length).toBeGreaterThan(msgLength);
        });

        describe("Defeat", () => {
            let boss: Character;

            beforeEach(() => {
                boss = gm.challengeBoss(testBoss);
                expect(boss).toBeTruthy();
            });

            test("Can now challenge a new boss", () => {
                let newSpot = {x: 30, y: 30};
                let placed = gm.map.placeAndAddRoomCentered(newSpot, BOSS_ROOM_TEMPLATE_FILLED);
                expect(placed).toBeTruthy();

                gm.forceMoveCharacter(player, {x: 30, y: 26});

                let newDoorTile = gm.map.tileAt({x: 30, y: 27});
                expect(newDoorTile.char).toStrictEqual('+');

                gm.characterDied(boss);

                let doorCallback = jest.fn();
                gm.onDoor(doorCallback);
                expect(player.isReady()).toBeTruthy();
                expect(pc.canChallengeBoss()).toBeTruthy();
                expect(pc.bump({x: 0, y: 1})).toBeFalsy();
                expect(doorCallback).toHaveBeenCalled();
            });

            test("Can't challenge an already defeated boss", () => {
                let newSpot = {x: 30, y: 30};
                let placed = gm.map.placeAndAddRoomCentered(newSpot, BOSS_ROOM_TEMPLATE_FILLED);
                expect(placed).toBeTruthy();

                gm.forceMoveCharacter(player, {x: 30, y: 26});

                let newDoorTile = gm.map.tileAt({x: 30, y: 27});
                expect(newDoorTile.char).toStrictEqual('+');

                expect(gm.tier.bossesKilled).toStrictEqual(0);
                gm.characterDied(boss);
                expect(gm.tier.bossesKilled).toStrictEqual(1);

                expect(pc.canChallengeBoss(boss.name)).toBeFalsy();
            });

            test("Recharges powers", () => {
                let ability = player.gainAbility("HitEvenHarder");
                expect(ability.cooldown).toStrictEqual(Duration.Boss);
                expect(ability.isReady()).toBeTruthy();

                ability.startCooldown();
                expect(ability.isReady()).toBeFalsy();

                gm.characterDied(boss);

                expect(ability.isReady()).toBeTruthy();

                // See https://gitlab.com/rostrander/tyranny-of-the-labyrinth/-/issues/58
                expect(player.refreshBossNextTurn).toBeTruthy();
            });

            test("Makes callback", () => {
                let callback = jest.fn(() => {
                    // Make sure current boss isn't cleared yet, as
                    // we'll need it to give player credit
                    expect(gm.currentBoss).toBeTruthy();
                });
                gm.onBossKill(callback);

                gm.characterDied(boss);
                expect(callback).toHaveBeenCalled();
            });

            test("Levels up", () => {
                let oldLevel = player.level;
                gm.characterDied(boss);
                expect(player.level).toStrictEqual(oldLevel + 1);
            });

            test("Stairs down callback", () => {
                let bossLoc = boss.loc;
                gm.characterDied(boss);

                // Should be stairs there
                let tile = gm.map.tileAt(bossLoc);
                expect(tile.character).toBeFalsy();
                expect(tile.char).toStrictEqual(">");

                // nothing on those stairs
                expect(tile.displayedChar()).toStrictEqual(">");
                expect(tile.contents).toHaveLength(0);
                expect(tile.character).toBeFalsy();

                gm.forceMoveCharacter(player, {x: bossLoc.x, y: bossLoc.y-1});
                let stairCallbck = jest.fn();
                gm.onStairs(stairCallbck);
                expect(pc.bump({x:0, y: 1})).toBeFalsy();
                expect(stairCallbck).toHaveBeenCalled();
            });

        });
    });

    describe("Tier up", () => {
        let pc: PlayerController;

        beforeEach(() => {
            pc = new PlayerController(player, gm);
        });

        test("Increments tier, treasurer", () => {
            let oldTier = gm.tier;
            expect(oldTier.tierNum).toStrictEqual(1);
            expect(gm.treasurer.tierNum).toStrictEqual(1);

            let expectedNewTier = oldTier.nextTier();
            pc.nextTier();

            expect(gm.tier).toEqual(expectedNewTier);
            expect(gm.tier.tierNum).toStrictEqual(2);
        });

        test("Increments treasurer", () => {
            pc.nextTier();
            expect(gm.treasurer.tierNum).toStrictEqual(2);
        });

        test("Increments spawner", () => {
            pc.nextTier();
            expect(gm.spawner.tierNum).toStrictEqual(2);
            expect(gm.spawner.spawnPool).toHaveProperty("Hobgoblin");
        })

        test("Creates a new map", () => {
            let oldMap = gm.map;
            let oldLoc = gm.player.loc;
            pc.nextTier();
            expect(gm.map).not.toBe(oldMap);
            expect(gm.characters).toHaveLength(1);
            expect(gm.zones).toHaveLength(0);

            // Player spawns somewhere else, right?
            expect(gm.player.loc).not.toEqual(oldLoc);
            // How about that lighting?
            let tile = gm.map.tileAt(gm.player.loc);
            expect(tile.lit).toBeTruthy();
        });
    });

    describe("Serialization", () => {
        test("Characters put back on their tiles", () => {
            gm.forceMoveCharacter(player, {x: 5, y: 2});
            let onTilePlayer = gm.map.tileAt(player.loc);
            let npc = tgf.testMonster( {
                loc: {x: 3, y: 3},
                energy: -2,
            });
            let onTileMonster = gm.map.tileAt(npc.loc);
            expect(gm.player.map).toBe(gm.map);

            let exported = jsonRoundTrip(gm);
            expect(exported['characters']).toHaveLength(1); // player omitted

            let newgame = new GameMaster(exported);
            expect(newgame.characters).toHaveLength(2);
            let newTilePlayer = newgame.map.tileAt(player.loc);
            expect(newTilePlayer.character).toBe(newgame.player);
            let newTileNpc = newgame.map.tileAt(npc.loc);
            expect(newTileNpc.character).toBeTruthy();
            expect(newgame.player.map).toBe(newgame.map);
        });

        test("Characters get GM injected into their behaviors", () => {
            let npc = tgf.testMonster( {
                loc: {x: 3, y: 3},
                energy: -2,
            });
            let onTileMonster = gm.map.tileAt(npc.loc);

            let exported = jsonRoundTrip(gm);
            let newgame = new GameMaster(exported);
            let newNpc = newgame.characters.find(c => c !== newgame.player);
            expect(newNpc).toBeTruthy();
            expect(newNpc.behavior.character).toBe(newNpc);
            expect(newNpc.behavior.gm).toBe(newgame);
        });

        test("Player reinserted correctly", () => {
            gm.forceMoveCharacter(player, {x: 5, y: 2});
            let onTile = gm.map.tileAt(player.loc);
            let exported = jsonRoundTrip(gm);

            expect(exported['characters']).toHaveLength(0);

            let newgame = new GameMaster(exported);
            expect(newgame.characters).toHaveLength(1);
            expect(newgame.characters[0]).toBe(newgame.player);

            expect(player.loc).toEqual({x: 5, y: 2});
            let newTile = gm.map.tileAt(player.loc)
            expect(newTile).toEqual(onTile);
            expect(newTile).toBeInstanceOf(Tile);
        });

        test("GM sets currentTurn and ready correctly", () => {
            let exported = jsonRoundTrip(gm);
            let newgame = new GameMaster(exported);
            expect(newgame.player.isReady()).toBeTruthy();
            expect(newgame.locked).toStrictEqual(1);  // locked shouldn't persist
            expect(newgame.currentTurn).toBe(newgame.player);
        });

        test("JSON exclusions", () =>{
            let exported = jsonRoundTrip(gm);
            expect(exported['spawner']).toBeUndefined();
            expect(exported['treasurer']).toBeUndefined();
        });

        test("Zones re-created correctly", () => {
            let nilZone = new Zone({
                center: {x: 3, y: 3},
                radius: 1,
            });
            gm.addZone(nilZone);

            let exported = jsonRoundTrip(gm);
            let newgame = new GameMaster(exported);

            expect(newgame.zones).toHaveLength(1);
            expect(newgame.zones[0]).toBeInstanceOf(Zone);
            let zoneTile = newgame.map.tileAt({x: 2, y: 4});
            expect(zoneTile.zone).toBe(newgame.zones[0]);
        });

        test("Treasurer tiers set correctly", () => {
            gm.nextTier();
            expect(gm.tier.tierNum).toStrictEqual(2);
            expect(gm.treasurer.tierNum).toStrictEqual(2);
            expect(gm.treasurer.lootTable).toBe(masterLootTableByTier[1]);

            let exported = jsonRoundTrip(gm);
            let newgame = new GameMaster(exported);
            expect(newgame.tier.tierNum).toStrictEqual(2);
            expect(newgame.treasurer.tierNum).toStrictEqual(2);
            expect(newgame.treasurer.lootTable).toBe(masterLootTableByTier[1]);
        });
    });
});

describe("Tier", () => {
    test("Tier is a full-fledged class", () => {
        let tgf: TestGameFactory;
        let gm: GameMaster;

        tgf = new TestGameFactory();
        gm = tgf.testGameData().gm;
        expect(gm.tier).toBeInstanceOf(Tier);
    });

    test("Knows when to go up a tier", () => {
        let tier = new Tier({tierNum: 1, bossesThisTier: 1, topTier: 2});
        expect(tier.bossesKilled).toStrictEqual(0);
        expect(tier.nextTierReady()).toBeFalsy();
        tier.bossKilled();
        expect(tier.bossesKilled).toStrictEqual(1);
        expect(tier.nextTierReady()).toBeTruthy();
    });

    test("Knows when game is almost won", () => {
        let tier = new Tier({tierNum: 1, bossesThisTier: 2, topTier: 3});
        expect(tier.oneBossLeft()).toBeFalsy();
        tier.bossKilled();  // killing one of two on bottom tier doesn't do it
        expect(tier.oneBossLeft()).toBeFalsy();
        tier.bossKilled();  // Nor the other
        expect(tier.oneBossLeft()).toBeFalsy();
        tier = tier.nextTier();
        // Even though there's one boss left on this tier, it's not the final boss
        expect(tier.oneBossLeft()).toBeFalsy();
        tier.bossKilled();
        // Now there actually is one boss left, but it doesn't count because we haven't tiered up
        expect(tier.oneBossLeft()).toBeFalsy();
        tier = tier.nextTier();
        // That'll do it
        expect(tier.oneBossLeft()).toBeTruthy();
    });

    test("Knows when the game is won", () => {
        let tier = new Tier({tierNum: 1, bossesThisTier: 1, topTier: 2});
        expect(tier.wonGame()).toBeFalsy();
        tier = tier.nextTier();
        expect(tier.wonGame()).toBeFalsy();
        tier.bossKilled();
        // This should have us on tier 2 (the top) w/ 1 boss killed out of 1
        expect(tier.wonGame()).toBeTruthy();
    })

    test("Goes up a tier", () => {
        let tier = new Tier({
            tierNum: 1,
            bossesThisTier: 2,
            topTier: 3,
            bossesKilled: 2,
        });
        let nextTier = tier.nextTier();
        expect(nextTier.tierNum).toStrictEqual(2);
        expect(nextTier.bossesThisTier).toStrictEqual(1);
        expect(nextTier.topTier).toStrictEqual(3);
        expect(nextTier.bossesKilled).toStrictEqual(0);

        nextTier.bossesKilled = 1;
        expect(nextTier.nextTierReady()).toBeTruthy();
        nextTier = nextTier.nextTier();
        expect(nextTier.tierNum).toStrictEqual(3);
        expect(nextTier.bossesThisTier).toStrictEqual(1);
        expect(nextTier.topTier).toStrictEqual(3);
        expect(nextTier.bossesKilled).toStrictEqual(0);
    });

    test("Tutorial tier also goes up a tier", () => {
        let tier = new Tier({tierNum: 1, bossesThisTier: 1, topTier: 2, bossesKilled: 1});

        let nextTier = tier.nextTier();
        expect(nextTier.tierNum).toStrictEqual(2);
        expect(nextTier.bossesThisTier).toStrictEqual(1);
        expect(nextTier.topTier).toStrictEqual(2);
        expect(nextTier.bossesKilled).toStrictEqual(0);
    });
});
