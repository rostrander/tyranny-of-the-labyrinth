import {defaults} from "../common";

describe("defaults", () => {
    let baseline: any;
    beforeEach(() => {
        baseline = {a: 1, b: 2};
    });

    test("Basic operation", () => {
        let result = defaults({c: 3}, baseline, {z: 1});
        expect(result).toHaveLength(2);
        expect(result[0].c).toStrictEqual(3);
        expect(result[1].c).toStrictEqual(3);
    });

    test("Doesn't override preexisting", () => {
        let result = defaults({b: 9}, baseline, {z: 1});
        expect(result).toHaveLength(2);
        expect(result[0].b).toStrictEqual(2);
        expect(result[1].b).toStrictEqual(9);
    });

    test("Nesting", () => {
        let result = defaults({c: 3},
            baseline,
            ...defaults({d: 40},
                {a:2, b:1}
            )
        );
        expect(result).toHaveLength(2);
        expect(result[0].c).toStrictEqual(3);
        expect(result[0].d).toBeFalsy();
        expect(result[1].c).toStrictEqual(3);
        expect(result[1].d).toStrictEqual(40);
    });
})
