import {Character} from "../../character";
import {GameMaster} from "../../gamemaster";
import {MeleeCombatBehavior, StandThereBehavior} from "../../behavior";
import {PlayerController} from "../../control";
import {Dice} from "../../dice";
import {UsableItem, Weapon} from "../../items/item";
import {WEAPONS} from "../../items/weapons";
import {Condition, CONDITIONS} from "../../effects/condition";
import {Chain, Duration, Energy, Point} from "../../common";
import {IEffectTemplate} from "../../effects/effect";
import {BestowConditionEffect} from "../../effects/active";
import {POTIONS} from "../../items/potions";
import {jsonRoundTrip, TestGameFactory} from "../testutils";

// This file is to test overall conditions usable for any skill in general.
// Ability-specific conditions are tested in that ability's test.
let tgf: TestGameFactory;
let gm: GameMaster;
let pc: PlayerController;
let player: Character;
let npc: Character;

beforeEach(() => {
    tgf = new TestGameFactory();
    ({player, gm} = tgf.testGameData());
    npc = tgf.testMonster({baseMaxHp: 10000});
    pc = new PlayerController(player, gm);
})

describe("Debuffs", () => {
    // Dazed means you spend twice as much energy on standard actions
    test("Dazed", () => {
        tgf.bestowAndVerify("Dazed");
        let oldEnergy = player.energy;
        player.expend(Energy.STANDARD);
        expect(oldEnergy - player.energy).toEqual(Energy.STANDARD * 2);

        // Regular stuff unaffected
        oldEnergy = player.energy;
        player.expend(Energy.MOVE);
        expect(oldEnergy - player.energy).toEqual(Energy.MOVE);
    });

// Slow means you spend twice as much energy moving
    test("Slow", () => {
        tgf.bestowAndVerify("Slow");
        let oldEnergy = player.energy;

        player.expend(Energy.MOVE);
        expect(oldEnergy - player.energy).toEqual(Energy.MOVE * 2);

        // Regular stuff unaffected
        oldEnergy = player.energy;
        player.expend(Energy.STANDARD);
        expect(oldEnergy - player.energy).toEqual(Energy.STANDARD);
    });

// Ongoing damage means you take... ongoing damage
    test("Ongoing Damage", () => {
        gm.endTurn(player, Energy.MINOR);
        tgf.bestowAndVerify("Ongoing", {amount: 3});
        let oldHp = player.hp;

        // fix for https://gitlab.com/rostrander/tyranny-of-the-labyrinth/-/issues/51
        let numMsgs = player.messages.length;

        // Ongoing stuff happens at the beginning of the character's turn,
        // saves at the end.
        tgf.nextTurn();
        expect(player.isReady()).toBeTruthy();
        expect(player.hp).toStrictEqual(oldHp - 3);
        // Only one message about damage
        expect(player.messages).toHaveLength(numMsgs + 1);

        // Saving throws
        gm.fixRolls(1);  // failure to save
        gm.endTurn(player, Energy.MINOR);
        // Still have condition
        let ongoing = player.hasConditionNamed("Ongoing")
        expect(ongoing).toBeTruthy();
        expect(ongoing.beginningOfTurnEffects[0].amount).toStrictEqual(3);

        tgf.nextTurn();
        expect(npc.isReady()).toBeFalsy();
        expect(player.isReady()).toBeTruthy();
        expect(player.hasConditionNamed("Ongoing")).toBeTruthy();
        expect(player.hp).toStrictEqual(oldHp - 6);

        gm.fixRolls(19);  // save!
        gm.endTurn(player, Energy.MINOR);
        // No have condition
        expect(player.hasConditionNamed("Ongoing")).toBeFalsy();
    });

// Weakened means you do half damage
    test("Weakened", () => {
        tgf.bestowAndVerify("Weakened");

        gm.fixRolls(19, 6);
        pc.bump({x: 1, y: 1});

        expect(npc.hp).toStrictEqual(9997);
    });

// Prone means your next move is spent getting up
    test("Prone", () => {
        let prone = tgf.bestowAndVerify("Prone");
        let originalLoc = Point.fromLoc(player.loc);
        expect(prone.duration).toStrictEqual(Duration.AlwaysOn);
        pc.wait();
        // Didn't count down at all
        expect(prone.duration).toStrictEqual(Duration.AlwaysOn);

        tgf.nextTurn();
        expect(pc.bump({x: 1, y: 0})).toBeTruthy();
        expect(originalLoc.sameAs(player.loc)).toBeTruthy(); // didn't move
        expect(player.isReady()).toBeFalsy();  // ended their turn
        expect(player.hasConditionNamed("Prone")).toBeFalsy();
    });

// Immobilized means you can't move at all
    test("Immobilized", () => {
        let immob = tgf.bestowAndVerify("Immobilized");
        let originalLoc = Point.fromLoc(player.loc);
        expect(immob.duration).toStrictEqual(Duration.SaveEnds);

        gm.fixRolls(1);  // fail saving throw (happens at EoT)
        expect(pc.bump({x: 1, y: 0})).toBeTruthy();
        expect(originalLoc.sameAs(player.loc)).toBeTruthy(); // didn't move
        expect(player.isReady()).toBeFalsy();  // ended their turn
        expect(player.hasConditionNamed("Immobilized")).toBeTruthy();

        gm.fixRolls(19); // succeed saving throw (when turn ends)
        tgf.nextTurn();
        expect(player.hasConditionNamed("Immobilized")).toBeTruthy();
        expect(pc.bump({x: 1, y: 0})).toBeTruthy();
        expect(originalLoc.sameAs(player.loc)).toBeTruthy(); // still can't move
        expect(player.isReady()).toBeFalsy();  // ended their turn again
        expect(player.hasConditionNamed("Immobilized")).toBeFalsy();

        tgf.nextTurn();
        expect(player.hasConditionNamed("Immobilized")).toBeFalsy();
        expect(pc.bump({x: 1, y: 0})).toBeTruthy();
        expect(originalLoc.sameAs(player.loc)).toBeFalsy(); // moved
    });

    // Impaired is a -2 (by default) penalty to (by default) everything
    test("Impaired", () => {
        let impair = tgf.bestowAndVerify("Impaired");
        gm.fixRolls(10);  // Normally would hit
        gm.fixRolls(1);  // fail saving throw (happens at EoT)
        expect(pc.bump({x: 1, y: 1})).toBeTruthy();
        expect(npc.hp).toStrictEqual(10000);

        tgf.nextTurn();
        gm.fixRolls(19, 3);  // Will hit but will do less damage
        gm.fixRolls(19); // succeed saving throw at EoT
        expect(pc.bump({x: 1, y: 1})).toBeTruthy();
        expect(npc.hp).toStrictEqual(9999);  // aforementioned less damage


        let conditionTemplate = CONDITIONS.Impaired;
        expect(conditionTemplate).toBeTruthy();
        let effectTemplate = {
            name: "bestowAndVerify",
            condition: conditionTemplate,
        }
        let bce = new BestowConditionEffect(effectTemplate);
        bce.invoke(player, npc, gm);
        impair = npc.hasConditionNamed(conditionTemplate.name);
        expect(impair).toBeTruthy();

        tgf.nextTurn();
        gm.fixRolls(9, 1);  // would normally miss, but enemy's AC is down
        expect(pc.bump({x: 1, y: 1})).toBeTruthy();
        expect(npc.hp).toStrictEqual(9998);
    });

    // Sleep means you can't act until you take damage or save
    test("Asleep", () => {
        expect(player.isReady()).toBeTruthy();
        gm.endTurn(player, Energy.MINOR);

        let sleep = tgf.bestowAndVerify("Asleep");

        // Fail the save
        gm.fixRolls(2);
        tgf.nextTurn();

        expect(player.isReady()).toBeFalsy();
        expect(gm.waitingForPlayer()).toBeFalsy();

        // Damage the player
        gm.damageCharacter(npc, player, Dice.fixed(2));
        expect(player.hasConditionNamed("Asleep")).toBeFalsy();
    });

    // Vulnerable means you take more damage
    test("Vulnerable", () => {
        player.hp = 10000;
        let vuln = tgf.bestowAndVerify("Vulnerable");
        player.takeDamage(Dice.fixed(2));
        expect(player.hp).toStrictEqual(9997);
    });

    // Penalty to saves
    test("Saveless", () => {
        gm.fixRolls(10);
        expect(player.savingThrow(gm)).toBeTruthy();
        let condition = tgf.bestowAndVerify("Saveless");

        gm.fixRolls(10);
        expect(player.savingThrow(gm)).toBeFalsy();
    });

    // Can't see
    test("Blind", () => {
        player.hp = 10000;
        // Can see by default
        expect(player.canSee()).toBeTruthy();
        expect(npc.canSee()).toBeTruthy();

        gm.forceMoveCharacter(npc, {x: 3, y: 3})
        tgf.bestowAndVerifyOn(npc, "Blind");
        expect(npc.canSee()).toBeFalsy();
        npc.setBehavior(new MeleeCombatBehavior(), gm);
        pc.wait();

        // This weirdness is because:
        // * I need that 9 so they fail their saving throw
        // * But I need their STR to be +1 so that if they try to take a swing
        //   below, it hits.  (Which they shouldn't do, so I want to know about it)
        npc.stats.str = 12;
        gm.fixRolls(9, 10, 2);
        tgf.nextTurn(npc);

        // Nothing should have happened, because the NPC can't see the player
        expect(npc.loc).toEqual({x: 3, y: 3});
        expect(player.hp).toStrictEqual(10000);

        gm.forceMoveCharacter(npc,{x: 2, y: 2});
        tgf.nextTurn(npc);

        // Now they did something
        expect(player.hp).toStrictEqual(9997); // Rolled 2, +1 stat
    });

    // Re-roll your dice, take the lowest
    test("Disadvantaged", () => {
        expect(player.advantageStatus()).toStrictEqual(0);
        tgf.bestowAndVerify("Disadvantaged");
        expect(player.advantageStatus()).toBeLessThan(0);
        gm.fixRolls(19, 2);
        expect(pc.attack(player.defaultMeleeAbility, npc.loc)).toBeTruthy();
        // But it missed because it rolled twice and picked the '2'
        expect(npc.hp).toStrictEqual(10000);
    });
});

describe("Buffs", () => {
    // Raises your AC
    test("Barkskin", () => {
        let potion = new UsableItem(POTIONS.BarkskinPotion);
        player.receiveItem(potion);
        let originalAc = player.calcDefense(Chain.ac).value;

        expect(pc.use(potion)).toBeTruthy();

        let newAc = player.calcDefense(Chain.ac).value;
        expect(newAc).toBeGreaterThan(originalAc);
    });

    describe("Extra damage roll", () => {
        test("Implied from attack", () => {
            let dmg = tgf.bestowAndVerify("ExtraDamageRoll");
            expect(dmg.duration).toStrictEqual(Duration.Encounter);

            gm.fixRolls(19, 6, 6);
            expect(pc.bump({x: 1, y: 1})).toBeTruthy();
            expect(npc.hp).toStrictEqual(9988);
        });

        test("Explicit in condition", () => {
            let dmg = tgf.bestowAndVerify("ExtraDamageRoll",
                {damage: '2d4'});
            // I'd really like to test that it's acutally rolling 2d4
            // but fixing the rolls overrides the '2' part of '2d4'
            gm.fixRolls(19, 2, 3);
            expect(pc.bump({x: 1, y: 1})).toBeTruthy();
            expect(npc.hp).toStrictEqual(9995);
        });
    });

    // All actions take half as much energy
    test("Hasted", () => {
        let condition = tgf.bestowAndVerify("Hasted");
        let oldEnergy = player.energy;

        let halfmove = Energy.MOVE / 2;
        player.expend(Energy.MOVE);
        expect(oldEnergy - player.energy).toEqual(halfmove);

        let halfminor = Math.max(Energy.MINOR / 2, Energy.MIN);
        oldEnergy = player.energy;
        player.expend(Energy.MINOR);
        expect(oldEnergy - player.energy).toEqual(halfminor);

        let halfstandard = Energy.STANDARD / 2;
        oldEnergy = player.energy;
        player.expend(Energy.STANDARD);
        expect(oldEnergy - player.energy).toEqual(halfstandard);
    });

    test("PlusToHit", () => {
        let condition = tgf.bestowAndVerify("PlusToHit");
        let hit = player.addToHitBonuses(Dice.zero, player);
        expect(hit.value).toStrictEqual(1);

        // Make sure stats work
        player.stats.con = 14; // +2
        condition.passiveEffects[0].amount = 'con';
        hit = player.addToHitBonuses(Dice.zero, player);
        expect(hit.value).toStrictEqual(2);

        // Make sure negative stats don't penalize
        player.stats.con = 9; // -1
        hit = player.addToHitBonuses(Dice.zero, player);
        expect(hit.value).toStrictEqual(0);
    });

    test("PlusDamage", () => {
        let condition = tgf.bestowAndVerify("PlusDamage");
        let dmg = player.addDamageBonuses(Dice.zero, player);
        expect(dmg.value).toStrictEqual(1);

        // Make sure stats work
        player.stats.str = 14; // +2
        condition.passiveEffects[0].amount = 'str';
        dmg = player.addDamageBonuses(Dice.zero, player);
        expect(dmg.value).toStrictEqual(2);

        // Make sure negative stats don't penalize
        player.stats.str = 9; // -1
        dmg = player.addDamageBonuses(Dice.zero, player);
        expect(dmg.value).toStrictEqual(0);
    });

    test("PlusDefense", () => {
        let ac = player.calcDefense(Chain.ac).value;
        let will = player.calcDefense(Chain.will).value;

        let condition = tgf.bestowAndVerify("PlusDefense");
        expect(player.calcDefense(Chain.ac).value).toStrictEqual(ac+1);
        expect(player.calcDefense(Chain.will).value).toStrictEqual(will);

        // Make sure all defenses works
        condition.passiveEffects[0].defense = Chain.AllDefenses;
        expect(player.calcDefense(Chain.ac).value).toStrictEqual(ac+1);
        expect(player.calcDefense(Chain.will).value).toStrictEqual(will+1);

        // Make sure stats work
        player.stats.con = 14; // +2
        condition.passiveEffects[0].amount = 'con';
        expect(player.calcDefense(Chain.ac).value).toStrictEqual(ac+2);
        expect(player.calcDefense(Chain.will).value).toStrictEqual(will+2);

        // Make sure negative stats don't penalize
        player.stats.con = 7; // -2
        expect(player.calcDefense(Chain.ac).value).toStrictEqual(ac);
        expect(player.calcDefense(Chain.will).value).toStrictEqual(will);
    });

    // You bestow a condition when you hit
    test("Inflicting", () => {
        let pcAc = player.calcDefense(Chain.ac).value;
        let npcAc = npc.calcDefense(Chain.ac).value;

        let override = {
            inflictedCondition: CONDITIONS.PlusDefense,
            inflictedOverride: {amount: -1}
        };
        let condition = tgf.bestowAndVerify("Inflicting", override);
        expect(player.calcDefense(Chain.ac).value).toStrictEqual(pcAc);
        expect(npc.calcDefense(Chain.ac).value).toStrictEqual(npcAc);
        gm.fixRolls(19);  // Make sure we hit
        expect(pc.attack(player.defaultMeleeAbility, npc.loc)).toBeTruthy();
        expect(npc.calcDefense(Chain.ac).value).toStrictEqual(npcAc-1);
    });

    // You move the enemy when you hit
    test("Pushy", () => {
        let origLoc = {x: npc.loc.x, y: npc.loc.y};
        let condition = tgf.bestowAndVerify("Pushy");
        gm.fixRolls(19);
        expect(pc.bump({x: 1, y: 1})).toBeTruthy();
        expect(npc.loc).not.toEqual(origLoc);
    })

    // You do damage when you are hit
    test("Thorny", () => {
        npc.hp = 1
        player.hp = 10000
        expect(gm.characters).toHaveLength(2); // player, npc

        tgf.bestowAndVerify("Thorny");
        npc.setBehavior(new MeleeCombatBehavior(), gm);
        pc.wait();
        gm.fixRolls(19, 2);
        tgf.nextTurn(npc);

        // Player should have taken damage
        expect(player.hp).toStrictEqual(9998);
        // But the enemy got the worse of it
        expect(npc.hp).toBeLessThan(1);
        expect(npc.alive).toBeFalsy();

        // Make sure they were removed
        expect(gm.characters).toHaveLength(1);
        expect(gm.map.tileAt(npc.loc).character).toBeFalsy();
    });

    // Resistant means you take less damage
    test("Resistant", () => {
        player.hp = 10000;
        let res = tgf.bestowAndVerify("Resistant");
        player.takeDamage(Dice.fixed(2));
        expect(player.hp).toStrictEqual(9999);

        // Make sure preventing all damage doesn't heal or something
        res.passiveEffects[0].amount = -10;
        player.takeDamage(Dice.fixed(2));
        expect(player.hp).toStrictEqual(9999);
    });

    // Gain some of your HP back when you hit
    test("Vampirism", () => {
        player.hp = 1;
        let vamp = tgf.bestowAndVerify("Vampirism");

        gm.fixRolls(19);
        expect(pc.bump({x: 1, y: 1})).toBeTruthy();
        expect(player.hp).toStrictEqual(3);

        // doesn't work on a miss
        tgf.nextTurn();
        gm.fixRolls(1);
        expect(pc.bump({x: 1, y: 1})).toBeTruthy();
        expect(player.hp).toStrictEqual(3);
    });

});

describe("Serialization", () => {
    let condition: Condition;

    beforeEach(() => {
        condition = new Condition(CONDITIONS.Ongoing);
        expect(condition).toBeTruthy();
    });

    test("Round trip", () => {
        let exported = jsonRoundTrip(condition);
        let newCondition = new Condition(exported);

        expect(newCondition).toMatchObject(condition);
        expect(newCondition.duration).toStrictEqual(exported.duration);
        let bleed = newCondition.beginningOfTurnEffects[0];
        expect(bleed).toBeTruthy();
        expect(bleed.constructor.name).toStrictEqual("RawDamageEffect")
    })
});
