import {TestGameFactory} from "../testutils";
import {GameMaster} from "../../gamemaster";
import {PlayerController} from "../../control";
import {Character} from "../../character";

let tgf: TestGameFactory;
let gm: GameMaster;
let pc: PlayerController;
let player: Character;
let npc: Character;

// Note that, even though these are explicitly non-boss monster abilities,
// the tests will be giving them to PCs because it's easier

beforeEach(() => {
    tgf = new TestGameFactory();
    ({player, gm} = tgf.testGameData());
    pc = new PlayerController(player, gm);
    npc = tgf.testMonster();
});

test("ImpLightning", () => {
    player.stats.int = 14;  // Shouldn't apply bonus damage, tho
    let ability = player.gainAbility("ImpLightning");

    // This attack shouldn't roll anything, but if it does,
    // I want it to hit.  And it also shouldn't roll for damage,
    // so I want it to do lots:
    gm.fixRolls(19, 50);

    expect(pc.attack(ability, npc.loc)).toBeTruthy();
    expect(npc.hp).toStrictEqual(9999); // 1 damage only
});

test("MinionStrike", () => {
    player.stats.int = 14;  // Shouldn't apply bonus damage, tho
    let ability = player.gainAbility("MinionStrike");

    // This attack only rolls to hit (the '19') but shouldn't roll
    // for damage, so if that happens I want it to be noticeable (the '50')
    gm.fixRolls(19, 50);

    expect(pc.attack(ability, npc.loc)).toBeTruthy();
    expect(npc.hp).toStrictEqual(9998); // 2 damage only
});

test("MonsterStrike", () => {
    player.stats.str = 12; // +1
    let ability = player.gainAbility("MonsterStrike");
    gm.fixRolls(19, 2);

    expect(pc.attack(ability, npc.loc)).toBeTruthy();
    expect(npc.hp).toStrictEqual(9997);  // rolled 2, +1 str
    expect(ability.currentCooldown).toStrictEqual(1);
});

test("MonsterRanged", () => {
    player.stats.dex = 14; // +2
    let ability = player.gainAbility("MonsterRanged");
    gm.fixRolls(19, 2);

    expect(pc.attack(ability, npc.loc)).toBeTruthy();
    expect(npc.hp).toStrictEqual(9996);  // rolled 2, +2 str
    expect(ability.currentCooldown).toStrictEqual(1);
});