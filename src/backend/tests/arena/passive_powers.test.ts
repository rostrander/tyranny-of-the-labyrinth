import {TestGameFactory} from "../testutils";
import {GameMaster} from "../../gamemaster";
import {Character} from "../../character";
import {WEAPONS} from "../../items/weapons";
import {Weapon} from "../../items/item";
import {PlayerController} from "../../control";
import {Chain, Energy} from "../../common";
import {Dice} from "../../dice";

let tgf: TestGameFactory;
let gm: GameMaster;
let player: Character;
let pc: PlayerController;
let npc: Character;

beforeEach(() => {
    tgf = new TestGameFactory();
    ({player, gm} = tgf.testGameData());
    pc = new PlayerController(player, gm);
    npc = tgf.testMonster();
});

describe("Einhander", () => {
    test("Happy path", () => {
        tgf.bestowAndVerifyPassive("Einhander");
        tgf.equip(new Weapon(WEAPONS.Mace));

        gm.fixRolls(9, 1); // Normally would miss
        expect(pc.bump({x: 1, y: 1})).toBeTruthy();
        expect(npc.hp).toStrictEqual(9998);  // +1 damage too
    });

    test("Grants no bonus if you're not wielding a 1h weapon", () => {
        tgf.bestowAndVerifyPassive("Einhander");
        tgf.equip(new Weapon(WEAPONS.Greatsword));
        gm.fixRolls(9); // Should miss
        expect(pc.bump({x: 1, y: 1})).toBeTruthy();
        expect(npc.hp).toStrictEqual(10000);
    });

    test("Grants no bonus at range", () => {
        tgf.bestowAndVerifyPassive("Einhander");
        tgf.equip(new Weapon(WEAPONS.Mace));
        gm.forceMoveCharacter(npc, {x: 4, y: 4});
        gm.fixRolls(9); // Should miss
        expect(pc.attack(player.defaultRangedAbility, {x: 4, y: 4})).toBeTruthy();
        expect(npc.hp).toStrictEqual(10000);
    });
});

describe("Zweihander", () => {
    test("Happy path", () => {
        tgf.bestowAndVerifyPassive("Zweihander");
        tgf.equip(new Weapon(WEAPONS.Greatsword));

        gm.fixRolls(9, 1); // Normally would miss
        expect(pc.bump({x: 1, y: 1})).toBeTruthy();
        expect(npc.hp).toStrictEqual(9998);  // +1 damage too
    });

    test("Grants no bonus if you're not wielding a 2h weapon", () => {
        tgf.bestowAndVerifyPassive("Zweihander");
        tgf.equip(new Weapon(WEAPONS.Mace));
        gm.fixRolls(9); // Should miss
        expect(pc.bump({x: 1, y: 1})).toBeTruthy();
        expect(npc.hp).toStrictEqual(10000);
    });

    test("Grants no bonus at range", () => {
        tgf.bestowAndVerifyPassive("Zweihander");
        tgf.equip(new Weapon(WEAPONS.Greatsword));
        gm.forceMoveCharacter(npc, {x: 4, y: 4});
        gm.fixRolls(9); // Should miss
        expect(pc.attack(player.defaultRangedAbility, {x: 4, y: 4})).toBeTruthy();
        expect(npc.hp).toStrictEqual(10000);
    });
});

test("Toughness grants more HP per level", () => {
    let origHp = player.maxHp;
    tgf.bestowAndVerifyPassive("Toughness");
    let lvl1Hp = player.maxHp;
    expect(lvl1Hp).toBeGreaterThan(origHp);
    player.levelUp();
    expect(player.maxHp).toBeGreaterThan(lvl1Hp);
});

test("Bloodied does more damage when bloody", () => {
    gm.fixRolls(19, 2);
    expect(pc.bump({x: 1, y: 1})).toBeTruthy();
    expect(npc.hp).toStrictEqual(9998);  // normal

    tgf.nextTurn();
    tgf.bestowAndVerifyPassive("BloodRage");
    gm.fixRolls(19, 2);
    player.hp = 1;
    expect(pc.bump({x: 1, y: 1})).toBeTruthy();
    expect(npc.hp).toStrictEqual(9994); // 2 damage, +2 from feat
});

test("Bloodseeker does more damage to bloodied enemies", () => {
    tgf.bestowAndVerifyPassive("Bloodseeker");
    gm.fixRolls(19, 2);
    npc.hp = 4000;
    expect(pc.bump({x: 1, y: 1})).toBeTruthy();
    expect(npc.hp).toStrictEqual(3996); // 2 damage, +2 from feat
});

test("Punching Down does more damage to lesser enemies", () => {
    tgf.bestowAndVerifyPassive("PunchingDown");
    gm.fixRolls(19, 2);
    npc.baseMaxHp = 1;
    expect(npc.maxHp).toBeLessThan(player.maxHp);
    expect(pc.bump({x: 1, y: 1})).toBeTruthy();
    expect(npc.hp).toStrictEqual(9997); // 2 damage, +1 from feat
});

test("Defiant rage does more damage to healthier enemies", () => {
    tgf.bestowAndVerifyPassive("DefiantRage");
    gm.fixRolls(19, 2);
    expect(npc.maxHp).toBeGreaterThan(player.maxHp);

    expect(pc.bump({x: 1, y: 1})).toBeTruthy();
    expect(npc.hp).toStrictEqual(9996); // 2 damage, +2 from feat
});

test("Defy the strong grants AC vs more maxHP", () => {
    expect(player.calcDefense(Chain.ac, npc).value).toStrictEqual(10);
    tgf.bestowAndVerifyPassive("DefyTheStrong");
    expect(player.calcDefense(Chain.ac, npc).value).toStrictEqual(11);
    // Normal defense and other defenses unaffected
    expect(player.calcDefense(Chain.ac).value).toStrictEqual(10);
    expect(player.calcDefense(Chain.will,npc).value).toStrictEqual(10);
});

test("Disdain the weak grants AC vs less maxHP", () => {
    npc.baseMaxHp = 1;
    expect(player.calcDefense(Chain.ac, npc).value).toStrictEqual(10);
    tgf.bestowAndVerifyPassive("DisdainTheWeak");
    expect(player.calcDefense(Chain.ac, npc).value).toStrictEqual(11);
    // Normal defense and other defenses unaffected
    expect(player.calcDefense(Chain.ac).value).toStrictEqual(10);
    expect(player.calcDefense(Chain.will,npc).value).toStrictEqual(10);
});

test("Healer results in you being healed more", () => {
    player.hp = 1;
    expect(player.hp).toStrictEqual(1);
    tgf.bestowAndVerifyPassive("Healer");
    player.heal(1);
    expect(player.hp).toStrictEqual(3);
});

test("CautiousCadence", () => {
    expect(player.calcDefense(Chain.ac, npc).value).toStrictEqual(10);
    tgf.bestowAndVerifyPassive("CautiousCadence");
    expect(player.calcDefense(Chain.ac, npc).value).toStrictEqual(10);

    expect(pc.bump({x: 1, y: 0})).toBeTruthy();
    expect(player.calcDefense(Chain.ac, npc).value).toStrictEqual(12);

})

test("Runner", () => {
    tgf.bestowAndVerifyPassive("Runner");
    let oldEnergy = player.energy;
    let target = Energy.MOVE - 1;

    player.expend(Energy.MOVE);
    expect(oldEnergy - player.energy).toEqual(target);
});

describe("Tradeoffs", () => {
    test("Curse of Recklessness", () => {
        // Curse of Recklessness: -1 tohit, +1 damage
        tgf.bestowAndVerifyPassive("CurseOfRecklessness");
        gm.fixRolls(10); // Would ordinarily hit, but -1 misses
        expect(pc.bump({x: 1, y: 1})).toBeTruthy();
        expect(npc.hp).toStrictEqual(10000);

        tgf.nextTurn();
        gm.fixRolls(11, 2);
        expect(pc.bump({x: 1, y: 1})).toBeTruthy();
        expect(npc.hp).toStrictEqual(9997); // 2 damage, +1 feat
    });

    test("Curse of Caution", () => {
        tgf.bestowAndVerifyPassive("CurseOfCaution");
        gm.fixRolls(9, 2); // Would ordinarily miss, but +1 hits
        expect(pc.bump({x: 1, y: 1})).toBeTruthy();
        expect(npc.hp).toStrictEqual(9999);  // 2 damage, -1 feat
    });

    test("Greater Recklessness", () => {
        tgf.bestowAndVerifyPassive("GreaterRecklessness");
        gm.fixRolls(12); // Ordinarily would hit but wow -3
        expect(pc.bump({x: 1, y: 1})).toBeTruthy();
        expect(npc.hp).toStrictEqual(10000);

        tgf.nextTurn();
        gm.fixRolls(13, 2);
        expect(pc.bump({x: 1, y: 1})).toBeTruthy();
        expect(npc.hp).toStrictEqual(9996); // 2 damage, +2 feat
    });

    test("Greater Caution", () => {
        tgf.bestowAndVerifyPassive("GreaterCaution");
        gm.fixRolls(8, 4); // Ordinarily would miss but +2
        expect(pc.bump({x: 1, y: 1})).toBeTruthy();
        expect(npc.hp).toStrictEqual(9999); // 4 damage, -3 feat
    });

    describe("Curses stack", () => {
        test("Reckless", () => {
            tgf.bestowAndVerifyPassive("CurseOfRecklessness");
            tgf.bestowAndVerifyPassive("GreaterRecklessness");
            gm.fixRolls(13); // Ordinarily would hit but -4!
            expect(pc.bump({x: 1, y: 1})).toBeTruthy();
            expect(npc.hp).toStrictEqual(10000);

            tgf.nextTurn();
            gm.fixRolls(19, 2);
            expect(pc.bump({x: 1, y: 1})).toBeTruthy();
            expect(npc.hp).toStrictEqual(9995); // 2 damage, +3 feat
        });

        test("Cautious", () => {
            tgf.bestowAndVerifyPassive("CurseOfCaution");
            tgf.bestowAndVerifyPassive("GreaterCaution");
            gm.fixRolls(7, 5); // Ordinarily would miss but +3
            expect(pc.bump({x: 1, y: 1})).toBeTruthy();
            expect(npc.hp).toStrictEqual(9999); // 5 damage, -4 feat
        });
    });

    describe("Curses cancel", () => {
        test("Lesser", () => {
            tgf.bestowAndVerifyPassive("CurseOfRecklessness");
            tgf.bestowAndVerifyPassive("CurseOfCaution");
            gm.fixRolls(9);  // Should miss as usual
            expect(pc.bump({x: 1, y: 1})).toBeTruthy();
            expect(npc.hp).toStrictEqual(10000);

            tgf.nextTurn();
            gm.fixRolls(10, 2);  // Should hit as usual
            expect(pc.bump({x: 1, y: 1})).toBeTruthy();
            expect(npc.hp).toStrictEqual(9998);  // standard damage
        });

        test("Greater", () => {
            tgf.bestowAndVerifyPassive("GreaterRecklessness");
            tgf.bestowAndVerifyPassive("GreaterCaution");
            gm.fixRolls(10);  // Net -1 to hit
            expect(pc.bump({x: 1, y: 1})).toBeTruthy();
            expect(npc.hp).toStrictEqual(10000);

            tgf.nextTurn();
            gm.fixRolls(11, 2);  // Should hit
            expect(pc.bump({x: 1, y: 1})).toBeTruthy();
            expect(npc.hp).toStrictEqual(9999);  // 2 damage, net -1 feat
        });
    });
});

describe('Adjacency', function () {
    test("Defy the odds", () => {
        expect(player.calcDefense(Chain.ac).value).toStrictEqual(10);
        tgf.bestowAndVerifyPassive("DefyTheOdds");
        // One enemy is not enough
        expect(player.calcDefense(Chain.ac).value).toStrictEqual(10);
        // Two enemies is
        let npc2 = tgf.testMonster({
            loc: {x: player.loc.x, y: player.loc.y+1}
        });
        expect(player.calcDefense(Chain.ac).value).toStrictEqual(11);
        expect(player.calcDefense(Chain.ref).value).toStrictEqual(11);
        // More does nothing
        let npc3 = tgf.testMonster({
            loc: {x: player.loc.x+1, y: player.loc.y}
        });
        expect(player.calcDefense(Chain.ac).value).toStrictEqual(11);

        // should go down if I move them
        gm.forceMoveCharacter(npc, {x: 5, y: 2});
        gm.characterDied(npc2);  // what, it's a kind of movement!
        gm.characterDied(npc3);
        expect(player.calcDefense(Chain.ac).value).toStrictEqual(10);
    });

    test("Wall Shield", () => {
        expect(player.calcDefense(Chain.ac).value).toStrictEqual(10);
        tgf.bestowAndVerifyPassive("WallShield");
        // Player's already adjacent to upperleft, upper, upperright, left,
        // and lowerleft - max 4 only counts 4 of those.
        expect(player.calcDefense(Chain.ac).value).toStrictEqual(14);
        expect(pc.bump({x: 0, y: 1})).toBeTruthy();
        // Now only upperleft, left, and lowerleft
        expect(player.calcDefense(Chain.ac).value).toStrictEqual(13);
    });

    test("Cornered", () => {
        expect(player.addDamageBonuses(Dice.zero, npc).value).toStrictEqual(0);
        tgf.bestowAndVerifyPassive("Cornered");
        // As above:
        expect(player.addDamageBonuses(Dice.zero, npc).value).toStrictEqual(4);
        expect(pc.bump({x: 0, y: 1})).toBeTruthy();
        expect(player.addDamageBonuses(Dice.zero, npc).value).toStrictEqual(3);
    });

    test("Isolate", () => {
        expect(player.addDamageBonuses(Dice.zero, npc).value).toStrictEqual(0);
        tgf.bestowAndVerifyPassive("Isolate");
        expect(player.addDamageBonuses(Dice.zero, npc).value).toStrictEqual(2);
        let npc2 = tgf.testMonster({
            loc: {x: npc.loc.x, y: npc.loc.y+1}
        });
        expect(player.addDamageBonuses(Dice.zero, npc).value).toStrictEqual(0);
    });

    test("Point blank", () => {
        expect(player.addDamageBonuses(Dice.zero, npc).value).toStrictEqual(0);
        expect(player.addToHitBonuses(Dice.zero, npc).value).toStrictEqual(0);
        tgf.bestowAndVerifyPassive("PointBlank");

        // No change, we're using an improvised weapon
        expect(player.addDamageBonuses(Dice.zero, npc).value).toStrictEqual(0);
        expect(player.addToHitBonuses(Dice.zero, npc).value).toStrictEqual(0);

        tgf.equip(new Weapon(WEAPONS.Longbow));
        expect(player.addDamageBonuses(Dice.zero, npc).value).toStrictEqual(1);
        expect(player.addToHitBonuses(Dice.zero, npc).value).toStrictEqual(1);

    })
});