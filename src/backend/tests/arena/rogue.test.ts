import {TestGameFactory} from "../testutils";
import {GameMaster} from "../../gamemaster";
import {PlayerController} from "../../control";
import {Character} from "../../character";
import {Ability} from "../../abilities/ability";
import {Chain, Duration, Point} from "../../common";
import {MeleeCombatBehavior} from "../../behavior";
import {XY} from "../../interfaces";

let tgf: TestGameFactory;
let gm: GameMaster;
let pc: PlayerController;
let player: Character;
let npc: Character;

beforeEach(() => {
    tgf = new TestGameFactory();
    ({player, gm} = tgf.testGameData());
    pc = new PlayerController(player, gm);
    npc = tgf.testMonster();
})

describe("T1", () => {
    let ability: Ability;

    afterEach(() => {
        expect(ability.tier).toStrictEqual(1);
    });

    describe("Instant", () => {
        test("Darting Shiv", () => {
            ability = player.gainAbility("DartingShiv");
            player.stats.dex = 18; // +4
            gm.forceMoveCharacter(npc, {x: 3, y: 3});
            let originalLoc = Point.fromLoc(player.loc);

            gm.fixRolls(19, 2);
            expect(pc.attack(ability, npc.loc)).toBeTruthy();
            expect(npc.hp).toStrictEqual(9994);  // rolled 2, +4 stat
            let expectedLoc = {x: originalLoc.x + 1, y: originalLoc.y + 1};
            expect(expectedLoc).toEqual(player.loc);
        });

        test("Reflexive Shiv", () => {
            ability = player.gainAbility("ReflexiveShiv");
            player.stats.dex = 9; // -1
            gm.fixRolls(19, 2);
            expect(pc.attack(ability, npc.loc)).toBeTruthy();
            expect(npc.hp).toStrictEqual(9999); // rolled 2, -1 stat
        });

        test("Insult to Injury", () => {
            ability = player.gainAbility("InsultToInjury");
            player.stats.dex = 12; // +1
            player.stats.cha = 14; // +2
            gm.fixRolls(19, 2);
            expect(pc.attack(ability, npc.loc)).toBeTruthy();
            expect(npc.hp).toStrictEqual(9995); // rolled 2, +2 cha, +1 dex
        });

        test("Exploit Attack", () => {
            ability = player.gainAbility("ExploitAttack");
            player.stats.dex = 12; // +1
            player.stats.cha = 14; // +2
            gm.fixRolls(19, 2);
            expect(pc.attack(ability, npc.loc)).toBeTruthy();
            expect(npc.hp).toStrictEqual(9997); // rolled 2, +1 dex

            npc.setBehavior(new MeleeCombatBehavior(), gm);
            gm.fixRolls(19, 2);
            player.hp = 10000;
            tgf.nextTurn(npc);

            expect(player.hp).toStrictEqual(9998);
            expect(npc.hp).toStrictEqual(9995); // CHA thorns
        });
    }); // Instant

    describe("Encounter", () => {
        afterEach(() => {
            expect(ability.currentCooldown).toStrictEqual(Duration.Encounter);
        });

        test("Dazing Shiv", () => {
            ability = player.gainAbility("DazingShiv");
            player.stats.dex = 9; // -1
            gm.fixRolls(19, 2);
            expect(pc.attack(ability, npc.loc)).toBeTruthy();
            expect(npc.hp).toStrictEqual(9999); // rolled 2, -1 stat
            expect(npc.hasConditionNamed("Dazed")).toBeTruthy();
        })

        test("Shifting Shiv", () => {
            let origPlayer = player.loc;
            let origNpc = npc.loc;
            ability = player.gainAbility("ShiftingShiv");
            player.stats.dex = 9; // -1
            gm.fixRolls(19, 2, 2);
            expect(pc.attack(ability, npc.loc)).toBeTruthy();
            expect(npc.hp).toStrictEqual(9997); // rolled 4, -1 stat

            expect(npc.loc).toEqual(origPlayer);
            expect(player.loc).toEqual(origNpc);
        });

        test("Brute Shiv", () => {
            ability = player.gainAbility("BruteShiv");
            player.stats.dex = 12; // +1
            player.stats.str = 14; // +2
            gm.fixRolls(19, 1, 1);
            expect(pc.attack(ability, npc.loc)).toBeTruthy();
            expect(npc.hp).toStrictEqual(9995); // rolled 2, +1 dex, +2 str
        });

        test("Defensive Shiv", () => {
            ability = player.gainAbility("DefensiveShiv");
            player.stats.dex = 16; // +3
            expect(player.calcDefense(Chain.ac).value).toStrictEqual(13);

            gm.fixRolls(19, 2, 3);
            expect(pc.attack(ability, npc.loc)).toBeTruthy();
            expect(npc.hp).toStrictEqual(9992); // rolled 5, +3 stat
            expect(player.calcDefense(Chain.ac).value).toStrictEqual(15);

        })
    }); // Encounter

    describe("Boss", () => {
        afterEach(() => {
            expect(ability.currentCooldown).toStrictEqual(Duration.Boss);
        });

        test("Pocket Sand", () => {
            ability = player.gainAbility("PocketSand");
            player.stats.dex = 12; // +1

            // need another NPC
            let npc2 = tgf.testMonster({
                loc: {x: 3, y: 2},
                baseMaxHp: 5000,
                name: 'npc2',
            });
            let npc3 = tgf.testMonster({
                loc: {x: 5, y: 3},
                baseMaxHp: 4000,
                name: "npc3",
            });

            gm.fixRolls(19, 19, 19);
            expect(pc.attack(ability, npc.loc)).toBeTruthy();

            expect(npc.hp).toBeLessThan(10000);
            expect(npc.hasConditionNamed("Blind")).toBeTruthy();

            expect(npc2.hp).toBeLessThan(5000);
            expect(npc.hasConditionNamed("Blind")).toBeTruthy();

            expect(npc3.hp).toStrictEqual(4000);
            expect(npc3.hasConditionNamed("Blind")).toBeFalsy();

        });

        test("Bulldozer", () => {
            ability = player.gainAbility("Bulldozer");
            let basic = player.defaultMeleeAbility;
            player.assignDefaultMeleeAbility(ability);
            player.stats.dex = 14; // +2

            let originalLoc = {x: npc.loc.x, y: npc.loc.y};

            gm.fixRolls(19, 1, 2, 3);
            expect(pc.attack(ability, npc.loc)).toBeTruthy();

            expect(npc.hp).toStrictEqual(9992); // Rolled 6, +2 stat
            // I don't think the mode will trigger now, but there's a built-in push anyway
            expect(npc.loc).not.toEqual(originalLoc);

            originalLoc = {x: npc.loc.x, y: npc.loc.y};

            tgf.nextTurn();
            gm.forceMoveCharacter(player, {x: 2, y: 2});
            gm.fixRolls(19);
            expect(pc.attack(basic, npc.loc)).toBeTruthy();
            expect(npc.hp).toBeLessThan(9992);
            expect(npc.loc).not.toEqual(originalLoc);
        });
    }); // Boss
}); // T1

describe("T2", () => {
    let ability: Ability;

    afterEach(() => {
        expect(ability.tier).toStrictEqual(2);
    });

    describe("Instant", () => {
        afterEach(() => {
            expect(ability.currentCooldown).toBeLessThanOrEqual(1);
        });

        test("Dart", () => {
            ability = player.gainAbility("Dart");
            let oldloc = {x: player.loc.x, y: player.loc.y};
            let newloc = {x: 3, y: 3};
            expect(pc.attack(ability, newloc)).toBeTruthy();

            expect(player.loc).toStrictEqual(newloc);
            expect(gm.map.tileAt(player.loc).character).toBe(player);
            expect(gm.map.tileAt(oldloc).character).toBeFalsy();

            tgf.nextTurn();
            // Can't telefrag
            expect(pc.attack(ability, npc.loc)).toBeFalsy();
        });

        test("Hobbling Shiv", () => {
            ability = player.gainAbility("HobblingShiv");
            player.stats.dex = 14; // +2
            gm.fixRolls(19, 2);
            expect(pc.attack(ability, npc.loc)).toBeTruthy();
            expect(npc.hp).toStrictEqual(9996); // Rolled 2, +2 stat
            expect(npc.hasConditionNamed("Slow")).toBeTruthy();
        });

        test("Slicing Shiv", () => {
            ability = player.gainAbility("SlicingShiv");
            player.stats.dex = 12; // +1
            gm.fixRolls(19, 6);
            expect(pc.attack(ability, npc.loc)).toBeTruthy();
            expect(npc.hp).toStrictEqual(9993); // rolled 6, +1 stat
            expect(npc.hasConditionNamed("Ongoing")).toBeTruthy();
        })

    }); // Instant

    describe("Encounter", () => {
        afterEach(() => {
            expect(ability.currentCooldown).toStrictEqual(Duration.Encounter);
        });

        test("Pinning Shiv", () => {
            ability = player.gainAbility("PinningShiv");
            player.stats.dex = 9; // -1
            gm.fixRolls(19, 5, 1);
            expect(pc.attack(ability, npc.loc)).toBeTruthy();
            expect(npc.hp).toStrictEqual(9995); // rolled 6, -1 stat
            expect(npc.hasConditionNamed("Immobilized")).toBeTruthy();
        });

        test("Wide Shiv", () => {
            ability = player.gainAbility("WideShiv");
            player.stats.dex = 12; // +1

            // need another NPC
            let npc2 = tgf.testMonster({
                loc: {x: 3, y: 2},
                baseMaxHp: 5000,
                name: 'npc2',
            });
            let npc3 = tgf.testMonster({
                loc: {x: 5, y: 3},
                baseMaxHp: 4000,
                name: "npc3",
            });

            gm.fixRolls(19, 19, 4, 3);
            expect(pc.attack(ability, npc.loc)).toBeTruthy();
            expect(npc.hp).toStrictEqual(9995);
            expect(npc2.hp).toStrictEqual(4996);
            expect(npc3.hp).toStrictEqual(4000); // out of range
        });

        test("Setup Shiv", () => {
            ability = player.gainAbility("SetupShiv");
            player.stats.dex = 17; // +3

            gm.fixRolls(19, 2, 1);
            expect(pc.attack(ability, npc.loc)).toBeTruthy();
            expect(npc.hp).toStrictEqual(9994); // 3, +3 dex
            expect(npc.calcDefense(Chain.ac).value).toStrictEqual(8);
            expect(npc.calcDefense(Chain.ref).value).toStrictEqual(8);
            expect(npc.calcDefense(Chain.will).value).toStrictEqual(10);
        });
    }); // Encounter

    describe("Boss", () => {
        afterEach(() => {
            expect(ability.currentCooldown).toStrictEqual(Duration.Boss);
        });

        test("Fast Forward", () => {
            ability = player.gainAbility("FastForward");

            expect(player.energy).toStrictEqual(0);
            expect(pc.selfAbility(ability)).toBeTruthy();
            expect(player.energy).toBeLessThan(-1);
            tgf.nextTurn();

            expect(player.energy).toStrictEqual(0);
            expect(pc.bump({x: 1, y: 0})).toBeTruthy();
            expect(player.energy).toStrictEqual(-1);
        });

        test("Stay Down", () => {
            ability = player.gainAbility("StayDown");
            player.stats.dex = 9; // -1
            gm.fixRolls(19, 5, 1);
            expect(pc.attack(ability, npc.loc)).toBeTruthy();
            expect(npc.hp).toStrictEqual(9995); // rolled 6, -1 stat
            expect(npc.hasConditionNamed("Prone")).toBeTruthy();
            expect(npc.hasConditionNamed("Inflicting")).toBeTruthy();
            npc.removeConditionNamed("Prone");
            expect(npc.hasConditionNamed("Prone")).toBeFalsy();

            tgf.nextTurn();
            gm.fixRolls(19, 2);
            expect(pc.attack(player.defaultMeleeAbility, npc.loc)).toBeTruthy();

            expect(npc.hp).toStrictEqual(9993); // rolled 2 (stat is STR in this case)
            expect(npc.hasConditionNamed("Prone")).toBeTruthy();
        });
    })
}); // T2

describe("T3", () => {
    let ability: Ability;

    afterEach(() => {
        expect(ability.tier).toStrictEqual(3);
    });

    describe("Instant", () => {
        afterEach(() => {
            expect(ability.currentCooldown).toBeLessThanOrEqual(1);
        });

        describe("Retreating Shiv", () => {
            let origLoc: XY;

            beforeEach(() => {
                origLoc = {x: 4, y: 1};
                // Move everyone so we can see retreats actually happen
                gm.forceMoveCharacter(player, origLoc);
                gm.forceMoveCharacter(npc, {x: 5, y: 1});

                ability = player.gainAbility("RetreatingShiv");
                player.stats.dex = 12; // +1
            })

            test("Miss", () => {
                gm.fixRolls(4);  // Guarantee a miss
                expect(pc.attack(ability, npc.loc)).toBeTruthy();
                expect(npc.hp).toStrictEqual(10000);
                expect(player.loc).toEqual(origLoc);  // No movement
            });

            test("Hit", () => {
                gm.fixRolls(19, 2);
                expect(pc.attack(ability, npc.loc)).toBeTruthy();
                expect(npc.hp).toStrictEqual(9997); // rolled 2, +1 stat
                expect(player.loc).not.toEqual(origLoc);
            });
        });

        test("Stuck Shiv", () => {
            // Stuck Shiv: autohit, ongoing 2
            ability = player.gainAbility("StuckShiv");
            gm.fixRolls(1); // shouldn't roll but if it does I want it to miss
            expect(pc.attack(ability, npc.loc)).toBeTruthy();
            expect(npc.hasConditionNamed("Ongoing")).toBeTruthy();

            tgf.nextTurn(npc);
            expect(npc.hp).toStrictEqual(9998);
        });
    }); // Instant

    describe("Encounter", () => {
        afterEach(() => {
            expect(ability.currentCooldown).toStrictEqual(Duration.Encounter);
        });

        describe("AoE", () => {
            let origLoc: XY;
            let origLoc2: XY;
            let origLoc3: XY;
            let npc2: Character;
            let npc3: Character;

            beforeEach(() => {
                origLoc = {x: npc.loc.x, y: npc.loc.y};
                origLoc2 = {x: player.loc.x, y: player.loc.y + 2};
                origLoc3 = {x: 5, y: 3};
                npc2 = tgf.testMonster({
                    loc: origLoc2,
                    baseMaxHp: 5000,
                    name: 'npc2',
                });
                npc3 = tgf.testMonster({
                    loc: origLoc3,
                    baseMaxHp: 4000,
                    name: "npc3",
                });
                gm.fixRolls(19, 19);
            });

            test("Freeing Shiv", () => {
                // Freeing Shiv: PBaoE 2, Dex vs Ref, 1W+DEX, knockback 2
                ability = player.gainAbility("FreeingShiv");

                expect(pc.selfAbility(ability)).toBeTruthy();

                expect(npc.hp).toBeLessThan(10000);
                expect(npc2.hp).toBeLessThan(5000);
                expect(npc3.hp).toStrictEqual(4000);

                expect(npc.loc).not.toEqual(origLoc);
                expect(npc2.loc).not.toEqual(origLoc2);
                expect(npc3.loc).toEqual(origLoc3);
            });

            test("Challenging Shiv", () => {
                // Challenging Shiv: PBaoE 3, Dex vs Ref, 2W+DEX, pull 3
                ability = player.gainAbility("ChallengingShiv");

                expect(pc.selfAbility(ability)).toBeTruthy();

                expect(npc.hp).toBeLessThan(10000);
                expect(npc2.hp).toBeLessThan(5000);
                expect(npc3.hp).toStrictEqual(4000);

                expect(npc.loc).toEqual(origLoc);  // already next to you
                expect(npc2.loc).not.toEqual(origLoc2);
                expect(npc2.loc).toEqual({x: 1, y: 2});
                expect(npc3.loc).toEqual(origLoc3);
            });
        });
    }); // Encounter

    describe("Boss", () => {
        afterEach(() => {
            expect(ability.currentCooldown).toStrictEqual(Duration.Boss);
        });

        test("Extra Shiv", () => {
            // Extra Shiv: Gain buff, +1d6 Damage
            ability = player.gainAbility("ExtraShiv");

            expect(pc.selfAbility(ability)).toBeTruthy();
            expect(player.hasConditionNamed("ExtraDamageRoll"));
        });
    });
});