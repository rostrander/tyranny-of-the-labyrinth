import {ABILITIES, IAbilityTemplate} from "../../abilities/ability";
import {Targets} from "../../common";

describe("All abilities have a tier", () => {
    const table = ABILITIES.all.map(b => [b.template.name, (b.template as IAbilityTemplate)]);
    test.each(table)
    ('Has a tier: %s', (name: string, template: IAbilityTemplate) => {
        expect(template.tier).not.toBeUndefined();
        // Non-boss monsters have tier-0 abilities
        expect(template.tier).toBeGreaterThanOrEqual(0);
    });
})

describe("All ranged powers have a range", () => {
    const ranged = ABILITIES.all.filter(b => (b.template as IAbilityTemplate).targeting === Targets.Ranged);
    const table = ranged.map(r => [r.template.name, (r.template as IAbilityTemplate)]);
    test.each(table)
    ('is ranged: %s',
        (name: string, r: IAbilityTemplate) => {
            expect(r.range).not.toBeUndefined();
            expect(r.range).toBeGreaterThan(0);
        }
    );
});

describe("All AoE Powers have a radius", () => {
    const aoe = ABILITIES.all.filter(b => {
        let template = b.template as IAbilityTemplate;
        return template.targeting === Targets.PBAoE ||
            template.targeting === Targets.TAoE;
    });
    const table = aoe.map(a => [
        a.template.name,
        (a.template as IAbilityTemplate)
    ]);
    test.each(table)
    ("Has a radius: %s", (name: string, t: IAbilityTemplate) => {
        expect(t.radius).not.toBeUndefined();
        // Zero radius is possible for e.g. small zones
        expect(t.radius).toBeGreaterThanOrEqual(0);
    })
})