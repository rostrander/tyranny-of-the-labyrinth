import {Character} from "../../character";
import {GameMaster} from "../../gamemaster";
import {StandThereBehavior} from "../../behavior";
import {PlayerController} from "../../control";
import {Dice} from "../../dice";
import {Weapon} from "../../items/item";
import {WEAPONS} from "../../items/weapons";
import {TestGameFactory} from "../testutils";
import {Duration} from "../../common";

let tgf: TestGameFactory;
let gm: GameMaster;
let pc: PlayerController;
let player: Character;
let npc: Character;

beforeEach(() => {
    tgf = new TestGameFactory();
    ({player, gm} = tgf.testGameData());
    pc = new PlayerController(player, gm);
    npc = tgf.testMonster();
})

describe("Adventurer", () => {
    test("Basic Melee Attack", () => {

        expect(npc.alive).toBeTruthy();
        expect(gm.characters).toContain(npc);
        expect(gm.map.tileAt(npc.loc).character).not.toBeNull();
        expect(npc.messages).toHaveLength(0);
        expect(player.messages).toHaveLength(0);
        let originalEnergy = player.energy;

        player.stats.str = 14; // +2
        gm.fixRolls(19, 6);
        npc.hp = 8;
        pc.bump({x: 1, y: 1});
        expect(npc.alive).toBeFalsy();
        expect(gm.map.tileAt(npc.loc).character).toBeNull();
        expect(gm.characters).not.toContain(npc);

        expect(npc.messages).not.toHaveLength(0);
        expect(player.messages).not.toHaveLength(0);

        // Also, it should no longer be the PC's turn
        expect(player.isReady()).toBeFalsy();
        let newEnergy = player.energy;
        expect(newEnergy).toBeLessThan(originalEnergy);
    });

    test("Basic Melee Attack uses the player's weapon", () => {

        let weaponForAbility = jest.fn().mockImplementation(() => {
            return new Weapon(WEAPONS.Mace);
        })
        player.weaponForAbility = weaponForAbility;

        gm.fixRolls(19);
        pc.bump({x: 1, y: 1});

        expect(weaponForAbility).toHaveBeenCalled();
    });

    test("Basic Ranged Attack", () => {
        gm.moveCharacter(npc, {x: 5, y: 5});
        expect(npc.alive).toBeTruthy();
        expect(gm.characters).toContain(npc);
        expect(gm.map.tileAt(npc.loc).character).not.toBeNull();
        expect(npc.messages).toHaveLength(0);
        expect(player.messages).toHaveLength(0);
        let originalEnergy = player.energy;

        player.stats.dex = 16; // +3
        gm.fixRolls(18, 5);
        npc.hp = 8;
        expect(pc.attack(player.defaultRangedAbility, npc.loc)).toBeTruthy();
        expect(npc.alive).toBeFalsy();
        expect(gm.map.tileAt(npc.loc).character).toBeNull();
        expect(gm.characters).not.toContain(npc);

        expect(npc.messages).not.toHaveLength(0);
        expect(player.messages).not.toHaveLength(0);

        // Also, it should no longer be the PC's turn
        expect(player.isReady()).toBeFalsy();
        let newEnergy = player.energy;
        expect(newEnergy).toBeLessThan(originalEnergy);
    });

    test("Basic Heal", () => {
        player.hp = 1;

        let heal = player.gainAbility("BasicHeal");
        expect(heal.currentCooldown).toStrictEqual(0);
        let msgs = player.messages.length;
        // If the GM is rolling dice to hit us, make sure it does, because they
        // shouldn't be doing that.
        gm.fixRolls(19);

        expect(pc.selfAbility(heal)).toBeTruthy();

        expect(player.hp).toStrictEqual(4);
        expect(heal.currentCooldown).toStrictEqual(Duration.Encounter);
        // One "You use...", one for "You were healed..."
        expect(player.messages).toHaveLength(msgs+2);
    });

    test("Hit Harder", () => {
        let ability = player.gainAbility("HitHarder");
        expect(ability.currentCooldown).toStrictEqual(0);
        player.assignDefaultMeleeAbility(ability);

        gm.fixRolls(19, 1, 1);
        expect(pc.bump({x: 1, y: 1})).toBeTruthy();

        expect(npc.hp).toStrictEqual(9998);
        expect(ability.currentCooldown).toStrictEqual(Duration.Encounter);
    });

    test("Hit Even Harder", () => {
        let ability = player.gainAbility("HitEvenHarder");
        expect(ability.currentCooldown).toStrictEqual(0);
        player.assignDefaultMeleeAbility(ability);

        gm.fixRolls(19, 1, 1, 1);
        expect(pc.bump({x: 1, y: 1})).toBeTruthy();

        expect(npc.hp).toStrictEqual(9997);
        expect(ability.currentCooldown).toStrictEqual(Duration.Boss);
    });

    describe("T2", () => {
        test("Outmoving", () => {
            let ability = player.gainAbility("Outmoving");
            expect(ability.tier).toStrictEqual(2);
            player.assignDefaultMeleeAbility(ability);

            player.stats.dex = 12; // +1
            gm.fixRolls(19, 1);
            expect(pc.bump({x: 1, y: 1})).toBeTruthy();

            expect(npc.hp).toStrictEqual(9998);
        });

        test("Outsmarting", () => {
            let ability = player.gainAbility("Outsmarting");

            player.stats.int = 12; // +1
            gm.fixRolls(19, 1);
            expect(pc.attack(ability, npc.loc)).toBeTruthy();

            expect(npc.hp).toStrictEqual(9998);
        });

        test("Outthinking", () => {
            let ability = player.gainAbility("Outthinking");
            player.assignDefaultMeleeAbility(ability);
            expect(ability.currentCooldown).toStrictEqual(0);
            player.stats.wis = 12; // +1

            gm.fixRolls(19, 1);
            expect(pc.bump({x: 1, y: 1})).toBeTruthy();

            expect(npc.hp).toStrictEqual(9998);
            expect(ability.currentCooldown).toStrictEqual(Duration.Encounter);
        });

        test("Outtalking", () => {
            let ability = player.gainAbility("Outtalking");

            expect(ability.currentCooldown).toStrictEqual(0);
            player.stats.cha = 12; // +1

            gm.fixRolls(19, 2);
            expect(pc.attack(ability, npc.loc)).toBeTruthy();

            expect(npc.hp).toStrictEqual(9997);
            expect(ability.currentCooldown).toStrictEqual(Duration.Encounter);
        });

        test("Outstanding", () => {
            let ability = player.gainAbility("Outstanding");
            expect(ability.currentCooldown).toStrictEqual(0);
            player.defaultMeleeAbility = ability;
            player.stats.con = 12; // +1

            gm.fixRolls(19, 3);
            expect(pc.bump({x: 1, y: 1})).toBeTruthy();

            expect(npc.hp).toStrictEqual(9996);
            expect(ability.currentCooldown).toStrictEqual(Duration.Boss);
        });
    });
});
