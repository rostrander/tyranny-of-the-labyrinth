import {BOSS_ROOM_TEMPLATE_CARVED, BOSS_ROOM_TEMPLATE_FILLED, Tile, TileMap, Zone} from "../world";
import {Armor, Item} from "../items/item";
import * as ROT from "rot-js";
import {Character} from "../character";
import {jsonRoundTrip, TestGameFactory} from "./testutils";
import {ARMOR} from "../items/armors";
import {BoundingBox, Chain} from "../common";
import {IEffectTemplate, PassiveEffect} from "../effects/effect";
import {IZoneTemplate} from "../interfaces";
import {GameMaster} from "../gamemaster";

describe("TileMap", () => {
    let emptyMap: TileMap;

// Actual tests begin
    beforeEach(() => {
        emptyMap = new TileMap();
    });

    describe("blockingTileAt", () => {

        test("Happy path", () => {
            let newTile = new Tile('#', {x: 0, y: 1}, false);
            emptyMap.setTile(newTile);
            let oob = emptyMap.blockingTileAt(newTile);
            expect(oob).toStrictEqual(newTile);
        });

        test("Passable tiles return null", () => {
            let newTile = new Tile('#', {x: 0, y: 1}, true);
            emptyMap.setTile(newTile);
            let oob = emptyMap.blockingTileAt(newTile);
            expect(oob).toBeNull();
        });

        test("Uninitialized returns the NULL tile", () => {
            let oob = emptyMap.blockingTileAt({x: 0, y: 1});
            expect(oob).toStrictEqual(Tile.NULL);
        });

        test("Out of bounds returns the NULL tile", () => {
            let oob = emptyMap.blockingTileAt({x: -1, y: -1});
            expect(oob).toStrictEqual(Tile.NULL);
        });

    });

    describe("locationInBounds", () => {
        test("In bounds", () => {
            expect(emptyMap.locationInBounds({x: 1, y: 1})).toBeTruthy();
        });

        test("Out of bounds negative", () =>{
            expect(emptyMap.locationInBounds({x: -1, y: 0})).toBeFalsy();
        });

        test("Out of bounds too big", () => {
            expect(emptyMap.locationInBounds({x: 0, y: 5000})).toBeFalsy();
        });

        test("Out of bounds on edge", () => {
            const loc = {x: emptyMap.width, y: emptyMap.height};
            expect(emptyMap.locationInBounds(loc)).toBeFalsy();
        });
    });

    describe("rotjsMapCallback", () => {
        test("Happy path", () => {
            let callback = emptyMap.rotjsMapCallback();
            // Every generator has 0 = open and 1 = wall
            // (Though Cellular would work better as the opposite)
            callback(0, 0, 0);
            let newTile = emptyMap.tileAt({x: 0, y: 0});
            expect(newTile).not.toStrictEqual(Tile.NULL);
            expect(newTile.passable).toBeTruthy();

            callback(1, 0, 1);
            let newerTile = emptyMap.tileAt({x: 1, y: 0});
            expect(newerTile).not.toStrictEqual(Tile.NULL);
            expect(newerTile).not.toStrictEqual(newTile);
            expect(newerTile.passable).toBeFalsy();
        });
    });

    describe("setTile", () => {
        let newTile: Tile;

        beforeEach(() => {
            newTile = new Tile('.', {x: 0, y: 1}, true);
        });

        test("Happy path", () => {
            emptyMap.setTile(newTile, {x: 0, y: 2});
            expect(emptyMap.tileAt({x: 0, y : 2})).toStrictEqual(newTile);
            // Also that should update the tile's location
            expect(newTile.y).toEqual(2);
        });

        test("Happy path default location", () => {
            emptyMap.setTile(newTile);
            expect(emptyMap.tileAt(newTile)).toStrictEqual(newTile);
            expect(newTile.y).toEqual(1);
        });

        test("Tile adds/removes itself to open list appropriately", () => {
            expect(emptyMap.open.size).toStrictEqual(0);
            emptyMap.setTile(newTile);
            expect(emptyMap.open.size).toStrictEqual(1);
            expect(emptyMap.open).toContainEqual(JSON.stringify(newTile.asXY()));
            let worseTile = new Tile('.', {x: 0, y: 1}, false);
            emptyMap.setTile(worseTile);
            expect(emptyMap.open.size).toStrictEqual(0);
        });

    });

    describe("Open list", () => {
        let newTile: Tile;

        beforeEach(() => {
            newTile = new Tile('.', {x: 3, y: 4}, true);
        });

        test("If no tiles are open, random returns null", () => {
            let chosen = emptyMap.randomOpenTile();
            expect(chosen).toBeFalsy();
        });

        test("RandomSpawnable returns null if nothing is open", () => {
            let chosen = emptyMap.randomSpawnableTile();
            expect(chosen).toBeFalsy();
        });

        describe("Map with open spaces", () => {
            let upTile: Tile;

            beforeEach(() => {
                emptyMap.setTile(newTile);
                upTile = new Tile(".", {x: 3, y: 3}, true);
                emptyMap.setTile(upTile);
            });

            test("Random open chooses from open list", () => {
                let chosen = emptyMap.randomOpenTile();
                expect(chosen).toBeTruthy();
                expect(chosen.x).toStrictEqual(3);
                expect(chosen.y).toBeGreaterThanOrEqual(3);
                expect(chosen.y).toBeLessThanOrEqual(4);
            });

            test("RandomSpawnable chooses from open list", () => {
                let chosen = emptyMap.randomSpawnableTile();
                expect(chosen).toBeTruthy();
                expect(chosen.x).toStrictEqual(3);
                expect(chosen.y).toBeGreaterThanOrEqual(3);
                expect(chosen.y).toBeLessThanOrEqual(4);
            });

            test("RandomSpawnable ignores lit tiles", () => {
                upTile.lit = 1;
                let chosen = emptyMap.randomSpawnableTile();
                expect(chosen).toBe(newTile);
            });

            test("RandomSpawnable ignores occupied tiles", () => {
                let tgf = new TestGameFactory();
                let monster = tgf.basicTestMonster();
                newTile.character = monster;

                let chosen = emptyMap.randomSpawnableTile();
                expect(chosen).toBe(upTile);
            });

            test("RandomSpawnable gives up if it tries too hard", () => {
                upTile.lit = 1;
                newTile.lit = 1;

                let chosen = emptyMap.randomSpawnableTile();
                expect(chosen).toBeFalsy();
            });
        })

    });

    describe("items", () => {
        let item: Item;
        let map: TileMap;
        let tgf: TestGameFactory;

        beforeEach(() => {
            tgf = new TestGameFactory();
            item = tgf.junkItem();
            let mapGen = new ROT.Map.Arena(40, 40);
            map = new TileMap(40, 40);
            mapGen.create(map.rotjsMapCallback());
        });

        test("Place an item", () => {
            let place = {x: 3, y: 3};
            let testtile = map.tileAt(place);
            expect(testtile.contents).toHaveLength(0);

            map.placeItem(item, place);
            expect(testtile.contents).toHaveLength(1);
            expect(testtile.contents[0]).toBe(item);
        });

        test("Can't place an item on null tile", () => {
            let place = {x: -3, y: -3};
            let nulltile = map.tileAt(place);
            expect(nulltile.contents).toHaveLength(0);

            expect(map.placeItem(item, place)).toBeFalsy();
            expect(nulltile.contents).toHaveLength(0);
        });

        test("Placing an item changes the displayed character", () => {
            let place = {x: 3, y: 3};
            let testtile = map.tileAt(place);
            map.placeItem(item, place);

            expect(testtile.displayedChar()).toStrictEqual("?");
        });

        test("Characters override placed items", () => {
            let place = {x: 3, y: 3};
            let testtile = map.tileAt(place);
            map.placeItem(item, place);

            let npc = tgf.basicTestMonster();
            testtile.character = npc;

            expect(testtile.displayedChar()).toStrictEqual('k');
            // But not if you can't see them
            expect(testtile.displayedChar(false)).toStrictEqual('?');

        });
    });

    describe("Serialization", () => {
        let tgf: TestGameFactory;
        let map: TileMap;
        let gm: GameMaster;

        beforeEach(() => {
            tgf = new TestGameFactory();
            ({map, gm} = tgf.testGameData());
        });

        test("TileMap no problems", () => {
            expect(() => {JSON.stringify(map)}).not.toThrow();
        });

        test("Tiles exclusions on save", () => {
            let charTile = map.tileAt({x: 1, y: 1});
            expect(charTile.character).toBeTruthy();
            let exported = jsonRoundTrip(charTile);
            expect(exported['character']).toBeFalsy();
            expect(exported['zone']).toBeFalsy();
        });

        test("Tile roundtrip", () => {
            let junk = tgf.junkItem();
            let someTile = map.tileAt({x: 1, y: 2});
            someTile.receive(junk);
            let armor = new Armor(ARMOR.Shield);
            someTile.receive(armor);

            let exported = jsonRoundTrip(someTile);
            let newTile = Tile.fromJson(exported);
            expect(newTile).toEqual(someTile);
            // Make sure new tile contents are real
            expect(newTile.contents[0].getItemType).not.toBeUndefined();
            // Make sure it preserved type
            expect(newTile.contents[1].getItemType()).toStrictEqual("Armor");
        });

        test("Map roundtrip", () => {
            let junk = tgf.junkItem();
            let someTile = map.tileAt({x: 1, y: 2});
            someTile.receive(junk);
            let armor = new Armor(ARMOR.Shield);
            someTile.receive(armor);

            map.placeRooms(1);
            gm.recalculateLighting();
            expect(map.placedRooms).toStrictEqual(1);

            let exported = jsonRoundTrip(map);
            expect(exported.rooms).toBeTruthy();

            let newMap = TileMap.fromJson(exported);
            expect(newMap.height).toStrictEqual(map.height);
            expect(newMap.width).toStrictEqual(map.width);
            for(let x=0; x < map.width; x++) {
                for(let y=0; y < map.height; y++) {
                    const xy = {x, y}
                    let oldTile = map.tileAt(xy);
                    if(!oldTile.character) {
                        expect(oldTile).toEqual(newMap.tileAt(xy));
                    }
                }
            }
            // The set of lit tiles should be the same, as `setTile` does that
            // for us
            expect(newMap.lit.size).toEqual(map.lit.size);
            // Likewise the set of open tiles
            expect(newMap.open).toEqual(map.open);
            // Rooms should be identical
            expect(newMap.placedRooms).toStrictEqual(1);
            expect(newMap.rooms[0]).toEqual(map.rooms[0]);
            expect(newMap.rooms[1]).toEqual(map.rooms[1]);
        });
    });

    describe("Rooms", () => {
        let map: TileMap;
        let tgf: TestGameFactory;

        beforeEach(() => {
            tgf = new TestGameFactory();
            ({map} = tgf.testGameData());
        });

        test("Can't place room even partially off-map", () => {
            let where = BoundingBox.centered({x: 1, y: 1, w: 5, h: 3});
            expect(where.x).toStrictEqual(-1);
            expect(where.y).toStrictEqual(0);

            expect(map.canPlaceRoomAt(where)).toBeFalsy();
        });

        test("Can't place intersecting rooms", () => {
            map = new TileMap(40, 40);
            expect(map.rooms).toHaveLength(0);
            let center = {x: 7, y: 5};
            let template = BOSS_ROOM_TEMPLATE_CARVED;
            expect(map.placeAndAddRoomCentered(center, template)).toBeTruthy();

            expect(map.rooms).toHaveLength(1);

            let where = BoundingBox.centered({
                x: center.x + 3,
                y: center.y + 4,
                w: template[0].length, h: template.length,
            });
            expect(map.canPlaceRoomAt(where)).toBeFalsy();

            expect(map.placeAndAddRoomCentered(where, template)).toBeFalsy();
            expect(map.rooms).toHaveLength(1);
        });

        test("Placing a room in a regular arena", () => {
            map = new TileMap(40, 40);
            let template = BOSS_ROOM_TEMPLATE_FILLED;
            let center = {x: 15, y: 15}

            let centerTile = map.tileAt(center);
            expect(centerTile.isNullTile()).toBeTruthy();

            expect(map.placeAndAddRoomCentered(center, template)).toBeTruthy();
            centerTile = map.tileAt(center);
            expect(centerTile.isNullTile()).toBeFalsy();
            expect(centerTile.passable).toBeFalsy();
        });

        test("Placing a room in a void", () => {
            map = new TileMap(20, 20);
            expect(map.rooms).toHaveLength(0);
            let center = {x: 7, y: 5}

            let centerTile = map.tileAt(center);
            expect(centerTile.passable).toBeFalsy();
            expect(centerTile.isNullTile()).toBeTruthy();

            // Adding an already-open tile in the correction zone
            // to make sure it stays that way
            let tile = Tile.glyphToTile(' ', {x: 1, y: 10});
            map.setTile(tile);

            // Place the room
            let template = BOSS_ROOM_TEMPLATE_CARVED;
            expect(map.placeAndAddRoomCentered(center, template)).toBeTruthy();
            centerTile = map.tileAt(center);
            expect(centerTile.isNullTile()).toBeFalsy();
            expect(centerTile.passable).toBeTruthy();

            // Check open space border
            let bb = BoundingBox.centered({
                x: center.x, y: center.y,
                w: template[0].length, h: template.length,
            });
            let ul = bb.upperLeft;
            let checkLoc = {x: ul.x, y: ul.y - 1};
            tile = map.tileAt(checkLoc);
            expect(tile.isNullTile()).toBeFalsy();
            expect(tile.passable).toBeTruthy();

            checkLoc = {x: ul.x - 1, y: ul.y};
            tile = map.tileAt(checkLoc);
            expect(tile.isNullTile()).toBeFalsy();
            expect(tile.passable).toBeTruthy();

            // Test to see that the space around that is real but nonpassable
            checkLoc = {x: ul.x - 2, y: ul.y}
            tile = map.tileAt(checkLoc);
            expect(tile.isNullTile()).toBeFalsy();
            expect(tile.passable).toBeFalsy();

            // Check the corner case
            checkLoc = {x: ul.x - 2, y: ul.y - 2}
            tile = map.tileAt(checkLoc);
            expect(tile.isNullTile()).toBeFalsy();
            expect(tile.passable).toBeFalsy();

            // Check that original spot we opened
            checkLoc = {x: 1, y: 10};
            tile = map.tileAt(checkLoc);
            expect(tile.isNullTile()).toBeFalsy();
            expect(tile.passable).toBeTruthy();

            expect(map.rooms).toHaveLength(1);
        });
    });
});

describe("Zone", () => {
    let zone: Zone;
    let pe: IEffectTemplate;

    beforeEach(() => {
        zone = new Zone({
            center: {x: 3, y: 3},
            radius: 1,
        });
        pe = {
            name: 'DefenseEffect',
            amount: 2,
            defense: Chain.ac,
        };
    });

    test("Default no passive effects", () => {
        expect(zone.passiveEffects).toBeTruthy();
        expect(zone.passiveEffects).toHaveLength(0);
    });

    test("Passive effects realized", () => {
        zone = new Zone({
            center: {x: 1, y: 1}, radius: 1,
            passiveEffects: [pe]
        });
        expect(zone.passiveEffects[0]).toBeInstanceOf(PassiveEffect);
    });

    test("Serialization", () => {
        zone = new Zone({
            center: {x: 1, y: 1}, radius: 1,
            passiveEffects: [pe]
        });
        let exported = jsonRoundTrip(zone);
        expect(exported).toMatchObject(zone);
        let newZone = new Zone(exported);
        expect(newZone.passiveEffects).toHaveLength(1)
        expect(newZone.passiveEffects[0]).toBeInstanceOf(PassiveEffect);
    });

});