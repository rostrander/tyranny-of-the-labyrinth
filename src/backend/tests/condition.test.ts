import {Character} from "../character";
import {PlayerController} from "../control";
import {GameMaster} from "../gamemaster";
import {UsableItem} from "../items/item";
import {POTIONS} from "../items/potions";
import {Chain, Duration} from "../common";
import {Condition, CONDITIONS} from "../effects/condition";
import {jsonRoundTrip, TestGameFactory} from "./testutils";

// This is the test for the Condition class overall.
// Specific conditions are in arena/conditions.test.ts
describe("Conditions / Buffs", () => {
    let tgf: TestGameFactory;
    let player: Character;
    let gm: GameMaster;
    let pc: PlayerController;

    beforeEach(() => {
        tgf = new TestGameFactory();
        ({player, gm} = tgf.testGameData());
        pc = new PlayerController(player, gm);
    });

    describe("Duration", () => {
        let cond: Condition;

        beforeEach(() => {
            cond = new Condition(CONDITIONS.Barkskin);
            player.receiveCondition(cond);
        });

        test("Conditions wear off", () => {
            cond.duration = Duration.Instant;
            expect(player.conditions).toHaveLength(1);
            let originalAc = player.calcDefense(Chain.ac).value;

            expect(pc.wait()).toBeTruthy();

            expect(cond.duration).toStrictEqual(0);
            expect(player.conditions).toHaveLength(0);
            let newAc = player.calcDefense(Chain.ac).value;
            expect(newAc).toBeLessThan(originalAc);

        });
    });

    describe("Serialization", () => {
        // for https://gitlab.com/rostrander/tyranny-of-the-labyrinth/-/issues/80
        test("onBehalfOf doesn't result in loops", () => {
            let cond = tgf.bestowAndVerify("Ongoing");

            // The normal way this would get its 'onBehalfOf' set is that
            // the character would chain through its conditions with itself
            // and the condition's chain would set it, but this is faster:
            cond.onBehalfOf = player;

            let exported = jsonRoundTrip(cond);
            expect(exported).toBeTruthy();
            expect(exported.onBehalfOf).toBeFalsy();
        });

        // Since effects don't have separate tests (instead being tested as
        // part of e.g. conditions), I'm putting the circular JSON detection
        // for them here
        test("Passive effect onBehalfOf doesn't result in loops", () => {
            let cond = tgf.bestowAndVerify("Barkskin");
            cond.onBehalfOf = player;
            let pe = cond.passiveEffects[0];
            pe.onBehalfOf = player;

            let exported = jsonRoundTrip(cond);
            expect(exported).toBeTruthy();
            expect(exported.onBehalfOf).toBeFalsy();
        });

        test("Active effect onBehalfOf doesn't result in loops", () => {
            let cond = tgf.bestowAndVerify("Ongoing");
            cond.onBehalfOf = player;
            let eff = cond.beginningOfTurnEffects[0];
            eff.onBehalfOf = player;

            let exported = jsonRoundTrip(cond);
            expect(exported).toBeTruthy();
            expect(exported.onBehalfOf).toBeFalsy();
        });
    });

});