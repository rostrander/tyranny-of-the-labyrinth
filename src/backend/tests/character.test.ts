import {Character} from "../character";
import {Chain, Duration, Energy, EquipmentTag, LastAction, Targets} from "../common";
import {ABILITIES, Ability, IAbilityTemplate, registerAbility} from "../abilities/ability";
import {GameMaster} from "../gamemaster";
import {Armor, IArmorTemplate, NothingEquipment, NOTHINGNAME, Weapon} from "../items/item";
import {ARMOR} from "../items/armors";
import {WEAPONS} from "../items/weapons";
import {jsonRoundTrip, TestGameFactory} from "./testutils";
import {StandThereBehavior} from "../behavior";
import {Condition, CONDITIONS} from "../effects/condition";
import {CLASSES} from "../classes";
import {Zone} from "../world";
import {IEffectTemplate} from "../effects/effect";
import {Faction} from "../interfaces";
import {PASSIVES} from "../effects/passive_powers";
import {Dice} from "../dice";
import {PlayerController} from "../control";

@registerAbility
export class TestAbility extends Ability {
    constructor() {
        super({
            name:"Test",
            targeting: Targets.None,
            description: "A test ability",
        });
    }

    invoke(fromWho: Character, toWho: Character, gm: GameMaster): boolean {
        return false;
    }
}

describe("Character", () => {
    let joe: Character;

    beforeEach(() => {
        joe = new Character({
            char: '@',
            name: 'Joe',
            baseMaxHp: 10,
            hpPerLevel: 2,
            description: "A test player",
        });
    });

    test("Handles adventurer template correctly", () => {
        // The adventurer doesn't have any abilities at all; the player
        // selects Basic Melee Attack and Basic Ranged Attack as part
        // of the tutorial.  This test makes sure that works:
        let adventurer = new Character(CLASSES.Adventurer);
        expect(adventurer.abilityNames).toHaveLength(0);
        expect(adventurer.abilities).toHaveLength(0);
        expect(adventurer.defaultMeleeAbility).toBeFalsy();
        expect(adventurer.defaultRangedAbility).toBeFalsy();

        // However, when the player picks the power, it'll be automatically assigned
        let bma = ABILITIES.templateHolderFromName("BasicMeleeAttack").template as IAbilityTemplate;
        adventurer.gainAbilityFromTemplate(bma);
        expect(adventurer.abilityNames).toHaveLength(1);
        expect(adventurer.abilities).toHaveLength(1);
        expect(adventurer.defaultMeleeAbility).toBeTruthy();
        expect(adventurer.defaultMeleeAbility.name).toStrictEqual(bma.name);
        expect(adventurer.defaultRangedAbility).toBeFalsy();

        // No matter how I give them the power:
        adventurer.gainAbility("BasicRangedAttack");
        expect(adventurer.abilityNames).toHaveLength(2);
        expect(adventurer.abilities).toHaveLength(2);
        expect(adventurer.defaultRangedAbility).toBeTruthy();
        expect(adventurer.defaultRangedAbility.name).toStrictEqual("BasicRangedAttack");

        // That shouldn't affect the template itself, though:
        expect(CLASSES.Adventurer).not.toBe(adventurer);
        expect(CLASSES.Adventurer.abilityNames).toHaveLength(0);
    });

    describe("Stats", () => {
        test("By default all are 10", () => {
            const stats = "str,dex,con,int,wis,cha".split(",");
            for(let stat of stats) {
                expect(joe.stats[stat]).toStrictEqual(10);
            }
        });

        test("Unknown stats crash", () => {
            expect(() => joe.statBonus('joe')).toThrow(/joe/);
        });

        test("Stat bonuses work as expected", () => {
            // default 0
            expect(joe.statBonus('str')).toStrictEqual(0);
            // Positive
            joe.stats.int = 14;
            expect(joe.statBonus('int')).toStrictEqual(2);
            // Bonuses round down
            joe.stats.dex = 11;
            joe.stats.con = 9;
            joe.stats.wis = 1;
            expect(joe.statBonus('dex')).toStrictEqual(0);
            expect(joe.statBonus('con')).toStrictEqual(-1);
            expect(joe.statBonus('wis')).toStrictEqual(-5);
        });

        describe("Matter", () => {
            test("STR affects fort", () => {
                let fort = joe.calcDefense(Chain.fort).value;
                joe.stats.str = 16; // +3

                expect(joe.calcDefense(Chain.fort).value).toStrictEqual(fort+3);

                // Lesser CON doesn't count
                joe.stats.con = 14; // +2
                expect(joe.calcDefense(Chain.fort).value).toStrictEqual(fort+3);
            });

            test("DEX affects AC", () => {
                let ac = joe.calcDefense(Chain.ac).value;
                joe.stats.dex = 18; // +4
                expect(joe.calcDefense(Chain.ac).value).toStrictEqual(ac+4);

                // lesser INT doesn't count
                joe.stats.int = 8; // -1
                expect(joe.calcDefense(Chain.ac).value).toStrictEqual(ac+4);

                // Heavy armor nullifies this
                let plate = ARMOR.FullPlate;
                joe.equipment.body = new Armor(plate);
                expect(joe.calcDefense(Chain.ac).value).toStrictEqual(ac+8);
            });

            test("DEX affects reflex", () => {
                let ref = joe.calcDefense(Chain.ref).value;
                joe.stats.dex = 12; // +1
                expect(joe.calcDefense(Chain.ref).value).toStrictEqual(ref+1);

                // I just realized I don't have to test that the lower stat
                // doesn't subsume the greater, I'm implicitly doing so already
                // because the lower stat is '10' by default
            });

            test("CON affects fort", () => {
                let fort = joe.calcDefense(Chain.fort).value;
                joe.stats.con = 17; // +3

                expect(joe.calcDefense(Chain.fort).value).toStrictEqual(fort+3);

                // Lesser STR doesn't count
                joe.stats.str = 14; // +2
                expect(joe.calcDefense(Chain.fort).value).toStrictEqual(fort+3);
            });

            test("CON affects HP", () => {
                let maxHp = joe.maxHp;
                joe.stats.con = 14; // +2
                expect(joe.maxHp).toStrictEqual(maxHp + 2);

                let levelTwoHp = joe.maxHp + joe.hpPerLevel + 2;
                joe.levelUp();
                expect(joe.maxHp).toStrictEqual(levelTwoHp);
            });

            test("INT affects AC", () => {
                let ac = joe.calcDefense(Chain.ac).value;
                joe.stats.int = 19; // +4
                expect(joe.calcDefense(Chain.ac).value).toStrictEqual(ac+4);

                // lesser DEX doesn't count
                joe.stats.dex = 9; // -1
                expect(joe.calcDefense(Chain.ac).value).toStrictEqual(ac+4);

                // Heavy armor nullifies this
                let plate = ARMOR.FullPlate;
                joe.equipment.body = new Armor(plate);
                expect(joe.calcDefense(Chain.ac).value).toStrictEqual(ac+8);
            });

            test("INT affects reflex", () => {
                let ref = joe.calcDefense(Chain.ref).value;
                joe.stats.int = 13; // +1
                expect(joe.calcDefense(Chain.ref).value).toStrictEqual(ref+1);
            });

            test('WIS affects will', () => {
                let will = joe.calcDefense(Chain.will).value;
                joe.stats.wis = 15; // +2
                expect(joe.calcDefense(Chain.will).value).toStrictEqual(will+2);
            });

            test("CHA affects will", () => {
                let will = joe.calcDefense(Chain.will).value;
                joe.stats.cha = 16; // +3
                expect(joe.calcDefense(Chain.will).value).toStrictEqual(will+3);
            });
        });
    });

    test("Assigns hotkeys to non-basic moves automatically", () => {
        for(let hotkey of joe.hotkeys) {
            expect(hotkey).toBeFalsy();
        }

        let heal = joe.gainAbility("BasicHeal");
        for(let i=0; i < joe.hotkeys.length; i++) {
            let hotkey = joe.hotkeys[i]
            if(i === 0) {
                expect(hotkey).toBe(heal);
            } else {
                expect(hotkey).toBeFalsy();
            }
        }

    });

    test("Leveling up", () => {
        let origLevel = joe.level;
        let origHp = joe.maxHp;
        let origStr = joe.stats.str;
        joe.hp = 1;

        joe.levelUp();

        expect(joe.level).toStrictEqual(origLevel + 1);
        expect(joe.hp).toStrictEqual(joe.maxHp)
        expect(joe.maxHp).toStrictEqual(origHp + joe.hpPerLevel);
        expect(joe.stats.str).toStrictEqual(origStr);
    });

    describe("Equipment", () => {

        test("Start with nothing", () => {
            expect(joe.equipment.body.name).toBe(NOTHINGNAME);
        });

        describe("Armor", () => {
            test("Increases AC", () => {
                expect(joe.calcDefense(Chain.ac).value).toStrictEqual(10);

                let padding = ARMOR.PaddedArmor;
                joe.equipment.body = new Armor(padding);

                expect(joe.calcDefense(Chain.ac).value).toStrictEqual(10 + padding.ac);
            });
        });

        describe("Weapons", () => {
            test("Does more damage", () => {
                let dice1 = joe.weaponDice(joe.defaultMeleeAbility);
                expect(dice1.d).toStrictEqual(4);  // default unarmed

                let wpn = WEAPONS.Mace;
                joe.equipment.mainHand = new Weapon(wpn);

                expect(joe.weaponDice(joe.defaultMeleeAbility).d).toStrictEqual(6); // mace
            });

            test("weaponDice are different objects", () => {
                let wpn = WEAPONS.Mace;
                joe.equipment.mainHand = new Weapon(wpn);

                let dice1 = joe.weaponDice(joe.defaultMeleeAbility);
                let dice2 = joe.weaponDice(joe.defaultMeleeAbility);
                expect(dice1).not.toBe(dice2);

                dice1.add(1, "test");
                expect(dice1.additions).toHaveLength(1);
                expect(dice1.additions).not.toBe(dice2.additions);
                expect(dice2.additions).toHaveLength(0);
            });
        });

        describe("Proficiency", () => {
            let tgf: TestGameFactory;
            let gm: GameMaster;
            let pc: PlayerController;
            let player: Character;

            beforeEach(() => {
                tgf = new TestGameFactory();
                ({player, gm} = tgf.testGameData());
                pc = new PlayerController(player, gm);
            })

            test("Always proficient in frw, rings, amulets", () => {
                player.proficiencies = [];

                for(let slot of ['head', 'feet', 'waist', 'neck', 'ring1', 'hands']) {
                    let template: IArmorTemplate = {
                        ac: 12,
                        slots: [slot],
                        name: 'test',
                        char: '?',
                        desc: 'test',
                    }
                    tgf.equip(new Armor(template));
                }
            });

            test("'all' proficiency allows everything", () => {
                for(let wpn of Object.values(WEAPONS)) {
                    if(wpn.name === "ImprovisedWeapon") continue;
                    let equipped = tgf.equip(new Weapon(wpn));
                    expect(pc.unequip(equipped.slots[0])).toBeTruthy();
                }
                for(let armor of Object.values(ARMOR)) {
                    tgf.equip(new Armor(armor));
                }
            });

            test("Light Armor", () => {
                player.proficiencies = [EquipmentTag.LightArmor];

                tgf.equip(new Armor(ARMOR.PaddedArmor));
                expect(tgf.equip(new Armor(ARMOR.LeatherArmor), false)).toBeFalsy();
                // Even though its AC <= 1, you need a shield proficiency for this
                expect(tgf.equip(new Armor(ARMOR.Shield), false)).toBeFalsy();
            });

            test("Medium Armor", () => {
                player.proficiencies = [EquipmentTag.MediumArmor];

                tgf.equip(new Armor(ARMOR.HideArmor));
                tgf.equip(new Armor(ARMOR.PaddedArmor));  // Also light armor due to dex bonus
                expect(tgf.equip(new Armor(ARMOR.ScaleArmor), false)).toBeFalsy();
                expect(tgf.equip(new Armor(ARMOR.Shield), false)).toBeFalsy();
            });

            test("All Armor", () => {
                player.proficiencies = [EquipmentTag.AllArmor];

                tgf.equip(new Armor(ARMOR.PaddedArmor));
                tgf.equip(new Armor(ARMOR.FullPlate));
                expect(tgf.equip(new Armor(ARMOR.Shield), false)).toBeFalsy();
            });

            test("Light Weapon", () => {
                player.proficiencies = [EquipmentTag.LightWeapon];

                tgf.equip(new Weapon(WEAPONS.Mace));
                expect(tgf.equip(new Weapon(WEAPONS.Quarterstaff), false)).toBeFalsy();
            });

            test("One handed weapon", () => {
                player.proficiencies = [EquipmentTag.OneHandedWeapon];

                tgf.equip(new Weapon(WEAPONS.Mace));
                expect(tgf.equip(new Weapon(WEAPONS.Quarterstaff), false)).toBeFalsy();
            });

            test("Light Shields", () => {
                player.proficiencies = [EquipmentTag.LightShield];

                tgf.equip(new Armor(ARMOR.Shield));
                expect(tgf.equip(new Armor(ARMOR.HeavyShield), false)).toBeFalsy();
            });

            test("All Shields", () => {
                player.proficiencies = [EquipmentTag.AllShield];

                tgf.equip(new Armor(ARMOR.Shield));
                tgf.equip(new Armor(ARMOR.HeavyShield));
                expect(tgf.equip(new Armor(ARMOR.ScaleArmor), false)).toBeFalsy();
            });
        });
    });

    describe("Energy", () => {
        test("Starts with zero energy", () => {
            expect(joe.energy).toStrictEqual(0);
        });

        test("Adds one energy per tick", () => {
            let origEnergy = joe.energy;
            joe.tick();
            expect(joe.energy).toStrictEqual(origEnergy + 1);
        });

        test("Is ready with zero or more energy", () => {
            expect(joe.isReady()).toBeTruthy();
        });

        test("Is not ready with less than zero energy", () => {
            joe.energy = -10;
            expect(joe.isReady()).toBeFalsy();
        });

        test("Expend expends and renders non-ready", () => {
            let origEnergy = joe.energy;
            joe.expend(Energy.MOVE);
            expect(joe.energy).toStrictEqual(origEnergy - Energy.MOVE);
            expect(joe.isReady()).toBeFalsy();
        });

        describe("Movement Speed", () => {
            let origEnergy: number;
            beforeEach(() => {
                origEnergy = joe.energy;
            });

            test("Faster / Higher", () => {
                // Higher movement speeds are better (even though it decreases energy spent)
                joe.baseMovementSpeed = Energy.MOVE + 1;
                joe.expend(Energy.MOVE);
                expect(joe.energy).toStrictEqual(origEnergy - Energy.MOVE + 1);
                expect(joe.isReady()).toBeFalsy();
            });

            test("Effectively capped", () => {
                joe.baseMovementSpeed = 8675309
                joe.expend(Energy.MOVE);
                expect(joe.energy).toStrictEqual(origEnergy - Energy.MIN);
                expect(joe.isReady()).toBeFalsy();
            });

            test("Slower / Lower", () => {
                joe.baseMovementSpeed = Energy.MOVE - 1;
                joe.expend(Energy.MOVE);
                expect(joe.energy).toStrictEqual(origEnergy - Energy.MOVE - 1);
                expect(joe.isReady()).toBeFalsy();
            });
        });
    });

    describe("Abilities", () => {
        test("Everything has basic melee by default", () => {
            const bma = "BasicMeleeAttack";
            expect(joe.abilityNames).toContain(bma);
            let foundAbility: Ability = joe.abilityByName(bma);
            expect(foundAbility).toBeTruthy();
            expect(joe.defaultMeleeAbilityName).toStrictEqual(bma);
            expect(joe.defaultMeleeAbility.name).toStrictEqual(bma);
        });

        test("Everything has basic ranged by default", () => {
            const ranged = "BasicRangedAttack";
            expect(joe.abilityNames).toContain(ranged);
            let foundAbility: Ability = joe.abilityByName(ranged);
            expect(foundAbility).toBeTruthy();
            expect(joe.defaultRangedAbilityName).toStrictEqual(ranged);
            expect(joe.defaultRangedAbility.name).toStrictEqual(ranged);
        })

        test("Gain an ability by name", () => {
            expect(joe.abilityNames).not.toContain("TestAbility");
            let numAbilities = joe.abilities.length;

            joe.gainAbility("TestAbility");

            expect(joe.abilityNames).toContain("TestAbility");
            expect(joe.abilities).toHaveLength(numAbilities + 1);

            // Can't gain it twice, though
            joe.gainAbility("TestAbility");
            expect(joe.abilities).toHaveLength(numAbilities + 1);
        });

        test("Gaining nonexistent abilities throws error", () => {
           expect(() => {
               joe.gainAbility("FakeAbility");
           }).toThrow("Registry could not look up FakeAbility!");
        });

        describe("Improvisation", () => {
            let improvisedName = WEAPONS.ImprovisedWeapon.name;
            beforeEach(() => {
                let wpn = WEAPONS.Mace;
                joe.equipment.mainHand = new Weapon(wpn);
            });

            test("Unarmed means improvising", () => {
                joe.equipment.mainHand = new NothingEquipment();
                expect(joe.equipment.mainHand.isNothing()).toBeTruthy();

                let melee = joe.weaponForAbility(joe.defaultMeleeAbility);
                expect(melee.isNothing()).toBeFalsy();
                expect(melee.name).toStrictEqual(improvisedName);

                let ranged = joe.weaponForAbility(joe.defaultRangedAbility);
                expect(ranged.isNothing()).toBeFalsy();
                expect(ranged.name).toStrictEqual(improvisedName);
            });

            test("Unknown ability means improvising", () => {
                console.warn = jest.fn()
                let whatevs = joe.weaponForAbility();
                expect(whatevs.name).toStrictEqual(improvisedName);
                expect(console.warn).toHaveBeenCalled();
            });

            test("Using a melee weapon for a ranged attack improvises", () => {
                let melee = joe.weaponForAbility(joe.defaultMeleeAbility);
                expect(melee.isNothing()).toBeFalsy();
                expect(melee.name).not.toStrictEqual(improvisedName);

                let ranged = joe.weaponForAbility(joe.defaultRangedAbility);
                expect(ranged.isNothing()).toBeFalsy();
                expect(ranged.name).toStrictEqual(improvisedName);
            });

            test("Using a ranged weapon for melee attack improvises", () => {
                joe.equipment.mainHand = new Weapon(WEAPONS.Crossbow);

                let melee = joe.weaponForAbility(joe.defaultMeleeAbility);
                expect(melee.isNothing()).toBeFalsy();
                expect(melee.name).toStrictEqual(improvisedName);

                let ranged = joe.weaponForAbility(joe.defaultRangedAbility);
                expect(ranged.isNothing()).toBeFalsy();
                expect(ranged.name).not.toStrictEqual(improvisedName);
            });

            test("PBAoE never improvises", () => {
                let aoe: IAbilityTemplate = {
                    name: "PBAoE Test",
                    description: "Test ability please ignore",
                    targeting: Targets.PBAoE
                };
                let ability = joe.gainAbilityFromTemplate(aoe);

                let wpn = joe.weaponForAbility(ability);
                expect(wpn.isNothing()).toBeFalsy();
                expect(wpn.name).not.toStrictEqual(improvisedName);

            });
        });

    });

    describe("Heal", () => {
        test("Regular healing", () => {
            joe.hp = 1;
            let healed = joe.heal(1);
            expect(healed).toBe(1);
            expect(joe.hp).toBe(2);
        });

        test("Overhealing caps at maxhp", () => {
            joe.hp = joe.maxHp - 1;
            let healed = joe.heal(10);
            expect(healed).toBe(1);
            expect(joe.hp).toBe(joe.maxHp);
        });
    });

    describe("Serialization", () => {
        test("Character roundtrip", () => {
            joe.setBehavior(new StandThereBehavior());
            let exported = jsonRoundTrip(joe);
            let char = new Character(exported);
            expect(char).toMatchObject(exported);
        });

        test("behavior itself excluded", () => {
            let exported = jsonRoundTrip(joe);
            expect(exported['behavior']).toBeUndefined();
            expect(exported['_behavior']).toBeUndefined();
        });

        test("behaviorName exported correctly", () => {
            joe.setBehavior(new StandThereBehavior());
            expect(joe.behaviorName).toBeTruthy();
            let exported = jsonRoundTrip(joe);
            let name = exported['behaviorName']
            expect(name).toBeTruthy();
            expect(name).toStrictEqual("StandThereBehavior");
        });

        test("Derived abilities excluded", () => {
            let exported = jsonRoundTrip(joe);
            expect(exported['defaultMeleeAbility']).toBeUndefined();
            expect(exported['defaultRangedAbility']).toBeUndefined();
            // Regular abilities still there, though:
            expect(exported['abilities']).not.toBeUndefined();
        });

        test("Roundtrip preserves derived abilities", () => {
            expect(joe.defaultMeleeAbility).toBeTruthy();
            let exported = jsonRoundTrip(joe);
            let char = new Character(exported);
            expect(char.defaultMeleeAbility).toBeTruthy();
        });

        test("Roundtrip preserves cooldowns", () => {
            let heal = joe.gainAbility("BasicHeal");
            expect(heal.currentCooldown).toStrictEqual(0);
            heal.startCooldown();
            expect(heal.cooldown).toBeGreaterThan(1);
            expect(heal.currentCooldown).toStrictEqual(heal.cooldown);

            let poison = new Condition(CONDITIONS.Ongoing);
            joe.receiveCondition(poison);

            let exported = jsonRoundTrip(joe);
            let char = new Character(exported);
            let newHeal = char.abilityByName('BasicHeal');
            expect(newHeal).toBeTruthy();
            expect(newHeal.currentCooldown).toBeGreaterThan(1);
            expect(newHeal.currentCooldown).toStrictEqual(heal.currentCooldown);
            expect(newHeal.constructor.name).toStrictEqual("Ability");

            expect(char.conditions).toHaveLength(1);
            let newPoison = char.conditions[0];
            expect(newPoison.constructor.name).toStrictEqual("Condition");
            expect(newPoison.duration).toStrictEqual(Duration.SaveEnds);
        });

        test("Roundtrip preserves tempHp", () => {
            // This wasn't working because I was setting tempHp to zero after the copy
            joe.tempHp = 9;
            let exported = jsonRoundTrip(joe);
            let char = new Character(exported);
            expect(char.tempHp).toStrictEqual(9);
        });

        test("Behavior realized correctly", () => {
            joe.setBehavior(new StandThereBehavior());
            let exported = jsonRoundTrip(joe);
            let char = new Character(exported);
            expect(char.behaviorName).toStrictEqual("StandThereBehavior");
            expect(char.behavior.character).toBe(char);
        });

        test("Hotkeys restored correctly", () => {
            expect(joe.defaultMeleeAbility).toBeTruthy();
            joe.hotkeys[0] = joe.defaultMeleeAbility;
            let exported = jsonRoundTrip(joe);
            let char = new Character(exported);
            expect(char.hotkeys[0]).toBeTruthy();
            expect(char.hotkeys[0]).toBe(char.defaultMeleeAbility);
            expect(char.hotkeys[0].constructor.name).toStrictEqual("Ability");
            expect(char.hotkeys[1]).toBeFalsy();
        });

        test("Inventory realized correctly", () => {
            let shield = new Armor(ARMOR.Shield);
            joe.receiveItem(shield);
            expect(joe.inventory.length).toStrictEqual(1);
            let exported = jsonRoundTrip(joe);
            let char = new Character(exported);
            expect(char.inventory.length).toStrictEqual(1);
            let newShield = char.inventory[0];
            expect(newShield).toMatchObject(shield);
            expect(newShield.constructor.name).toStrictEqual("Armor");
        });

        test("Equipment realized correctly", () => {
            let shield = new Armor(ARMOR.Shield);
            joe.receiveItem(shield);
            joe.equipment.offHand = shield;
            let exported = jsonRoundTrip(joe);
            let char = new Character(exported);

            let newShield = char.equipment.offHand;
            expect(newShield).toMatchObject(shield);
            expect(newShield.constructor.name).toStrictEqual("Armor");
        });

        // See https://gitlab.com/rostrander/tyranny-of-the-labyrinth/-/issues/113
        test("Equipment does not duplicate on roundtrip", () => {
            let shield = new Armor(ARMOR.Shield);
            joe.receiveItem(shield);
            joe.equipment.offHand = shield;
            expect(joe.inventory[0]).toBe(shield);
            expect(joe.inventory.length).toBeGreaterThan(0);

            let exported = jsonRoundTrip(joe);
            let char = new Character(exported);

            let newShield = char.equipment.offHand;
            let invShield = char.inventory[0];
            expect(char.inventory.length).toBeGreaterThan(0);

            // They should be equal due to iid being the same:
            expect(newShield).toStrictEqual(invShield);
            // But also the `Character` constructor should have linked them back up:
            expect(newShield).toBe(invShield);
        });

        test("Passives restored correctly", () => {
            joe.gainPassive(new Condition(PASSIVES.Einhander));
            let exported = jsonRoundTrip(joe);
            let char = new Character(exported);
            expect(char.passives).toHaveLength(1);
            expect(char.passives[0]).toBeInstanceOf(Condition);
        });
    });

    describe("Point buy", () => {
        test("New characters have 20 points", () => {
            expect(joe.maxPoints).toStrictEqual(20);
            expect(joe.points).toStrictEqual(20);
        });

        test("Spend a point on a stat", () => {
            expect(joe.stats.con).toStrictEqual(10);
            expect(joe.buyStat('con')).toBeTruthy();
            expect(joe.points).toStrictEqual(19);
            expect(joe.pointsSpent).toStrictEqual(1);
            expect(joe.stats.con).toStrictEqual(11);
        });

        test("Get points by moving stat to 8", () => {
            expect(joe.buyStat('str', -1)).toBeTruthy();
            expect(joe.points).toStrictEqual(21);
        });

        test("Spend and refund a point", () => {
            expect(joe.buyStat('dex')).toBeTruthy();
            expect(joe.points).toStrictEqual(19);
            expect(joe.buyStat('dex', -1)).toBeTruthy();
            expect(joe.points).toStrictEqual(20);
        });

        describe("canBuyStat", () => {
            test("Can't buy nonsense stat", () => {
                expect(joe.canBuyStat('pulchritude')).toBeFalsy();
            });

            test("Can't go lower than 8 or higher than 18", () => {
                joe.stats.wis = 18;
                expect(joe.canBuyStat('wis')).toBeFalsy();
                joe.stats.cha = 8;  // dump stat!
                expect(joe.canBuyStat('cha', -1)).toBeFalsy();
            });

            test("Can't buy a stat you can't afford", () => {
                joe.points = 0;
                expect(joe.canBuyStat('int')).toBeFalsy();
            });
        });

        describe("pointCostAt", () => {
            test("Negative points have a negative total cost", () => {
                expect(joe.totalPointCost(9)).toStrictEqual(-1);
                expect(joe.totalPointCost(8)).toStrictEqual(-2);
            });

            test("Total point costs match the chart", () => {
                // doesn't include 8 and 9 because the chart is weird for them,
                // see test above for those cases
                expect(joe.totalPointCost(10)).toStrictEqual(0);
                expect(joe.totalPointCost(11)).toStrictEqual(1);
                expect(joe.totalPointCost(12)).toStrictEqual(2);
                expect(joe.totalPointCost(13)).toStrictEqual(3);
                expect(joe.totalPointCost(14)).toStrictEqual(5);
                expect(joe.totalPointCost(15)).toStrictEqual(7);
                expect(joe.totalPointCost(16)).toStrictEqual(9);
                expect(joe.totalPointCost(17)).toStrictEqual(12);
                expect(joe.totalPointCost(18)).toStrictEqual(16);
            });

            test("One-point cost stats", () => {
                for(let i=8; i <= 12; i++) {
                    expect(joe.pointCostAt(i)).toStrictEqual(1);
                }
            });

            test("Two-point cost stats", () => {
                for(let i=13; i <= 15; i++) {
                    expect(joe.pointCostAt(i)).toStrictEqual(2);
                }
            });

            test("Higher cost stats", () => {
                expect(joe.pointCostAt(16)).toStrictEqual(3);
                expect(joe.pointCostAt(17)).toStrictEqual(4);
            });

            test("Negative-one-point cost stats", () => {
                for(let i=9; i <= 13; i++) {
                    expect(joe.pointCostAt(i, -1)).toStrictEqual(-1);
                }
            });

            test("Negative-two-point cost stats", () => {
                for(let i=14; i <= 16; i++) {
                    expect(joe.pointCostAt(i, -1)).toStrictEqual(-2);
                }
            });

            test("Negative higher cost stats", () => {
                expect(joe.pointCostAt(17, -1)).toStrictEqual(-3);
                expect(joe.pointCostAt(18, -1)).toStrictEqual(-4);
            });
        });
    });

    describe("TempHP", () => {
        let joe: Character;

        beforeEach(() => {
            joe = new Character({
                char: '@',
                name: 'Joe',
                baseMaxHp: 10,
                hpPerLevel: 2,
                description: "test joe",
            });
        });

        test("Granting", () => {
            expect(joe.tempHp).toStrictEqual(0);

            joe.grantTempHp(Dice.fixed(3));
            expect(joe.tempHp).toStrictEqual(3);

            joe.grantTempHp(Dice.fixed(5));
            expect(joe.tempHp).toStrictEqual(5);
        });

        test("Not cumulative", () => {
            joe.grantTempHp(Dice.fixed(5));
            joe.grantTempHp(Dice.fixed(2));
            expect(joe.tempHp).toStrictEqual(5);
        });

        test("Depleting", () => {
            expect(joe.hp).toStrictEqual(10);

            joe.grantTempHp(Dice.fixed(5));
            joe.takeDamage(Dice.fixed(3));
            expect(joe.tempHp).toStrictEqual(2);

            joe.takeDamage(Dice.fixed(3));
            expect(joe.tempHp).toStrictEqual(0);
            expect(joe.hp).toStrictEqual(9);
        });

    });
});

describe("Needs Arena", () => {
    let tgf: TestGameFactory;
    let gm: GameMaster;
    let player: Character;
    let pc: PlayerController;

    beforeEach(() => {
        tgf = new TestGameFactory();
        ({player, gm} = tgf.testGameData());
        pc = new PlayerController(player, gm);
    });

    test("Refresh boss powers when queued", () => {
        let ability = player.gainAbility("HitEvenHarder");
        ability.startCooldown();
        expect(ability.isReady()).toBeFalsy();
        pc.wait();

        player.refreshBossNextTurn = true;
        tgf.nextTurn();
        expect(ability.isReady()).toBeTruthy();
        expect(player.refreshBossNextTurn).toBeFalsy();
    });

    describe("Zones", () => {
        let zone: Zone;
        let pe: IEffectTemplate;

        beforeEach(() => {
            pe = {
                name: 'DefenseEffect',
                amount: 2,
                defense: Chain.ac,
            };
            zone = new Zone({
                center: {x: 2, y: 2},
                radius: 1,
                passiveEffects: [pe],
            });
        });

        test("Character gains zone's passive effects", () => {
            expect(player.calcDefense(Chain.ac).value).toStrictEqual(10);
            let playerTile = gm.map.tileAt(player.loc);
            expect(playerTile.zone).toBeFalsy();

            gm.addZone(zone);

            expect(playerTile.zone).toBeTruthy();
            expect(player.calcDefense(Chain.ac).value).toStrictEqual(12);
        });

        test("Unaffected by your own zones", () => {
            zone.faction = Faction.Player;

            gm.addZone(zone);
            expect(player.calcDefense(Chain.ac).value).toStrictEqual(10);
            let playerTile = gm.map.tileAt(player.loc);
            expect(playerTile.zone).toBeTruthy();
        });
    });

    describe("Last turn actions", () => {
        test("Default nothing", () => {
            expect(player.lastTurn).toEqual([LastAction.Nothing]);
            expect(player.didLastTurn(LastAction.Nothing)).toBeTruthy();
            expect(player.didLastTurn(LastAction.Moved)).toBeFalsy();

            // empty activity is also nothing
            player.lastTurn = [];
            expect(player.didLastTurn(LastAction.Nothing)).toBeTruthy();
        });

        test("Move", () => {
            expect(pc.bump({x: 1, y: 0})).toBeTruthy();
            expect(player.didLastTurn(LastAction.Moved)).toBeTruthy();
            expect(player.didLastTurn(LastAction.Nothing)).toBeFalsy();
            tgf.nextTurn();
            expect(pc.wait).toBeTruthy();
            expect(player.didLastTurn(LastAction.Nothing)).toBeTruthy();
            expect(player.didLastTurn(LastAction.Moved)).toBeFalsy();
        });
    });
});