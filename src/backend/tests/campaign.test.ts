import {Campaign, IStorage} from "../campaign";
import {TestGameFactory} from "./testutils";
import {GameMaster} from "../gamemaster";

class MockStorage implements IStorage {
    public store: {[key: string]: string};

    constructor() {
        this.store = {};
    }

    public getItem(key: string): string {
        return this.store[key];
    }

    public setItem(key: string, value: string) {
        this.store[key] = value;
    }
}

let store: IStorage;
let c: Campaign;
let tgf: TestGameFactory;

beforeEach(() => {
    store = new MockStorage();
    c = new Campaign(store);
    tgf = new TestGameFactory();
});

test("Campaign attempts to bootstrap itself", () => {
    let storageGet = jest.fn();
    let storageSet = jest.fn();
    let store = new MockStorage();
    store.getItem = storageGet;
    store.setItem = storageSet;

    let c = new Campaign(store);
    expect(storageGet).toHaveBeenCalledWith("campaign");
    expect(storageSet).toHaveBeenCalled();

    expect(c.playing).toBeFalsy();
});

describe("New game", () => {

    test("Blank slate", () => {
        expect(c.playing).toBeFalsy();
        let gm = tgf.newGame();

        expect(c.newGame(gm)).toBeTruthy();

        expect(c.playing).toBe(gm);
        expect(c.unlocks).toHaveLength(0);
    });

    test("Newgame into occupied slot", () => {
        let gm = tgf.newGame();
        expect(c.newGame(gm)).toBeTruthy();

        let gm2 = tgf.newGame();
        expect(c.newGame(gm2)).toBeFalsy();
        expect(c.playing).toBe(gm);
    });

    test("Newgame into occupied slot with force", () => {
        let gm = tgf.newGame();
        expect(c.newGame(gm)).toBeTruthy();
        let oldGm = c.playing;

        let gm2 = tgf.newGame();
        expect(c.newGame(gm2, true)).toBeTruthy();
        expect(c.playing).not.toBe(oldGm);
        expect(c.playing).toBe(gm2);
    });

    test("Saving and loading a game", () => {
        let gm = tgf.newGame();
        expect(c.newGame(gm)).toBeTruthy();
        gm.forceMoveCharacter(gm.player, {x: 3, y: 4});
        c.save()
        let oldPlayer = gm.player;
        let oldMap = gm.map;

        c = new Campaign(store);
        let newGm = c.loadGame();
        expect(newGm).not.toBe(gm);
        expect(newGm).toBeInstanceOf(GameMaster);
        expect(newGm.player).not.toBe(oldPlayer);
        expect(newGm.map).not.toBe(oldMap);
        expect(newGm.player.loc).toEqual({x: 3, y: 4});

        newGm.forceMoveCharacter(gm.player, {x: 4, y: 4});
        c = new Campaign(store);
        let newerGm = c.loadGame();
        expect(newerGm).not.toBe(gm);
        expect(newerGm).not.toBe(newGm);
        expect(newerGm.player.loc).toEqual({x: 3, y: 4});
    });
});

describe("Game Over", () => {
    test("Basic gameover resets the slot", () => {
        let gm = tgf.newGame();
        expect(c.newGame(gm)).toBeTruthy();

        c.gameOver();
        expect(c.playing).toBeFalsy();
    });

    test("unlock records an unlock without repeats", () => {
        let gm = tgf.newGame();
        expect(c.newGame(gm)).toBeTruthy();

        expect(c.unlocks).toHaveLength(0);

        expect(c.unlock("test")).toBeTruthy();
        expect(c.unlocks).toHaveLength(1);
        expect(c.unlocks).toContain("test");
        expect(c.isUnlocked('test')).toBeTruthy();

        expect(c.unlock("test")).toBeFalsy();
        expect(c.unlocks).toHaveLength(1);
    });

    test("unlockAndGameOver unlocks and game overs", () => {
        let gm = tgf.newGame();
        expect(c.newGame(gm)).toBeTruthy();

        let unlock = c.unlockAndGameOver("test");
        expect(unlock).toBeTruthy();
        expect(unlock).toStrictEqual("test");

        expect(c.unlocks).toHaveLength(1);
        expect(c.unlocks).toContain("test");

        expect(c.unlockAndGameOver("test")).toBeFalsy();

        expect(c.playing).toBeFalsy();
    });
});