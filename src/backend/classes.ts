import * as ROT from 'rot-js';

import {Faction, ICharacterTemplate} from "./interfaces";
import {FIGHTER_ABILITIES} from "./abilities/fighter";
import {WIZARD_ABILITIES} from "./abilities/wizard";
import {ADVENTURER_ABILITIES} from "./abilities/adventurer";
import {CLERIC_ABILITIES} from "./abilities/cleric";
import {ROGUE_ABILITIES} from "./abilities/rogue";
import {EquipmentTag} from "./common";

export const BOSSES: {[bossName: string]: string} = {
    "Fighter Moxie": "Fighter",
    "Wizard Zankar": "Wizard",
    "Cleric Llynmir": "Cleric",
    "Rogue Jayne": "Rogue",
}

export const CLASSES: { [key:string]: ICharacterTemplate } = {
    "Adventurer": {
        char: "@",
        name: "Adventurer",
        baseMaxHp: 10,
        hpPerLevel: 6,
        description: "This is the tutorial class, to give you an idea of " +
            "how the game plays.  If you've already unlocked another class, "+
            "selecting this one will repeat the tutorial.",
        fgcolor: ROT.Color.fromString("#ffff14"),  // yellow
        className: "Adventurer",
        faction: Faction.Player,
        classAbilityNames: ADVENTURER_ABILITIES.map(b => b.name),
        classStatFocuses: ['str', 'dex', 'con', 'int', 'wis', 'cha'],
        proficiencies: [EquipmentTag.Everything],
        // Specifically passing this in so we can select Adventurer powers later:
        abilityNames: [],
    },
    "Fighter": {
        char: "@",
        name: "Fighter",
        baseMaxHp: 12,
        hpPerLevel: 10,
        description: "The fighter excels in close combat, capable of both "+
            "offense and defense.",
        fgcolor: ROT.Color.fromString("#e50000"),  // Red
        className: "Fighter",
        faction: Faction.Player,
        classAbilityNames: FIGHTER_ABILITIES.map(b => b.name),
        classStatFocuses: ['str', 'dex', 'con'],
        stats: {
            str: 12,
            dex: 10,
            con: 12,
            int: 8,
            wis: 8,
            cha: 10,
        },
        proficiencies: [EquipmentTag.Everything],
    },
    "Wizard": {
        char: "@",
        name: "Wizard",
        baseMaxHp: 8,
        hpPerLevel: 4,
        description: "Master of the arcane arts, the wizard makes up for " +
            "its fragility with ranged attacks and crowd control.",
        fgcolor: ROT.Color.fromString("#069af3"), // azure
        className: "Wizard",
        faction: Faction.Player,
        classAbilityNames: WIZARD_ABILITIES.map(b => b.name),
        classStatFocuses: ['int',],
        stats: {
            str: 6,
            dex: 10,
            con: 10,
            int: 12,
            wis: 14,
            cha: 6,
        },
        proficiencies: [EquipmentTag.LightArmor, EquipmentTag.LightWeapon],
    },
    "Cleric": {
        char: "@",
        name: "Cleric",
        baseMaxHp: 10,
        hpPerLevel: 8,
        description: "The cleric calls upon divine powers to fight in "+
            "either ranged or melee.",
        fgcolor: ROT.Color.fromString("#ffab0f"), // yellowish orange
        className: "Cleric",
        faction: Faction.Player,
        classAbilityNames: Object.keys(CLERIC_ABILITIES),
        classStatFocuses: ['str', 'con', 'wis', 'cha'],
        stats: {
            str: 12,
            dex: 8,
            con: 12,
            int: 8,
            wis: 12,
            cha: 12,
        },
        proficiencies: [EquipmentTag.OneHandedWeapon, EquipmentTag.AllShield, EquipmentTag.AllArmor],
    },
    "Rogue": {
        char: "@",
        name: "Rogue",
        baseMaxHp: 9,
        hpPerLevel: 6,
        description: "The rogue is a fast striker, doing damage through "+
            "brute force and/or trickery.",
        fgcolor: ROT.Color.fromString("#ff0490"), // Electric pink
        className: "Rogue",
        faction: Faction.Player,
        classAbilityNames: Object.keys(ROGUE_ABILITIES),
        classStatFocuses: ['str', 'dex'],
        stats: {
            str: 12,
            dex: 12,
            con: 10,
            int: 8,
            wis: 8,
            cha: 12,
        },
        proficiencies: [EquipmentTag.OneHandedWeapon, EquipmentTag.LightShield, EquipmentTag.MediumArmor],
    },
}
