import {Color} from "rot-js/lib/color";

import {IEffectTemplate} from "./effects/effect";
import {IChainReceiver} from "./chain";
import {IConditionTemplate} from "./effects/condition";
import {EquipmentTag} from "./common";


export interface XY {
    x: number;
    y: number;
}

export interface XYWH extends XY {
    w: number;
    h: number;
}

export interface RegistryMappable {
    name: string;
}

export interface SpawnTarget {
    spawnCount: number;
    map: SpawnQueryable;
    player: ICharacterTemplate;
    addCharacter(who: ICharacterTemplate): void;
    canChallengeBoss(who: string): boolean;
}

export interface IItemTemplate {
    name: string;
    displayName?: string;
    iid?: string;
    char: string;
    desc: string;
    itemType?: string;
}

export interface TreasureTarget {
    placeItemAt(what: IItemTemplate, where: XY): boolean;
}

export interface TileQueryable {
    tileAt(where: XY): ITileTemplate;
}

export interface SpawnQueryable extends TileQueryable {
    randomSpawnableTile(attempts?:number): XY;
}

export interface Stats {
    str: number,
    dex: number,
    con: number,
    int: number,
    wis: number,
    cha: number,

    [x: string]: number
}

export interface ICharacterTemplate {
    char: string;
    name: string;
    displayName?: string;
    baseMaxHp: number;
    hpPerLevel: number;
    description: string;
    fgcolor?: Color;

    // Characters rather than monsters
    className?: string
    classAbilityNames?: string[]  // ability names
    classStatFocuses?: string[];
    proficiencies?: EquipmentTag[],

    // Monsters rather than characters
    behaviorName?: string

    // Things that have defaults
    faction?: Faction; // Faction.Enemy
    stats?: Stats; // 10 for all
    baseAc?: number; // 10
    sightDistance?: number;  // 10
    maxPoints?: number; // 20
    level?: number; // 1
    baseMovementSpeed?: number; // Energy.MOVE

    // Likely to be part of saved data rather than templates
    energy?: number;
    loc?: XY;
    hp?: number;
    tempHp?: number;
    abilityNames?: string[];
    defaultMeleeAbilityName?: string;
    defaultRangedAbilityName?: string;
    conditions?: IConditionTemplate[];
}

export interface ITileTemplate {
    lit: number;
    viewed: boolean;
    char: string;
    passable: boolean;
    contents: IItemTemplate[];
    zone?: IZoneTemplate;
    x: number;
    y: number;
}

export enum Faction {
    // I know this is the default, but I want this explicitly falsy
    Nobody = 0,
    Player,
    Enemy, // Shorthand for 'all enemies'
    Everybody,
}

export interface IZoneTemplate {
    center: XY;
    radius: number;
    ttl?: number;  // Energy.TURN * 5
    passiveEffects?: IEffectTemplate[];
    faction?: Faction;
}

export type ChainableZone = IZoneTemplate & IChainReceiver;
export type EventCallback = () => void;
export type Weightable = { [key: string]: number, [key: number]: number }
export type TemplateMap = { [key: string]: RegistryMappable }
