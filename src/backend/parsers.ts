import {Character} from "./character";
import {GameMaster} from "./gamemaster";
import {Dice, IDieRollTemplate} from "./dice";
import {Chain} from "./common";
import {Ability} from "./abilities/ability";
import {DieCategory, DieRollArgs} from "./chain";

interface SpecPortionConstructor {
    new(
        portionStr: string
    ): SpecPortion
}

abstract class SpecParser {
    public abstract act(spec: string, fromWho: Character, what: Ability, toWho: Character, gm: GameMaster): Dice;

    public abstract parse(spec: string): SpecPortion[];

    abstract parseOne(portionStr: string): SpecPortion;
}

export abstract class SpecPortion {
    public negative: boolean;

    constructor(
        protected portionStr: string,
    ) {
        this.parsePortion(portionStr);
    }

    public abstract act(fromWho: Character, what: Ability, toWho: Character, gm: GameMaster, dice: Dice): Dice;

    protected abstract parsePortion(portionStr: string): void

    public negate(): this {
        this.negative = !this.negative;
        return this;
    }
}

/// Both parsers use these
export class StatPortion extends SpecPortion {
    public stat: string;

    public static REGEX = /((str)|(dex)|(con)|(int)|(wis)|(cha))/i

    act(fromWho: Character, what: Ability, toWho: Character, gm: GameMaster, dice: Dice): Dice {
        let statBonus = fromWho.statBonus(this.stat);
        let mult = this.negative ? -1 : 1;
        return dice.addOrAdopt(statBonus * mult, this.stat);
    }

    protected parsePortion(portionStr: string) {
        this.stat = StatPortion.REGEX.exec(portionStr)[1]
    }
}

export class NumberPortion extends SpecPortion {
    public value: number;

    public static REGEX = /(\d+)/i

    act(fromWho: Character, what: Ability, toWho: Character, gm: GameMaster, dice: Dice): Dice {
        let mult = this.negative ? -1 : 1;
        return dice.addOrAdopt(this.value * mult, "Ability bonus")
    }

    protected parsePortion(portionStr: string): void {
        this.value = parseInt(portionStr);
    }

}

//region Tohit Spec
export class TohitSpecParser extends SpecParser {
    public roll: Dice = Dice.zero;
    public target: Dice = Dice.zero;

    public leftPortions: SpecPortion[];
    public rightPortions: SpecPortion[];

    public act(spec: string, fromWho: Character, what: Ability, toWho: Character, gm: GameMaster): Dice {
        this.parse(spec.toLowerCase());
        // Actually roll the tohit first, before we then add the spec bonuses
        this.roll = gm.roll();
        let advantage = fromWho.advantageStatus();
        if(advantage !== 0) {
            let other = gm.roll();
            if((advantage < 0 && other.value < this.roll.value) ||
                advantage > 0 && other.value > this.roll.value) {
                this.roll = other;
            }
        }
        for (let portion of this.leftPortions) {
            portion.act(fromWho, what, toWho, gm, this.roll);
        }
        fromWho.addToHitBonuses(this.roll, toWho);

        for (let portion of this.rightPortions) {
            portion.act(fromWho, what, toWho, gm, this.target);
        }
        return this.roll;
    }

    public parse(spec: string): SpecPortion[] {
        // Break up into portions
        let sides = spec.split("vs");
        this.leftPortions = this.parseSide(sides[0]);
        this.rightPortions = this.parseSide(sides[1]);
        return this.leftPortions;
    }

    protected parseSide(spec: string): SpecPortion[] {
        let mapper = (str: string) => this.parseOne(str.trim());
        let portions = spec.split("+");
        let result = [];
        for(let portion of portions) {
            let negatives = portion.split("-");

            let firstTerm = negatives.shift();  // first term isn't negative
            result.push(this.parseOne(firstTerm.trim()));
            for(let neg of negatives) {
                let term = this.parseOne(neg.trim()).negate();
                result.push(term);
            }
        }
        return result
    }

    parseOne(portionStr: string): SpecPortion {
        if (portionStr.match(StatPortion.REGEX)) {
            return new StatPortion(portionStr);
        }
        if (portionStr.match(DefensePortion.REGEX)) {
            return new DefensePortion(portionStr);
        }
        if (portionStr.match(NumberPortion.REGEX)) {
            return new NumberPortion(portionStr);
        }
        throw `Unhandlable portion ${portionStr}`;
    }
}

export class DefensePortion extends SpecPortion {
    public defense: Chain;

    public static REGEX = /((ac)|(fort)|(ref)|(will))/i

    act(fromWho: Character, what: Ability, toWho: Character, gm: GameMaster, dice: Dice): Dice {
        let defense = toWho.calcDefense(this.defense, fromWho);
        let mult = this.negative ? -1 : 1;
        return dice.addOrAdopt(defense.value * mult, `${Chain[this.defense]}`);
    }

    protected parsePortion(portionStr: string) {
        const def: Chain = Chain[portionStr as keyof typeof Chain];
        this.defense = def;
    }
}
//endregion

//region Damage Spec
export class DamageSpecParser extends SpecParser {
    public act(spec: string, fromWho: Character, what: Ability, toWho: Character, gm: GameMaster): Dice {
        let portions = this.parse(spec);
        let starter = Dice.zero;
        for (let portion of portions) {
            portion.act(fromWho, what, toWho, gm, starter);
        }
        return starter;
    }

    public parse(spec: string): SpecPortion[] {
        // Break up into portions
        let portions = spec.split("+");
        let result = [];
        for(let portion of portions) {
            let negatives = portion.split("-");

            let firstTerm = negatives.shift();  // first term isn't negative
            result.push(this.parseOne(firstTerm.trim()));
            for(let neg of negatives) {
                let term = this.parseOne(neg.trim()).negate();
                result.push(term);
            }
        }
        return result;
    }

    parseOne(portionStr: string): SpecPortion {
        if (portionStr.match(WeaponDamagePortion.REGEX)) {
            return new WeaponDamagePortion(portionStr);
        }
        if (portionStr.match(DicePortion.REGEX)) {
            return new DicePortion(portionStr);
        }
        if (portionStr.match(StatPortion.REGEX)) {
            return new StatPortion(portionStr);
        }
        // Has to be lower priority than the other two because otherwise it'll
        // match the "1" in "1W" damage portions
        if (portionStr.match(NumberPortion.REGEX)) {
            return new NumberPortion(portionStr);
        }
        throw `Unhandlable portion ${portionStr}`;
    }
}

export class DicePortion extends SpecPortion {
    public static REGEX = /(\d+)d(\d+)$/i

    public dice: IDieRollTemplate

    act(fromWho: Character, what: Ability, toWho: Character, gm: GameMaster, dice: Dice): Dice {
        let rolled = gm.roll(new Dice(this.dice));

        let args: DieRollArgs = {
            gm,
            toRoll: this.dice,
            category: DieCategory.EXPLICIT,
        }
        fromWho.chain(Chain.dieRoll, rolled, args);
        let mult = this.negative ? -1 : 1;
        return dice.addOrAdopt(rolled.value * mult, "Ability damage");
    }

    protected parsePortion(portionStr: string): void {
        let numbers = DicePortion.REGEX.exec(portionStr);
        let n = parseInt(numbers[1]);
        let d = parseInt(numbers[2]);
        this.dice = {n, d};
    }

}

export class WeaponDamagePortion extends SpecPortion {
    public dieRolls: number;

    public static REGEX = /(\d+)w$/i

    act(fromWho: Character, what: Ability, toWho: Character, gm: GameMaster, dice: Dice): Dice {
        // Effects won't have a reference to the ability that made them,
        let dieTemplate = fromWho.weaponDice(what)
        let rolled = gm.roll(dieTemplate);
        for(let i=this.dieRolls; i > 1; i--) {
            let extraRoll = gm.roll(fromWho.weaponDice(what));
            rolled.addDice(extraRoll, "Extra damage roll");
        }
        let args: DieRollArgs = {
            gm,
            toRoll: fromWho.weaponDice(what),
            category: DieCategory.WEAPON,
        }
        fromWho.chain(Chain.dieRoll, rolled, args);
        let mult = this.negative ? -1 : 1;
        return dice.addOrAdopt(rolled.value * mult, "Weapon damage");
    }

    parsePortion(portionStr: string) {
        this.dieRolls = parseInt(WeaponDamagePortion.REGEX.exec(portionStr)[1]);
    }
}
//endregion