import {IWeaponTemplate} from "./item";

export const WEAPONS: { [key:string]: IWeaponTemplate; } = {
    "ImprovisedWeapon": {
        name: "ImprovisedWeapon",
        displayName: "Improvised Weapon",
        char: '?',
        damage: {d: 4},
        isMelee: true,
        isRanged: true,
        slots: ['mainHand'],
        desc: "Just do something!  Anything!",
    },
    "Dagger": {
        name: "Dagger",
        char: '|',
        damage: { d: 4 },
        slots: ['mainHand'],
        desc: "For cutting monsters, purses"
    },
    "Whip": {
        name: "Whip",
        char: '|',
        damage: { d: 4 },
        slots: ['mainHand'],
        desc: "Whip it adequately"
    },
    "Mace": {
        name: "Mace",
        char: '|',
        damage: { d: 6 },
        slots: ['mainHand'],
        desc: "It's like an extremely dull knife"
    },
    "Handaxe": {
        name: "Handaxe",
        char: '|',
        damage: { d: 6 },
        slots: ['mainHand'],
        desc: "Allows you to axe enemies"
    },
    "Shortsword": {
        name: "Shortsword",
        char: '|',
        damage: { d: 6 },
        slots: ['mainHand'],
        desc: "Pretty self-describing"
    },
    "Sickle": {
        name: "Sickle",
        char: '|',
        damage: { d: 6 },
        slots: ['mainHand'],
        desc: "The weapon of comrades everywhere"
    },
    "Hammer": {
        name: "Hammer",
        char: '|',
        damage: { d: 6 },
        slots: ['mainHand'],
        desc: "Hammer in the morning, evening, etc"
    },
    "HandCrossbow": {
        name: "HandCrossbow",
        displayName: "Hand Crossbow",
        char: "}",
        damage: {d: 6},
        isMelee: false,
        isRanged: true,
        slots: ['mainHand'],
        desc: "Surprisingly clumsy for a light weapon"
    },
    "Sling": {
        name: "Sling",
        char: "}",
        damage: {d: 6},
        isMelee: false,
        isRanged: true,
        slots: ['mainHand'],
        desc: "You could take down a giant with this"
    },
    "Longsword": {
        name: "Longsword",
        char: "|",
        damage: {d: 8},
        slots: ['mainHand'],
        desc: "It's a sword that's long"
    },
    "Scimitar": {
        name: "Scimitar",
        char: "|",
        damage: {d: 8},
        slots: ['mainHand'],
        desc: "That's just a fancy word for 'sword'"
    },
    "Rapier": {
        name: "Rapier",
        char: "|",
        damage: {d: 8},
        slots: ['mainHand'],
        desc: "For the dashing warrior on the go"
    },
    "Pick": {
        name: "Pick",
        char: "|",
        damage: {d: 8},
        slots: ['mainHand'],
        desc: "Do not use on your nose"
    },
    "Quarterstaff": {
        name: "Quarterstaff",
        char: "|",
        damage: {d: 8},
        isMelee: true,
        isRanged: false,
        isTwoHanded: true,
        slots: ['mainHand'],
        desc: "Actually much larger than 1/4 of a staff"
    },
    "Crossbow": {
        name: "Crossbow",
        char: "}",
        damage: {d: 8},
        isMelee: false,
        isRanged: true,
        isTwoHanded: true,
        slots: ['mainHand'],
        desc: "An inelegant weapon for a more barbaric age"
    },
    "Shortbow": {
        name: "Crossbow",
        char: "}",
        damage: {d: 8},
        isMelee: false,
        isRanged: true,
        isTwoHanded: true,
        slots: ['mainHand'],
        desc: "Pretty tall, actually"
    },
    "Greatclub": {
        name: "Greatclub",
        char: "|",
        damage: {n: 2, d: 4},
        isMelee: true,
        isRanged: false,
        isTwoHanded: true,
        slots: ['mainHand'],
        desc: "I know this great club downtown"
    },
    "Scythe": {
        name: "Scythe",
        char: "|",
        damage: {n: 2, d: 4},
        isMelee: true,
        isRanged: false,
        isTwoHanded: true,
        slots: ['mainHand'],
        desc: "Don't fear the reaper"
    },
    "Battleaxe": {
        name: "Battleaxe",
        char: "|",
        damage: {d: 10},
        slots: ['mainHand'],
        desc: "Far more effective than the peace axe"
    },
    "Broadsword": {
        name: "Broadsword",
        char: "|",
        damage: {d: 10},
        slots: ['mainHand'],
        desc: "They say it's very bright to wield a broadsword"
    },
    "Flail": {
        name: "Flail",
        char: "|",
        damage: {d: 10},
        slots: ['mainHand'],
        desc: "Flailing around just got much more deadly"
    },
    "Warhammer": {
        name: "Warhammer",
        char: "|",
        damage: {d: 10},
        slots: ['mainHand'],
        desc: "What is it good for?"
    },
    "BastardSword": {
        name: "BastardSword",
        displayName: "Bastard Sword",
        char: "|",
        damage: {d: 10},
        slots: ['mainHand'],
        desc: "This sword's parents are unmarried"
    },
    "Longbow": {
        name: "Longbow",
        char: "}",
        damage: {d: 10},
        isMelee: false,
        isRanged: true,
        isTwoHanded: true,
        slots: ['mainHand'],
        desc: "Longbow is long"
    },
    "HeavyCrossbow": {
        name: "HeavyCrossbow",
        displayName: "Heavy Crossbow",
        char: "}",
        damage: {d: 10},
        isMelee: false,
        isRanged: true,
        isTwoHanded: true,
        slots: ['mainHand'],
        desc: "You are seriously underestimating how heavy this is"
    },
    "Morningstar": {
        name: "Morningstar",
        char: "|",
        damage: {d: 10},
        isMelee: true,
        isRanged: false,
        isTwoHanded: true,
        slots: ['mainHand'],
        desc: "A really, really big mace"
    },
    "Greatsword": {
        name: "Greatsword",
        char: "|",
        damage: {d: 10},
        isMelee: true,
        isRanged: false,
        isTwoHanded: true,
        slots: ['mainHand'],
        desc: "It's not a good sword, it's a great sword"
    },
    "Greatbow": {
        name: "Greatbow",
        char: "}",
        damage: {d: 8},
        isMelee: false,
        isRanged: true,
        isTwoHanded: true,
        slots: ['mainHand'],
        desc: "It's not really that great"
    },
    "Greataxe": {
        name: "Greataxe",
        char: "|",
        damage: {d: 12},
        isMelee: true,
        isRanged: false,
        isTwoHanded: true,
        slots: ['mainHand'],
        desc: "Better than the goodaxe, not as good as the greatestaxe"
    },
    "HeavyFlail": {
        name: "HeavyFlail",
        displayName: "Heavy Flail",
        char: "|",
        damage: {n: 2, d: 6},
        isMelee: true,
        isRanged: false,
        isTwoHanded: true,
        slots: ['mainHand'],
        desc: "Like a regular flail, but heavier"
    },
    "Maul": {
        name: "Maul",
        char: "|",
        damage: {n: 2, d: 6},
        isMelee: true,
        isRanged: false,
        isTwoHanded: true,
        slots: ['mainHand'],
        desc: "Does exactly what it says"
    },

}