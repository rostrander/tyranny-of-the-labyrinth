import {IUsableTemplate} from "./item";
import {CONDITIONS} from "../effects/condition";
import {Chain, Duration} from "../common";


export const POTIONS: { [key:string]: IUsableTemplate} = {
    "MinorHealingPotion": {
        name: "MinorHealingPotion",
        displayName: "Minor Healing Potion",
        char: '!',
        effectTemplates: [
            {
                name: 'HealingEffect',
                amount: 5
            }
        ],
        desc: "A minor healing potion, this heals 5HP",
    },
    BarkskinPotion: {
        name: "BarkskinPotion",
        displayName: "Barkskin Potion",
        char: '!',
        effectTemplates: [
            {
                name: 'BestowConditionEffect',
                condition: CONDITIONS.Barkskin,
            }
        ],
        desc: "This potion will turn your skin to bark, increasing your defense",
    }
};