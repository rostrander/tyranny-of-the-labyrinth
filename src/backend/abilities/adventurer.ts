import {IAbilityTemplate} from "./ability";
import {defaults, Duration, Targets} from "../common";

export const ADVENTURER_ABILITIES: IAbilityTemplate[] = [
    //region T1
    ...defaults({tier: 1},
    //region Instant
    {
        name: "BasicMeleeAttack",
        displayName: "Basic Melee Attack",
        description: "The least fancy attack possible",
        basic: true,
        targeting: Targets.Melee,
    },
    {
        name: "BasicRangedAttack",
        displayName: "Basic Ranged Attack",
        description: "The least fancy ranged attack possible",
        basic: true,
        targeting: Targets.Ranged,
        range: 10,
        toHit: "dex vs ac",
        damage: "1W+dex",
        techExtra: "Usable even without a ranged weapon",
    },
    //endregion Instant
    //region Encounter
    {
        name: "BasicHeal",
        displayName: "Basic Heal",
        description: "Heal yourself for a very small amount",
        targeting: Targets.Self,
        cooldown: Duration.Encounter,
        invokerEffects: [
            {
                name: 'HealingEffect',
                amount: 3
            }
        ],
        techExtra: "Heal 3",
    },
    {
        name: "HitHarder",
        displayName: "Hit Harder",
        description: "Exert yourself to do extra damage",
        targeting: Targets.Melee,
        cooldown: Duration.Encounter,
        damage: "2W+str",
    },
    //endregion Encounter

    //region Boss
    {
        name: "HitEvenHarder",
        displayName: "Hit Even Harder",
        description: "An all-out attack",
        targeting: Targets.Melee,
        cooldown: Duration.Boss,
        damage: "3W+str",
    },
    //endregion Boss
    ),
    //endregion T1

    //region T2
    ...defaults({tier: 2},
    //region Instant
    {
        name: "Outmoving",
        description: "A finesse attack",
        targeting: Targets.Melee,
        toHit: "dex vs ref",
        damage: "1d6+dex",
    },
    {
        name: "Outsmarting",
        description: "A mind attack",
        targeting: Targets.Ranged,
        range: 10,
        toHit: "int vs will",
        damage: "1d6+int",
    },
    //endregion
    //region Encounter
    {
        name: "Outthinking",
        description: "Hit smarter",
        targeting: Targets.Melee,
        toHit: "wis vs will",
        damage: "2d6+wis",
        cooldown: Duration.Encounter,
    },
    {
        name: "Outtalking",
        description: "Cutting insults",
        targeting: Targets.Ranged,
        range: 10,
        toHit: "cha vs will",
        damage: "2d6+cha",
        cooldown: Duration.Encounter,
    },
    //endregion

    //region Boss
    {
        name: "Outstanding",
        description: "Putting your very being into the next hit",
        targeting: Targets.Melee,
        toHit: "con vs fort",
        damage: "3d6+con",
        cooldown: Duration.Boss,
    },
    //endregion Boss
    ), //endregion T2

];