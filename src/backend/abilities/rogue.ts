import {IAbilityTemplate} from "./ability";
import {AUTOHIT, Chain, Duration, NODAMAGE, Targets} from "../common";
import {CONDITIONS} from "../effects/condition";

export const ROGUE_ABILITIES: {[key: string]: IAbilityTemplate} = {
    "DartingShiv": {
        // Darting Shiv: Move 2 toward the target, Dex vs AC, 1W+DEX
        name: "DartingShiv",
        displayName: "Darting Shiv",
        description: "Move two squares toward your enemy before hitting them",
        targeting: Targets.Ranged,
        range: 3,
        tier: 1,
        toHit: "dex vs ac",
        damage: "1w+dex",
        preRollEffects: [
            {name: "PullEffect", amount: -3}
        ],
        techExtra: "move 2",
    },
    "ReflexiveShiv": {
        // Reflexive Shiv: Dex vs Ref, 1W+DEX
        name: "ReflexiveShiv",
        displayName: "Reflexive Shiv",
        description: "Strike while ignoring your enemy's armor",
        targeting: Targets.Melee,
        tier: 1,
        toHit: "dex vs ref",
        damage: "1w+dex",
    },
    "InsultToInjury": {
        // Insult to Injury: Dex vs AC, 1W+DEX+CHA
        name: "InsultToInjury",
        displayName: "Insult to Injury",
        description: "Hurt them, then hurt their feelings",
        targeting: Targets.Melee,
        tier: 1,
        toHit: "dex vs ac",
        damage: "1w+dex+cha",
    },
    "ExploitAttack": {
        //Exploit Attack: Dex vs AC, 1W+DEX, gain CHA thorns until EoNT
        name: "ExploitAttack",
        displayName: "Exploit Attack",
        description: "Hit your enemy, and do damage if they hit you",
        targeting: Targets.Melee,
        tier: 1,
        toHit: "dex vs ac",
        damage: "1w+dex",
        invokerEffects: [{
            name: 'BestowConditionEffect',
            condition: CONDITIONS.Thorny,
            effectOverrides: {
                amount: 'cha',
            }
        }],
        techExtra: "cha thorns",
    },
    // Encounter
    "DazingShiv": {
        // Dazing Shiv: Dex vs AC, 1W+DEX, dazed
        name: "DazingShiv",
        displayName: "Dazing Shiv",
        description: "Hit so hard it dazes your enemy",
        targeting: Targets.Melee,
        tier: 1,
        cooldown: Duration.Encounter,
        toHit: "dex vs ac",
        damage: "1w+dex",
        onHitEffects: [{
            name: "BestowConditionEffect",
            condition: CONDITIONS.Dazed,
        }],
        techExtra: "daze",
    },
    "ShiftingShiv": {
        // Shifting Shiv: Dex vs Will, 2W+DEX, swap places
        name: "ShiftingShiv",
        displayName: "Shifting Shiv",
        description: "Slice your enemy and swap places with them",
        targeting: Targets.Melee,
        tier: 1,
        cooldown: Duration.Encounter,
        toHit: "dex vs will",
        damage: "2w+dex",
        onHitEffects: [{name: 'SwapPlacesEffect'}]
    },
    "BruteShiv": {
        // Brute Shiv: Dex vs AC, 2W+DEX+STR
        name: "BruteShiv",
        displayName: "Brute Shiv",
        description: "Stab your enemy very much",
        targeting: Targets.Melee,
        tier: 1,
        cooldown: Duration.Encounter,
        toHit: "dex vs ac",
        damage: "2W+dex+str",
    },
    "DefensiveShiv": {
        // Defensive Shiv: Dex vs AC, 2W+DEX, +2 AC EoNT
        name: "DefensiveShiv",
        displayName: "Defensive Shiv",
        description: "Strike and assume a defensive stance",
        targeting: Targets.Melee,
        tier: 1,
        cooldown: Duration.Encounter,
        toHit: "dex vs ac",
        damage: "2W+dex",
        invokerEffects: [{
            name: "BestowConditionEffect",
            condition: CONDITIONS.PlusDefense,
            effectOverrides: {amount: 2},
        }],
        techExtra: "+2 ac",
    },
    // Boss
    "PocketSand": {
        // PBaOE 3, Dex vs Ref, 2W+DEX, blind
        name: "PocketSand",
        displayName: "Pocket Sand",
        description: "Throw sand everywhere, wounding and blinding anyone nearby",
        targeting: Targets.PBAoE,
        radius: 2,
        tier: 1,
        cooldown: Duration.Boss,
        toHit: "dex vs ref",
        damage: "2W+dex",
        onHitEffects: [{
            name: "BestowConditionEffect",
            condition: CONDITIONS.Blind,
        }],
        techExtra: "blind",
    },
    "Bulldozer": {
        // Bulldozer: Dex vs AC, 3W+dex, push 1, gain mode: push 1 whenever you hit an enemy
        name: "Bulldozer",
        description: "Push back your enemy with every hit",
        targeting: Targets.Melee,
        tier: 1,
        cooldown: Duration.Boss,
        toHit: "dex vs ac",
        damage: "3W+dex",
        onHitEffects: [
            {name: "PushbackEffect", amount: 1},
        ],
        invokerEffects: [
            {name: "BestowConditionEffect", condition: CONDITIONS.Pushy}
        ],
        techExtra: "prone, mode: prone on hit",
    },
    // T2 Instant
    "Dart": {
        name: "Dart",
        description: "Teleport 2 squares",
        targeting: Targets.Move,
        tier: 2,
        range: 2,
        invokerEffects: [
            {name: "TeleportEffect", amount: 2},
        ],
    },
    "HobblingShiv": {
        //Hobbling Shiv: Dex vs Ref, 1W+Dex, slow
        name: "HobblingShiv",
        displayName: "Hobbling Shiv",
        description: "Slice your enemy and slow them",
        targeting: Targets.Melee,
        tier: 2,
        toHit: "dex vs ref",
        damage: "1w+dex",
        onHitEffects: [{
            name: "BestowConditionEffect",
            condition: CONDITIONS.Slow,
        }],
        techExtra: "slow",
    },
    "SlicingShiv": {
        // Slicing Shiv: Dex vs AC, 1W+Dex, ongoing 1
        name: "SlicingShiv",
        displayName: "Slicing Shiv",
        description: "Slice your enemy and bleed them",
        targeting: Targets.Melee,
        tier: 2,
        toHit: "dex vs ac",
        damage: "1w+dex",
        onHitEffects: [{
            name: 'BestowConditionEffect',
            condition: CONDITIONS.Ongoing,
            effectOverrides: {amount: 1},
        }],
        techExtra: "ongoing 1",
    },
    // T2 encounter
    PinningShiv: {
        // Pinning Shiv: Dex vs Ref, 2W+Dex, immob
        name: "PinningShiv",
        displayName: "Pinning Shiv",
        description: "Stick your enemy in one spot",
        targeting: Targets.Melee,
        cooldown: Duration.Encounter,
        tier: 2,
        toHit: "dex vs ref",
        damage: "2w+dex",
        onHitEffects: [{
            name: "BestowConditionEffect",
            condition: CONDITIONS.Immobilized,
        }],
        techExtra: "immobilize",
    },
    WideShiv: {
        // Wide Shiv: PBAoE 2, Dex vs AC, 1W+Dex
        name: "WideShiv",
        displayName: "Wide Shiv",
        description: "Hit everything within 2 squares of you",
        targeting: Targets.PBAoE,
        cooldown: Duration.Encounter,
        tier: 2,
        toHit: "dex vs ac",
        damage: "1w+dex",
        radius: 2,
    },
    SetupShiv: {
        // Setup Shiv: Dex vs AC, 2W+Dex, enemy gets -2 AC, Ref
        name: "SetupShiv",
        displayName: "Setup Shiv",
        description: "Stab your enemy to set them up for the next hit",
        targeting: Targets.Melee,
        cooldown: Duration.Encounter,
        tier: 2,
        toHit: "dex vs ac",
        damage: "2w+dex",
        onHitEffects: [{
            name: "BestowConditionEffect",
            condition: CONDITIONS.PlusDefense,
            effectOverrides: {amount: -2},
        },{
            name: "BestowConditionEffect",
            condition: CONDITIONS.PlusDefense,
            effectOverrides: {amount: -2, defense: Chain.ref},
        }],
        techExtra: "-2 ref, -2 ac",
    },
    // T2 Boss
    FastForward: {
        // Fast Forward: Mode: movement speed is +15 (or whatever makes it so moving is 1 energy)
        name: "FastForward",
        displayName: "Fast Forward",
        description: "Your movement (but not attack) speed is vastly improved",
        targeting: Targets.Self,
        cooldown: Duration.Boss,
        tier: 2,
        invokerEffects: [{
            name: "BestowConditionEffect",
            condition: CONDITIONS.FastForwarded,
        }]
    },
    StayDown: {
        // Stay Down: Dex vs Ref, 2W+dex, prone, whenever it's hit it gains prone.
        name: "StayDown",
        displayName: "Stay Down",
        description: "Knock your enemy down, and again each time they are hit",
        targeting: Targets.Melee,
        cooldown: Duration.Boss,
        tier: 2,
        toHit: "dex vs ref",
        damage: "2w+dex",
        onHitEffects: [{
            name: "BestowConditionEffect",
            condition: CONDITIONS.Prone,
        }, {
            name: "BestowConditionEffect",
            condition: CONDITIONS.Inflicting,
            effectOverrides: {
                inflictedCondition: CONDITIONS.Prone,
                inflictedWhen: Chain.onWasHit,
            }
        }],
        techExtra: "prone, enemy mode: prone when hit",
    },
    // T3 Instant
    RetreatingShiv: {
        // Retreating Shiv: Dex vs AC, 1W+DEX, you get pushed back 1
        name: "RetreatingShiv",
        displayName: "Retreating Shiv",
        description: "Make a defensive hit as you retreat",
        targeting: Targets.Melee,
        tier: 3,
        toHit: "dex vs ref",
        damage: "1w+dex",
        onHitEffects: [{
            name: 'PushbackEffect',
            amount: -1,
        }],
        techExtra: "move 1",
    },
    StuckShiv: {
        name: "StuckShiv",
        displayName: "Stuck Shiv",
        description: "Stick your enemy so quickly they just bleed",
        targeting: Targets.Melee,
        tier: 3,
        toHit: AUTOHIT,
        damage: NODAMAGE,
        onHitEffects: [{
            name: 'BestowConditionEffect',
            condition: CONDITIONS.Ongoing,
            effectOverrides: {amount: 2},
        }],
        techExtra: "ongoing 2",
    },
    // T3 Encounter
    FreeingShiv: {
        name: "FreeingShiv",
        displayName: "Freeing Shiv",
        description: "Strike everywhere around you, knocking your enemies back",
        targeting: Targets.PBAoE,
        tier: 3,
        radius: 2,
        cooldown: Duration.Encounter,
        toHit: "dex vs ref",
        damage: "1w+dex",
        onHitEffects: [{
            name: 'PushbackEffect',
            amount: 2,
        }],
        techExtra: "knockback 2",
    },
    ChallengingShiv: {
        name: "ChallengingShiv",
        displayName: "Challenging Shiv",
        description: "Lure every nearby enemy into shivving range",
        targeting: Targets.PBAoE,
        tier: 3,
        radius: 3,
        cooldown: Duration.Encounter,
        toHit: "dex vs ref",
        damage: "2w+dex",
        onHitEffects: [{
            name: 'PullEffect',
            amount: 3,
        }],
        techExtra: "pull 3",
    },
    ExtraShiv: {
        name: "ExtraShiv",
        displayName: "Extra Shiv",
        description: "Your attacks do an extra 1d6",
        targeting: Targets.Self,
        tier: 3,
        cooldown: Duration.Boss,
        invokerEffects: [{
            name: 'BestowConditionEffect',
            condition: CONDITIONS.ExtraDamageRoll,
            effectOverrides: { damage: '1d6'},
        }],
        techExtra: "mode: +1d6 damage",
    },
}