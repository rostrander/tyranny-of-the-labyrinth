import {IAbilityTemplate} from "./ability";
import {defaults, Duration, Targets} from "../common";
import {CONDITIONS} from "../effects/condition";

export const FIGHTER_ABILITIES: IAbilityTemplate[] = [
    //region T1
    ...defaults({tier: 1},
    //region Instant
    {
        name: "ShieldPush",
        displayName: "Shield Push",
        description: "Pushes your target back as well as damaging them.",
        targeting: Targets.Melee,
        onHitEffects: [{name: "PushbackEffect", amount: 1, follow: true}],
        techExtra: "knockback 1",
    },
    {
        name: "ShieldBash",
        displayName: "Shield Bash",
        description: "Daze your enemy by hitting them with your shield.",
        targeting: Targets.Melee,
        onHitEffects: [
            {name: 'BestowConditionEffect', condition: CONDITIONS.Dazed}
        ],
        techExtra: "daze",
    },
    {
        name: "BloodStrike",
        displayName: "Blood Strike",
        description: "A quick strike that inflicts ongoing damage",
        targeting: Targets.Melee,
        onHitEffects: [
            {
                name: 'BestowConditionEffect',
                condition: CONDITIONS.Ongoing,
                effectOverrides: {amount: 1}
            }
        ],
        techExtra: "ongoing 1",
    },
    {
        name: "Cleave",
        description: "Also damages an enemy next to your target",
        targeting: Targets.Melee,
        onHitEffects: [
            {
                name: "BurstDamageEffect",
                radius: 1,
                damage: "str",
                numTargets: 1,
                includeOrigin: false,
            }
        ],
        techExtra: "str damage to adjacent target",
    },
    //endregion
    //region Encounter
    {
        name: "GetOverHere",
        displayName: "Get Over Here",
        description: "Pulls an enemy toward you",
        cooldown: Duration.Encounter,
        targeting: Targets.Ranged,
        range: 10,
        toHit: "dex vs ref",
        onHitEffects: [
            {name: 'PullEffect', amount: 5}
        ],
        techExtra: "pull 5",
    },
    {
        name: "Charge",
        description: "Charge toward an enemy",
        cooldown: Duration.Encounter,
        targeting: Targets.Ranged,
        range: 10,
        preRollEffects: [
            {name: "PullEffect", amount: -5}
        ],
        techExtra: "move 5",
    },
    {
        name: "DeepStrike",
        displayName: "Deep Strike",
        description: "A heavy strike that inflicts ongoing damage",
        cooldown: Duration.Encounter,
        targeting: Targets.Melee,
        damage: "2W+str",
        onHitEffects: [
            {
                name: 'BestowConditionEffect',
                condition: CONDITIONS.Ongoing,
                effectOverrides: {amount: 3}
            }
        ],
        techExtra: "ongoing 3",
    },
    {
        name: "LashOut",
        displayName: "Lash Out",
        description: "Hit every enemy around you",
        cooldown: Duration.Encounter,
        targeting: Targets.PBAoE,
        radius: 1,
    },
    //endregion
    //region Boss
    {
        name: "Beatdown",
        description: "Knock the enemy down and weaken them",
        cooldown: Duration.Boss,
        targeting: Targets.Melee,
        damage: "3W+str",
        onHitEffects: [
            {
                name: 'BestowConditionEffect',
                condition: CONDITIONS.Weakened,
            },
            {
                name: 'BestowConditionEffect',
                condition: CONDITIONS.Prone,
            },
        ],
        techExtra: "weak, prone",
    },
    {
        name: "MaimingStrike",
        displayName: "Maiming Strike",
        description: "A very heavy strike that inflicts ongoing damage",
        cooldown: Duration.Boss,
        targeting: Targets.Melee,
        damage: "3W+str",
        onHitEffects: [
            {
                name: 'BestowConditionEffect',
                condition: CONDITIONS.Ongoing,
                effectOverrides: {amount: 5}
            }
        ],
        techExtra: "ongoing 5",
    },
    //endregion
    ),
    //endregion

    //region T2
    ...defaults({tier: 2},
    //region Instant
    {
        name: "CarefulStrike",
        displayName: "Careful Strike",
        description: "Sacrifice damage for accuracy",
        targeting: Targets.Melee,
        toHit: "str+2 vs ac",
        damage: "1W",
    },
    {
        name: "SnaringStrike",
        displayName: "Snaring Strike",
        description: "Hit your enemy and slow their movement",
        targeting: Targets.Melee,
        onHitEffects: [
            {name: "BestowConditionEffect", condition: CONDITIONS.Slow}
        ],
        techExtra: "slow",
    },
    {
        name: "PowerAttack",
        displayName: "Power Attack",
        description: "Sacrifice accuracy for damage",
        targeting: Targets.Melee,
        toHit: "str-2 vs ac",
        damage: "1W+str+2",
    },
    //endregion
    //region Encounter
    {
        name: "ResurgentStrike",
        displayName: "Resurgent Strike",
        description: "Hurt them, heal you",
        cooldown: Duration.Encounter,
        targeting: Targets.Melee,
        invokerEffects: [
            {name: "HealingEffect", amount: 0.1, amountIsMultiplier: true}
        ],
        techExtra: "heal 10%",
    },
    {
        name: "BowlOver",
        displayName: "Bowl Over",
        description: "Knock down everyone near you",
        cooldown: Duration.Encounter,
        targeting: Targets.PBAoE,
        damage: "str",
        radius: 1,
        onHitEffects: [
            {
                name: 'BestowConditionEffect',
                condition: CONDITIONS.Prone,
            },
        ],
        techExtra: "prone",
    },
    {
        name: "NotSoFast",
        displayName: "Not So Fast",
        description: "Pin an enemy in place",
        cooldown: Duration.Encounter,
        targeting: Targets.Ranged,
        range: 5,
        toHit: "dex vs ref",
        damage: "2W",
        onHitEffects: [
            {
                name: 'BestowConditionEffect',
                condition: CONDITIONS.Immobilized,
            },
        ],
        techExtra: "immobilize",
    },
    //endregion
    //region Boss
    {
        name: "PutYourBackIntoIt",
        displayName: "Put Your Back Into It",
        description: "Gain an extra weapon's worth of damage for a time",
        cooldown: Duration.Boss,
        targeting: Targets.Self,
        invokerEffects: [
            {
                name: 'BestowConditionEffect',
                condition: CONDITIONS.ExtraDamageRoll,
            }
        ],
        techExtra: "mode: roll an additional weapon die",
    },
    {
        name: "ComparativeArmor",
        displayName: "Comparative Armor",
        description: "Make your enemy vulnerable and yourself stronger",
        cooldown: Duration.Boss,
        targeting: Targets.Melee,
        damage: "3W+str",
        onHitEffects: [
            {
                name: 'BestowConditionEffect',
                condition: CONDITIONS.Impaired,
            }
        ],
        invokerEffects: [
            {
                name: 'BestowConditionEffect',
                condition: CONDITIONS.ComparativeArmorBuff,
            }
        ],
        techExtra: "impair, +2 defenses, tohit, and damage"
    }
    //endregion Boss
    ), //endregion T2
    // region T3
    ...defaults({tier: 3},
    // region Instant
    {
        name: "TerrifyingStrike",
        displayName: "Terrifying Strike",
        description: "Strike your enemy and drive them before you",
        targeting: Targets.Melee,
        onHitEffects: [{
            name: 'PushbackEffect', amount: 2
        }],
        techExtra: "knockback 2",
    },
    {
        // Come At Me - 1W+STR damage, +2 AC until EOnT
        name: "ComeAtMe",
        displayName: "Come At Me",
        description: "Strike and be protected at the same time",
        targeting: Targets.Melee,
        invokerEffects: [
            {
                name: 'BestowConditionEffect',
                condition: CONDITIONS.PlusDefense,
                effectOverrides: {amount: 2}
            }
        ],
        techExtra: "+2 ac",
    },
    // endregion Instant

    // region Encounter
    {
        //  Untouchable ENC - 2W+STR damage, Resist 2 until EOnT
        name: "Untouchable",
        description: "Hit and ignore some incoming damage",
        targeting: Targets.Melee,
        cooldown: Duration.Encounter,
        damage: '2W+str',
        invokerEffects: [
            {
                name: 'BestowConditionEffect',
                condition: CONDITIONS.Resistant,
                effectOverrides: {amount: 2}
            }
        ],
        techExtra: "resist 2",
    },
    {
        // I Am Not Messing Around ENC - 3W+str damage
        name: "IAmNotMessingAround",
        displayName: "I Am Not Messing Around",
        description: "Hit them very hard",
        targeting: Targets.Melee,
        cooldown: Duration.Encounter,
        damage: "3W+str",
    },
    // endregion Encounter

    // region Boss
    {
        name: "SecondWind",
        displayName: "Second Wind",
        description: "Heal yourself for half of your max hp",
        cooldown: Duration.Boss,
        targeting: Targets.Self,
        invokerEffects: [{
            name: 'HealingEffect',
            amount: 0.5,
            amountIsMultiplier: true,
        }],
        techExtra: "heal 50%",
    }
    // endregion Boss

    ), // endregion T3
];