import {
    AUTOHIT,
    Chain,
    Duration,
    duration_to_color,
    Energy,
    exportJsonExcept,
    InvokeResult,
    NODAMAGE,
    NOHIT,
    Targets
} from "../common";
import {Character} from "../character";
import {GameMaster} from "../gamemaster";
import {Registry} from "../registry";
import {Effect, IEffectTemplate} from "../effects/effect";
import {FIGHTER_ABILITIES} from "./fighter";
import {EFFECTS} from "../effects/active";
import {WIZARD_ABILITIES} from "./wizard";
import {DamageSpecParser, TohitSpecParser} from "../parsers";
import {ADVENTURER_ABILITIES} from "./adventurer";
import {MONSTER_ABILITIES} from "./nonboss";
import {XY} from "../interfaces";
import {CLERIC_ABILITIES} from "./cleric";
import {Dice} from "../dice";
import {ROGUE_ABILITIES} from "./rogue";
import {CombatChainArgs} from "../chain";

export interface Invokable {
    invoke(fromWho: Character, toWho: Character, gm: GameMaster): boolean;
}

export const ABILITIES = new Registry<Ability>();
export const registerAbility = ABILITIES.registryDecorator();

export interface IAbilityTemplate {
    name: string,
    displayName?: string,     // defaults to name
    description: string,
    basic?: boolean,          // default false
    cooldown?: Duration,      // default Duration.Instant
    tier?: number,            // default 1
    targeting: Targets,
    area?: number,            // default 0, not AoE
    range?: number,           // default 1
    radius?: number,          // default 0
    friendlyFire?: boolean,   // default false
    toHit?: string,           // default 'str vs ac'
    damage?: string,          // default '1W+str'
    cost?: Energy,            // default Energy.STANDARD
    totalDamageMultiplier?: number // default 1
    preRollEffects?: Array<IEffectTemplate> // default []
    onHitEffects?: Array<IEffectTemplate>   // default []
    onMissEffects?: IEffectTemplate[]       // default []
    invokerEffects?: Array<IEffectTemplate> // default []
    techExtra?: string,       // default ''

    // Likely to be part of saved data rather than templates
    currentCooldown?: number
}

const AbilityDefaults: IAbilityTemplate = {
    name: "default",
    // displayName is done programmatically
    description: "If you see this, it's a bug",
    basic: false,
    cooldown: Duration.Instant,
    tier: 1,
    targeting: Targets.None,
    area: 0,
    range: 1,
    radius: 0,
    friendlyFire: false,
    toHit: "str vs ac",
    damage: "1W+str",
    cost: Energy.STANDARD,
    totalDamageMultiplier: 1,
    preRollEffects: [],
    onHitEffects: [],
    onMissEffects: [],
    invokerEffects: [],
    techExtra: '',
};

class Invocation {
    public targets: Character[];
    public hits: Character[];
    public misses: Character[];

    constructor(public what: Ability, public invoker: Character) {
        this.hits = [];
        this.misses = [];
        this.targets = [];
    }

    public get isSelfInvocation() {
        return this.what.targeting === Targets.Self ||
            (this.targets.length === 1 && this.targets[0] === this.invoker);
    }
}

export class Ability implements IAbilityTemplate {
    public name: string;
    public displayName: string;
    public description: string;
    public basic: boolean;
    public cooldown: Duration;
    public tier: number;
    public targeting: Targets;
    public area: number;
    public range: number;
    public radius: number;
    public friendlyFire: boolean;
    public toHit: string;
    public damage: string;
    public cost: Energy;
    public totalDamageMultiplier: number;
    public preRollEffects: Array<IEffectTemplate>;
    public onHitEffects: Array<IEffectTemplate>;
    public onMissEffects: IEffectTemplate[];
    public invokerEffects: Array<IEffectTemplate>;
    public techExtra: string;

    public currentCooldown: number;

    // Realized
    protected preRoll: Array<Effect>;
    protected onHit: Array<Effect>;
    protected onMiss: Array<Effect>;
    protected invokers: Array<Effect>;

    constructor(template: IAbilityTemplate) {
        Object.assign(this, JSON.parse(JSON.stringify(AbilityDefaults)));
        Object.assign(this, JSON.parse(JSON.stringify(template)));
        this.displayName = template.displayName || this.name;

        this.preRoll = EFFECTS.mapped(this.preRollEffects);
        this.onHit = EFFECTS.mapped(this.onHitEffects);
        this.onMiss = EFFECTS.mapped(this.onMissEffects);
        this.invokers = EFFECTS.mapped(this.invokerEffects);

        if(this.currentCooldown === undefined) this.resetCooldown();
    }

    public get fgcolor(): string {
        return duration_to_color(this.cooldown);
    }

    public toJSON() {
        let result = exportJsonExcept(this,
            'preRoll',
            'onHit',
            'invokers',
        )
        return result;
    }

    public startCooldown() {
        // TODO: Chain if needed
        this.currentCooldown = this.cooldown;
    }

    public doCooldown(amount=1) {
        // TODO: Chain if needed
        if(this.currentCooldown > 0) {
            this.currentCooldown = Math.max(this.currentCooldown -= amount, 0);
        }
    }

    public resetCooldown() {
        switch (this.cooldown) {
            case Duration.Instant:
            case Duration.Encounter:
            case Duration.Boss:
                this.currentCooldown = 0;
                break;
            default:
                this.currentCooldown = this.cooldown;
        }
    }

    public isReady() {
        return this.currentCooldown === 0;
    }

    public isMelee() {
        return this.targeting === Targets.Melee;
    }

    public isRanged() {
        switch(this.targeting) {
            case Targets.Ranged:
            case Targets.TAoE:
                return true;
            default:
                return false;
        }
    }

    public needsExplicitTarget() {
        return !(
            this.targeting === Targets.Self ||
            this.targeting === Targets.PBAoE ||
            this.targeting === Targets.None
        )
    }

    protected techRange(): string {
        let baseRange = Targets[this.targeting];
        let rangeNum = '';
        switch(this.targeting) {
            case Targets.Ranged:
                rangeNum = ` ${this.range}`;
                break;
            case Targets.PBAoE:
                rangeNum = ` ${this.radius}`;
                break;
            case Targets.TAoE:
                rangeNum = ` ${this.radius}; range ${this.range}`;
        }

        return `${baseRange}${rangeNum}`;
    }

    protected techToHit(): string {
        let tohit = this.toHit;
        if(tohit === AUTOHIT) return 'Autohit';
        if(tohit === NOHIT) return '';

        return tohit;
    }

    protected techDamage(): string {
        let dmg = this.damage;
        if(dmg === NODAMAGE) return '';

        return `${dmg} damage`;
    }

    public techDescription(): string {
        let parts = [];

        parts.push(Duration[this.cooldown]);

        parts.push(this.techRange());

        let tohit = this.techToHit();
        if(tohit.length > 0) parts.push(tohit);

        let dmg = this.techDamage();
        if(dmg.length > 0) parts.push(dmg);

        if(this.techExtra && this.techExtra.length > 0) {
            parts.push(this.techExtra);
        }

        return parts.join(", ");
    }

    protected determineTargets(invocation: Invocation, toWhere: XY, gm: GameMaster) {
        switch(this.targeting) {
            case Targets.Melee:
            case Targets.Ranged:
                let tile = gm.map.tileAt(toWhere);
                let target = tile.character;
                let targetable = gm.canTarget(invocation.invoker, invocation.what, target);
                if(target && targetable) {
                    invocation.targets.push(tile.character);
                }
                break;
            case Targets.PBAoE:
            case Targets.TAoE:
                for(let tile of gm.tilesInBurst(toWhere, this.radius)) {
                    let target = tile.character;
                    let targetable = gm.canTarget(invocation.invoker, invocation.what, target);
                    if(target && targetable) {
                        invocation.targets.push(target);
                    }
                }
                break;
            case Targets.Self:
                invocation.targets.push(invocation.invoker);
                break;
        }
    }
    
    protected determineHits(invocation: Invocation, toWhere: XY, gm: GameMaster) {
        let dispFrom = invocation.invoker.displayName || invocation.invoker.name;
        for(let toWho of invocation.targets) {
            let dispTo = toWho.displayName || toWho.name;
            let broadcastMsg = `The ${dispFrom} uses ${this.displayName} against ${dispTo}.`;
            let msgTo = `The ${dispFrom} uses ${this.displayName} against you.`;
            if(!invocation.isSelfInvocation) {
                toWho.message(msgTo);
            }
            toWho.broadcast(gm, broadcastMsg);
            let msgFrom = `You use ${this.displayName} against the ${dispTo}`;
            // Self invocations have already sent a 'you use' message.
            if(!invocation.isSelfInvocation) {
                invocation.invoker.message(msgFrom);
            }

            invocation.invoker.broadcast(gm, broadcastMsg);

            for(let effect of this.preRoll) {
                effect.invoke(invocation.invoker, toWho, gm);
            }

            // toHit portion, if needed
            if(this.targeting !== Targets.Self && this.toHit !== NOHIT) {
                if (this.toHit !== AUTOHIT) {
                    let parser = new TohitSpecParser();
                    let roll = parser.act(this.toHit, invocation.invoker, this, toWho, gm);

                    let hit = roll.value >= parser.target.value;
                    let hitStr = hit ? "HIT" : "MISS";
                    let msgRoll = `Roll: ${roll.value} vs ${parser.target.value}: ${hitStr}!`;
                    toWho.message(msgRoll);
                    toWho.broadcast(gm, msgRoll);
                    invocation.invoker.message(msgRoll);
                    invocation.invoker.broadcast(gm, msgRoll);
                    if (hit) {
                        invocation.hits.push(toWho);
                    } else {
                        invocation.misses.push(toWho);
                    }
                } else {
                    invocation.hits.push(toWho);
                }
            }
        }
    }
    
    protected doHits(invocation: Invocation, toWhere: XY, gm: GameMaster): boolean {
        let expend = false;
        for(let toWho of invocation.hits) {
            expend = true;
            // Before we do damage, we do on hit effects
            // (This is so that e.g. enemy death doesn't prevent beneficial
            //  effects from happening to the player or e.g. contagion from spreading,
            //  or for last minute on-hit effects from saving them
            let combatArgs: CombatChainArgs = {
                fromWho: invocation.invoker,
                toWho,
                gm
            }
            invocation.invoker.chain(Chain.onHitEnemy, Dice.zero, combatArgs);
            toWho.chain(Chain.onWasHit, Dice.zero, combatArgs);
            for(let effect of this.onHit) {
                effect.immuneFaction = invocation.invoker.faction;
                effect.invoke(invocation.invoker, toWho, gm);
            }

            if(this.damage !== NODAMAGE) {
                let parser = new DamageSpecParser();
                let dmg = parser.act(this.damage, invocation.invoker, this, toWho, gm);

                invocation.invoker.addDamageBonuses(dmg, toWho);
                gm.damageCharacter(invocation.invoker, toWho, dmg);
            }
        }
        return expend;
    }

    protected doMisses(invocation: Invocation, toWhere: XY, gm: GameMaster): boolean {
        let expend = false;
        for(let toWho of invocation.misses) {
            for(let effect of this.onMiss) {
                expend = true;
                effect.immuneFaction = invocation.invoker.faction;
                effect.invoke(invocation.invoker, toWho, gm);
            }
        }
        return expend;
    }

    protected hasNoTarget() {
        return this.targeting === Targets.Self ||
            this.targeting === Targets.None ||
            this.targeting === Targets.Move;
    }

    public invokeAt(fromWho: Character, toWhere: XY, gm: GameMaster): InvokeResult {
        let invokeResult = new InvokeResult(this.cooldown !== Duration.Boss);
        let result = new Invocation(this, fromWho);
        let dispFrom = fromWho.displayName || fromWho.name;

        this.determineTargets(result, toWhere, gm);

        if(this.hasNoTarget()) {
            invokeResult.expend();
            fromWho.message(`You use ${this.displayName}`);
            fromWho.broadcast(gm, `${dispFrom} uses ${this.displayName}`)
        }

        this.determineHits(result, toWhere, gm);
        invokeResult.expendMaybe(this.doHits(result, toWhere, gm));
        invokeResult.expendMaybe(this.doMisses(result, toWhere, gm));

        // Effects that always happen regardless of hit/miss:
        // Invoker effects happen to the person who used the ability rather than the target
        for(let effect of this.invokers) {
            invokeResult.expend();
            effect.srcLoc = toWhere;
            effect.invoke(fromWho, fromWho, gm);
        }
        return invokeResult;
    }
}

/** GENERAL ABILITY DISTRIBUTION DESIGN:
1: Basic, useful attacks
2: Essentially more options
3: While these don't (necessarily) do more damage, they should be better mechanically (status, knockback, etc)
4: Straight-up upgrades from before

Instant do 1x (1W, 1d8)
Encounters do 2x (1x aoe on a non-aoe character), more powerful statuses
Boss does up to 4x.  AoE up to 3x.  "Mode" powers.

Distribution: instant/encounter/boss per tier
1: 4/4/2
2: 3/3/2
3: 2/2/1
4: 1/1/1
**/

ABILITIES.adoptFromTemplates(Ability, ADVENTURER_ABILITIES);
ABILITIES.adoptFromTemplates(Ability, FIGHTER_ABILITIES);
ABILITIES.adoptFromTemplates(Ability, WIZARD_ABILITIES);
ABILITIES.adoptFromTemplateMap(Ability, CLERIC_ABILITIES);
ABILITIES.adoptFromTemplateMap(Ability, ROGUE_ABILITIES);

ABILITIES.adoptFromTemplateMap(Ability, MONSTER_ABILITIES);
