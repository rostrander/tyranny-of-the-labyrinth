import {IAbilityTemplate} from "./ability";
import {AUTOHIT, Energy, NOHIT, Targets} from "../common";

export const MONSTER_ABILITIES: {[key:string]: IAbilityTemplate} = {
    "ImpLightning": {
        name: 'ImpLightning',
        displayName: "Imp Lightning",
        description: 'A snap of unerring lightning damage',
        targeting: Targets.Ranged,
        range: 10,
        tier: 0,
        toHit: AUTOHIT,
        damage: '1',
    },
    "MinionStrike": {
        name: "MinionStrike",
        displayName: "Minion Strike",
        description: "A minion's strike",
        targeting: Targets.Melee,
        tier: 0,
        damage: '2',
    },
    MonsterStrike: {
        name: "MonsterStrike",
        displayName: "Basic Melee Attack",
        description: "The least fancy attack possible",
        targeting: Targets.Melee,
        tier: 0,
    },
    MonsterRanged: {
        name: "MonsterRanged",
        displayName: "Basic Ranged Attack",
        description: "The least fancy ranged attack possible",
        targeting: Targets.Ranged,
        range: 10,
        toHit: "dex vs ac",
        damage: "1W+dex",
        tier: 0,
    },
    "Web": {
        name: "Web",
        description: "Creates a zone of slowness at its target",
        targeting: Targets.Ranged,
        range: 10,
        tier: 0,
        toHit: NOHIT,
        invokerEffects: [{
            name: "ZoneEffect",
            radius: 0,
            zonePassives: [{
                name: "ExpendEnergyEffect",
                amount: 1,
                amountIsMultiplier: true,
                energyCategory: Energy.MOVE,
            }],
        }]
    }
}