import {Character} from "./character";
import {Tile, TileMap} from "./world";
import * as ROT from "rot-js";
import {GameMaster, Tier} from "./gamemaster";
import {MeleeCombatBehavior} from "./behavior";
import {Armor, Item, UsableItem, Weapon} from "./items/item";
import {POTIONS} from "./items/potions";
import {ARMOR} from "./items/armors";
import {WEAPONS} from "./items/weapons";
import {CLASSES} from "./classes";
import {Faction} from "./interfaces";
import {ABILITIES, IAbilityTemplate} from "./abilities/ability";
import {Duration, EquipmentTag} from "./common";
import {Condition} from "./effects/condition";

export abstract class AbstractGameFactory { // What is this, Java?
    abstract newGame(): GameMaster;

    public gm:GameMaster;
    public player:Character;

    protected easySetup(player?: Character, skipRoomGeneration=false) {
        player = player ?? new Character({
            char: '@',
            name: 'Test McGee',
            baseMaxHp: 10,
            hpPerLevel: 5,
            faction: Faction.Player,
            loc: {x: 1, y: 1},
            proficiencies: [EquipmentTag.Everything],
            description: "If you see this, it's a bug",
        });
        let map = new TileMap(40, 40);
        let mapGen = new ROT.Map.Arena(40, 40);
        mapGen.create(map.rotjsMapCallback());
        this.gm = new GameMaster({player, map, skipRoomGeneration});
        return player;
    }
}

export class RegularGameFactory extends AbstractGameFactory {
    public static make(player: Character): GameMaster {
        let rgf = new RegularGameFactory();
        return rgf.newGame(player);
    }

    newGame(player?: Character): GameMaster {
        let map = new TileMap(100, 100);
        let mapGen = new ROT.Map.Digger(100, 100)
        mapGen.create(map.rotjsMapCallback());
        player.stats = {
            str: 10,
            dex: 10,
            con: 10,
            int: 10,
            wis: 10,
            cha: 10,
        };

        let tutorialMode = false;
        if(player) {
            tutorialMode = player.className === "Adventurer";
        }
        let tier = new Tier({
            tierNum: 1,
            bossesThisTier: 2,
            topTier: 3,
        });
        if(tutorialMode) {
            tier = new Tier({
                tierNum: 1,
                bossesThisTier: 1,
                topTier: 2,
            });
        }

        player.fgcolor = CLASSES.Adventurer.fgcolor;
        this.gm = new GameMaster({player, map, tier, tutorialMode});
        return this.gm;
    }
}

export class DebugGameFactory extends AbstractGameFactory {
    
    public static make(player?: Character): GameMaster {
        let dgf = new DebugGameFactory();
        return dgf.newGame(player);
    }

    newGame(player?: Character): GameMaster {
        player = new Character(CLASSES.Adventurer);
        player.name = "Test McGee";
        player.loc = {x: 1, y: 1};
        //player.hp = 1000;
        this.easySetup(player);

        for(let name of player.classAbilityNames) {
            let template = ABILITIES.templateHolderFromName(name).template as IAbilityTemplate;
            if(template.tier === 1) player.gainAbility(name);
        }
        // zones again
        player.gainAbility("ZoneOfError");
        // teleportesting
        player.gainAbility("Dart");
        // imp testing
        player.gainAbility("SummonImp");
        // stat screen testing
        player.stats = {
            str: 13,
            con: 10,
            dex: 10,
            int: 8,
            wis: 10,
            cha: 9,
        }
        player.gainAbility("ShieldingSiege");

        // Make player invincible
        let invincible = new Condition({
            name: "NighInvulnerable",
            displayName: "Nigh Invulnerable",
            passiveEffectTemplates: [{
                name: "IncomingDamageEffect",
                amount: -10000,
            }],
            duration: Duration.AlwaysOn,
        });
        let testReminder = new Condition({
            name: "TestMode",
            displayName: "Test Mode",
            passiveEffectTemplates: [],
            duration: Duration.AlwaysOn,
        })
        player.receiveCondition(invincible);
        player.receiveCondition(testReminder);

        this.gm.map.setTile(Tile.rotjsValueToTile(1, {x: 5, y: 5}));
        let npc = new Character({
            char: 'k',
            name: 'Kobold',
            loc: {x: 13, y: 13},
            baseMaxHp: 1,
            hpPerLevel: 0,
            description: "test monster",
        });
        this.gm.addCharacter(npc);
        npc.setBehavior(new MeleeCombatBehavior(), this.gm);

        let item = new Item({name: "Thingie", char: "*", desc: 'a thing'});
        this.gm.map.placeItem(item, {x: 5, y: 3});
        for(let i=0; i < 10; i++) {
            let item = new Item({name: `Thingie ${i}`, char: "%", desc: `A ${i} kinda thing`});
            this.gm.map.placeItem(item, {x: 4, y: 3});
        }
        let healie = new UsableItem(POTIONS.MinorHealingPotion);
        this.gm.map.placeItem(healie, {x: 4, y: 2});
        let bark = new UsableItem(POTIONS.BarkskinPotion);
        this.gm.map.placeItem(bark, {x: 4, y: 2});

        let padding = new Armor(ARMOR.PaddedArmor);
        this.gm.map.placeItem(padding, {x: 3, y: 2});

        let mace = new Weapon(WEAPONS.Mace);
        this.gm.map.placeItem(mace, {x: 5, y: 2});
        let mace2 = new Weapon(WEAPONS.Mace);
        this.gm.map.placeItem(mace2, {x: 6, y: 2});

        let shield = new Armor(ARMOR.Shield);
        this.gm.map.placeItem(shield, {x: 4, y: 3});

        //this.gm.tutorialMode = true;

        return this.gm;
    }
}
