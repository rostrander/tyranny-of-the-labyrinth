import {Registry} from "../registry";
import {Invokable} from "../abilities/ability";
import {Character} from "../character";
import {GameMaster} from "../gamemaster";
import {Chain, Comparison, ConditionType, Energy, exportJsonExcept, HpType, LastAction} from "../common";
import {AddableResult} from "../dice";
import {IConditionTemplate} from "./condition";
import {chainDispatch, DieCategory, IChainReceiver} from "../chain";
import {Faction, ICharacterTemplate, XY} from "../interfaces";
import {DamageSpecParser} from "../parsers";

export interface IEffectTemplate {
    name: string;
    amount?: number | string;
    amountIsMultiplier?: boolean;  // default false
    srcLoc?: XY;
    immuneFaction?: Faction;
    targetFaction?: Faction;
    onBehalfOf?: Character

    // BestowCondition
    condition?: IConditionTemplate;
    conditionOverride?: Partial<IConditionTemplate>;
    effectOverrides?: Partial<IEffectTemplate>;
    bestowerImmune?: boolean
    // Defense
    defense?: Chain;
    // ExpendEnergy
    energyCategory?: Energy;
    // Pushback
    follow?: boolean;
    // BurstDamage
    radius?: number;
    damage?: string;  // Damage spec
    numTargets?: number;  // Default = 0, meaning all targets
    includeOrigin?: boolean; // default true
    friendlyFire?: boolean; // default false
    // ExtraRoll
    dieCategory?: DieCategory;
    // Zone
    zonePassives?: IEffectTemplate[];
    // SpawnCreature
    creatures?: ICharacterTemplate[];
    // Conditionals
    conditionType?: ConditionType;
    hpType?: HpType;
    comparison?: Comparison;
    lastTurn?: LastAction;
    perAdjacent?: number;
    maxAdjacentBonus?: number;
    targetAdjacentAllies?: number;
    // ToHitAndDamageEffect
    damageAmount?: number;
    // Infliction
    inflictedCondition?: IConditionTemplate;
    inflictedOverride?: any;
    inflictedWhen?: Chain;
    // StatBonus
    stat?: string;
}

export function numberAmount(
    amount: number | string,
    onBehalfOf: Character,
    gm?: GameMaster,
): number {
    let amt = Number(amount);
    if(isNaN(amt)) {
        let parser = new DamageSpecParser();
        let dmg = parser.act(
            amount as string,
            onBehalfOf,
            null,
            onBehalfOf,
            gm
        );
        dmg.minimum = 0;
        amt = dmg.value;
    }
    return amt;
}

export abstract class Effect implements Invokable, IEffectTemplate {
    //protected args: IEffectTemplate;

    public name: string;
    public amount: number | string;

    public tookEffect: boolean;
    public srcLoc: XY;
    public immuneFaction: Faction;
    public onBehalfOf: Character;

    public constructor(protected args: IEffectTemplate) {
        this.setDefaults();
        Object.assign(this, JSON.parse(JSON.stringify(args)));
        this.tookEffect = false;
    }

    public toJSON() {
        let result = exportJsonExcept(this,
            'onBehalfOf',
        )
        return result;
    }

    public numberAmount(onBehalfOf: Character): number {
        return numberAmount(this.amount, onBehalfOf);
    }

    public abstract invoke(fromWho: Character, toWho: Character, gm: GameMaster): boolean;

    protected setDefaults() {}
}

export abstract class PassiveEffect implements IChainReceiver, IEffectTemplate {
    protected args: IEffectTemplate;

    public name: string;
    public amount: number | string;
    public defense: Chain;

    public tookEffect: boolean;
    public immuneFaction: Faction;

    public onBehalfOf: Character;

    public constructor(args: IEffectTemplate) {
        Object.assign(this, JSON.parse(JSON.stringify(args)));
        this.tookEffect = false;
    }

    public toJSON() {
        let result = exportJsonExcept(this,
            'onBehalfOf',
        )
        return result;
    }

    public recvChain(topic: Chain, result: AddableResult, onBehalfOf: Character, args?: any): boolean {
        return chainDispatch(this, topic, result, onBehalfOf, args);
    }
}
