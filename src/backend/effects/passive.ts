import {IEffectTemplate, numberAmount, PassiveEffect} from "./effect";
import {Chain, Comparison, ConditionType, Energy, HpType, LastAction} from "../common";
import {AddableResult, Dice} from "../dice";
import {Registry} from "../registry";
import {CombatChainArgs, DieCategory, DieRollArgs, StatArgs} from "../chain";
import {Faction} from "../interfaces";
import {Character} from "../character";
import {IConditionTemplate} from "./condition";
import {BestowConditionEffect, HealingEffect, PushbackEffect} from "./active";
import {GameMaster} from "../gamemaster";
import {Tile} from "../world";


export const PASSIVEEFFECTS = new Registry<PassiveEffect>();
export const registerPassive = PASSIVEEFFECTS.registryDecorator();

//region Passive support classes

class SimpleModifierEffect extends PassiveEffect {
    amountIsMultiplier: boolean;
    additionalAmount: number;

    protected affect(result: AddableResult, args?: any, amount = 0): boolean {
        this.additionalAmount = 0;
        if(!this.shouldAffect(args)) return false;

        let amt = amount || this.amount
        amt = numberAmount(amt, this.onBehalfOf) + Math.floor(this.additionalAmount);

        this.tookEffect = true;
        let label = this.amount > 0 ? "bonus" : "penalty";
        if(this.amountIsMultiplier) {
            result.addMultiplier(amt, label);
            return true;
        }
        result.addOrAdopt(amt, label);

        return true;
    }

    protected shouldAffect(args?: any): boolean {
        return true;
    }
}


export class ConditionalEffect extends SimpleModifierEffect {
    conditionType: ConditionType;

    // Last turn
    lastTurn: LastAction;

    // HP
    hpType: HpType;
    comparison: Comparison;
    targetFaction: Faction;  // In this case, Faction.Player means 'self'

    // Adjacency
    perAdjacent: number;
    maxAdjacentBonus: number;
    targetAdjacentAllies: number;

    constructor(args: any) {
        super(args);
        this.conditionType = this.conditionType || ConditionType.unconditional;
    }

    //region Convenience getters for the enums
    protected get bloody() { return this.hpType === HpType.Bloody }
    //endregion Convenience getters for the enums

    protected shouldAffect(args?: CombatChainArgs): boolean {
        switch(this.conditionType) {
            case ConditionType.unconditional:
                return true;
            case ConditionType.hp:
                return this.hpConditionMet(args);
            case ConditionType.lastTurn:
                return this.lastTurnConditionMet(args);
            case ConditionType.enemyAdjacent:
                return this.enemyAdjacencyConditionMet(args);
            case ConditionType.wallAdjacent:
                return this.wallAdjacencyConditionMet(args);
            case ConditionType.enemyAdjacentAllies:
                return this.enemyAdjacentAlliesConditionMet(args);
            default:
                return false;
        }
    }

    protected lastTurnConditionMet(args: CombatChainArgs): boolean {
        if(!args.fromWho) return false;
        return args.fromWho.didLastTurn(this.lastTurn);
    }

    protected hpConditionMet(args: CombatChainArgs) {
        if(this.targetFaction === undefined || this.targetFaction === Faction.Nobody) return true;
        if(this.targetFaction === Faction.Player) {
            return this.appliesToGiver(args);
        } else {
            return this.appliesToEnemy(args);
        }
    }

    protected appliesToEnemy(args: CombatChainArgs): boolean {
        if(!args.toWho) return false;
        if(this.bloody && args.toWho.isBloody) return true;

        let myHp = this.fetchHp(args.fromWho);
        let theirHp = this.fetchHp(args.toWho);

        return this.compare(myHp, theirHp);
    }

    protected compare(src: number, target: number): boolean {
        switch (this.comparison) {
            // Seemingly backwards because the wording is e.g.
            // "with more maxHP than you"
            case Comparison.GreaterThan:
                return target > src;
            case Comparison.LessThan:
                return target < src;
            case Comparison.Equal:
                return target == src;
            default:
                return false;
        }
    }

    protected fetchHp(who: Character): number {
        if(this.hpType === HpType.Max) {
            return who.maxHp;
        } else {
            return who.hp;
        }
    }

    protected appliesToGiver(args: CombatChainArgs): boolean {
        // Currently 'bloody' is the only one with 'self' faction
        return this.bloody && args.fromWho && args.fromWho.isBloody;
    }

    protected enemyAdjacencyConditionMet(args: CombatChainArgs): boolean {
        if(!args.gm) return false;
        return this.adjacencyConditionMet(args, (tile)=> {
            return tile.character && tile.character !== args.fromWho;
        }) > 0;
    }

    protected enemyAdjacentAlliesConditionMet(args: CombatChainArgs): boolean {
        if(!args.gm) return false;
        let swapArgs = {
            fromWho: args.toWho,
            toWho: args.fromWho,
            gm: args.gm,
        }
        let allies = this.adjacencyConditionMet(swapArgs, (tile) => {
            return tile.character &&
                tile.character !== args.toWho &&
                tile.character.faction === args.toWho.faction;
        });
        this.additionalAmount = 0;
        if(this.compare(this.targetAdjacentAllies, allies)) return true;
        return false;
    }

    protected wallAdjacencyConditionMet(args: CombatChainArgs): boolean {
        if(!args.gm) return false;
        return this.adjacencyConditionMet(args, (tile) => {
            return tile.char === "#";
        }) > 0;
    }

    protected adjacencyConditionMet(args: CombatChainArgs, filter: (tile: Tile)=>boolean): number {
        let numAdjacent = 0;
        for(let tile of args.gm.tilesInBurst(args.fromWho.loc, 1)) {
            if(filter(tile)) {
                numAdjacent += 1;
            }
        }
        let bonus = Math.floor(numAdjacent * this.perAdjacent);
        if(bonus <= 0) return 0;
        this.additionalAmount = bonus;
        if(this.maxAdjacentBonus && this.maxAdjacentBonus > 0) {
            this.additionalAmount = Math.min(this.maxAdjacentBonus, this.additionalAmount);
        }
        return this.additionalAmount;
    }
}

//endregion Passive support classes

@registerPassive
export class DefenseEffect extends ConditionalEffect {

    public chainGeneral(topic: Chain, result: AddableResult, args?: any): boolean {
        if(
            topic === Chain.AllDefenses ||
            topic === this.defense ||
            (this.topicIsDefense(topic) && this.defense === Chain.AllDefenses)
        ) {
            return this.affect(result, args);
        }
        return true;
    }

    protected topicIsDefense(topic: Chain): boolean {
        switch(topic) {
            case Chain.AllDefenses:
            case Chain.ac:
            case Chain.fort:
            case Chain.ref:
            case Chain.will:
                return true;
            default:
                return false;
        }
    }

    protected shouldAffect(args?: CombatChainArgs): boolean {
        // Reversing for this because super assumes that damage is outgoing
        // and defenses are obviously incoming
        let reversed = {fromWho: args.toWho, toWho: args.fromWho, gm: args.gm}
        return super.shouldAffect(reversed);
    }

}

@registerPassive
export class ExpendEnergyEffect extends SimpleModifierEffect {
    energyCategory: Energy;

    public chainExpendEnergy(result: AddableResult): boolean {
        const energyMatch = (this.energyCategory as number) === result.originalValue;
        if(this.energyCategory === Energy.ALL || energyMatch) {
            return this.affect(result);
        }
        return true;
    }

}

// This modifies Outgoing damage, specifically
@registerPassive
export class DamageModifierEffect extends SimpleModifierEffect {
    public chainDamageModify(result: AddableResult, args: CombatChainArgs): boolean {
        return this.affect(result, args);
    }
}

@registerPassive
export class ToHitModifierEffect extends SimpleModifierEffect {
    public chainToHit(result: AddableResult, args?: CombatChainArgs): boolean {
        return this.affect(result, args);
    }
}

@registerPassive
export class ToHitAndDamageEffect extends SimpleModifierEffect {
    // If unset, this defaults to `amount`
    public damageAmount: number;

    public chainDamageModify(result: AddableResult, args: CombatChainArgs): boolean {
        return this.affect(result, args, this.damageAmount);
    }

    public chainToHit(result: AddableResult, args?: CombatChainArgs): boolean {
        return this.affect(result, args);
    }
}

@registerPassive
export class StatBonusEffect extends SimpleModifierEffect {
    public amount: number;
    public stat: string;

    public chainStatBonus(result: AddableResult, args?: StatArgs): boolean {
        if(args && args.statName === this.stat) {
            return this.affect(result, args, this.amount);
        }
    }
}

// This prevents the character from moving
@registerPassive
export class CanMoveEffect extends PassiveEffect {
    public chainCanMove(result: AddableResult): boolean {
        result.fail("You cannot move");
        this.tookEffect = true;
        return true;
    }
}

// This prevents the character from doing anything
@registerPassive
export class CanActEffect extends PassiveEffect {
    public chainCanAct(result: AddableResult): boolean {
        result.fail("You cannot act");
        this.tookEffect = true;
        return true;
    }
}

// Prevents the player from seeing
@registerPassive
export class BlindEffect extends PassiveEffect {
    public chainCanSee(result: AddableResult): boolean {
        result.fail("You are blind");
        this.tookEffect = true;
        return true;
    }
}

// This rolls additional dice
@registerPassive
export class ExtraRollEffect extends PassiveEffect {
    public dieCategory: DieCategory;
    public damage: string;

    public chainDieRoll(result: AddableResult, args: DieRollArgs): boolean {
        if(this.dieCategory === args.category) {
            return this.affect(result, args);
        }
        return true;
    }

    public affect(result: AddableResult, args: DieRollArgs): boolean {
        this.tookEffect = true;
        for(let i=0; i < this.amount; i++) {
            let die = new Dice(args.toRoll);
            if(this.damage) {
                die = Dice.fixed(numberAmount(this.damage, this.onBehalfOf, args.gm));
            }
            result.addDice(args.gm.roll(die), "Extra die roll");
        }
        return true;
    }
}

// Sets the 'advantage status' which makes the player re-roll and take high/low
// (depending on 'amount')
@registerPassive
export class AdvantageEffect extends SimpleModifierEffect {
    public chainAdvantage(result: AddableResult): boolean {
        return this.affect(result);
    }
}

@registerPassive
export class HealModifyEffect extends SimpleModifierEffect {
    public chainHealModify(result: AddableResult): boolean {
        return this.affect(result);
    }
}

@registerPassive
export class IncomingDamageEffect extends SimpleModifierEffect {
    public chainIncomingDamage(result: AddableResult, args: CombatChainArgs): boolean {
        return this.affect(result);
    }
}

@registerPassive
export class SaveModifyEffect extends SimpleModifierEffect {
    public chainSaveModify(result: AddableResult): boolean {
        return this.affect(result);
    }
}

@registerPassive
export class InflictionEffect extends PassiveEffect {
    public inflictedCondition: IConditionTemplate;
    public inflictedOverride: any;
    public inflictedWhen: Chain;

    constructor(args: IEffectTemplate) {
        super(args);
        if(!args.inflictedWhen) {
            this.inflictedWhen = Chain.onHitEnemy;
        }
    }

    public chainOnHitEnemy(result: AddableResult, args?: CombatChainArgs): boolean {
        if (!args.gm) return false;
        if (this.inflictedWhen !== Chain.onHitEnemy) return false;
        return this.affect(args.fromWho, args.toWho, args.gm);
    }

    public chainOnWasHit(result: AddableResult, args?: CombatChainArgs): boolean {
        if(!args.gm) return false;
        if (this.inflictedWhen !== Chain.onWasHit) return false;
        return this.affect(args.fromWho, args.toWho, args.gm);
    }

    protected affect(fromWho: Character, toWho: Character, gm: GameMaster): boolean {
        let effectTemplate: IEffectTemplate = {
            name: "BestowConditionEffect",
            condition: this.inflictedCondition,
        }
        if (this.inflictedOverride) {
            effectTemplate.effectOverrides = this.inflictedOverride;
        }
        let bce = new BestowConditionEffect(effectTemplate);
        bce.invoke(fromWho, toWho, gm);
        return true;
    }
}

@registerPassive
export class LifestealEffect extends PassiveEffect {
    public amount: number;
    public amountIsMultiplier: boolean;

    public chainOnHitEnemy(result: AddableResult, args?: CombatChainArgs): boolean {
        if (!args.gm) return false;
        return this.affect(args.fromWho, args.toWho, args.gm);
    }

    protected affect(fromWho: Character, toWho: Character, gm: GameMaster): boolean {
        let effectTemplate: IEffectTemplate = {
            name: "HealEffect",
            amount: this.amount,
            amountIsMultiplier: this.amountIsMultiplier,
        }
        let heals = new HealingEffect(effectTemplate);
        // no toWho because that's who the strike hit!  We don't want to heal _them_
        heals.invoke(fromWho, fromWho, gm);
        return true;
    }
}

@registerPassive
export class PushyEffect extends PassiveEffect {
    public chainOnHitEnemy(result: AddableResult, args?: CombatChainArgs): boolean {
        if (!args.gm) return false;

        let effectTemplate: IEffectTemplate = {
            name: "PushbackEffect",
            amount: this.amount,
        }
        let push = new PushbackEffect(effectTemplate);
        push.invoke(args.fromWho, args.toWho, args.gm);

        return true;
    }
}

@registerPassive
export class ThornsEffect extends PassiveEffect {
    public chainOnWasHit(result: AddableResult, args?: CombatChainArgs): boolean {
        if (!args.gm) return false;
        let amount = numberAmount(this.amount, args.toWho);
        let dmg = Dice.fixed(amount);
        dmg.add(result.value, "Original value");
        args.gm.damageCharacter(args.toWho, args.fromWho, dmg);
        return true;
    }
}
