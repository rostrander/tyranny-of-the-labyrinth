import * as ROT from 'rot-js';

import {Character} from "../character";
import {GameMaster} from "../gamemaster";
import {Effect, IEffectTemplate} from "./effect";
import {Registry} from "../registry";
import {Condition, IConditionTemplate} from "./condition";
import {Point} from "../common";
import {Dice} from "../dice";
import {Tile, Zone} from "../world";
import {DamageSpecParser} from "../parsers";
import {Faction, ICharacterTemplate} from "../interfaces";

export const EFFECTS = new Registry<Effect>();
export const registerEffect = EFFECTS.registryDecorator();

@registerEffect
export class HealingEffect extends Effect {
    amountIsMultiplier: boolean;

    public invoke(fromWho: Character, toWho: Character, gm: GameMaster): boolean {
        let healAmount = this.numberAmount(fromWho);
        let dispTo = toWho.displayName || toWho.name;

        if (this.amountIsMultiplier) {
            healAmount = toWho.maxHp * healAmount;
        }
        let amount = toWho.heal(healAmount);
        let broadcastMsg = `${dispTo} was healed for ${amount}`;
        if (amount === 0) {
            toWho.message("You are already at full health!");
            return false;
        }
        toWho.message(`You were healed for ${amount} HP!`);
        toWho.broadcast(gm, broadcastMsg);
        this.tookEffect = true;
        return true;
    }
}

@registerEffect
export class BestowConditionEffect extends Effect {
    public condition: IConditionTemplate;
    public conditionOverride?: Partial<IConditionTemplate>;
    public effectOverrides?: any;
    public bestowerImmune: boolean;

    public constructor(args: IEffectTemplate) {
        super(args);
    }

    invoke(fromWho: Character, toWho: Character, gm: GameMaster): boolean {
        if(!this.args.condition) {
            throw(`Empty condition for ${this.name}`);
        }
        let condition = new Condition(this.condition, this.effectOverrides, this.conditionOverride);
        if(this.bestowerImmune) {
            condition.immuneFaction = fromWho.faction;
        }
        let dispTo = toWho.displayName || toWho.name;
        let dispCond = condition.displayName || condition.name;
        let broadcastMsg = `${dispTo} is affected by ${dispCond}`;
        if(fromWho !== toWho) fromWho.message(broadcastMsg);
        fromWho.broadcast(gm, broadcastMsg);
        toWho.message(`You are affected by ${dispCond}`);
        toWho.broadcast(gm, broadcastMsg);
        this.tookEffect = true;
        return toWho.receiveCondition(condition);
    }
}

@registerEffect
export class PushbackEffect extends Effect {

    public follow: boolean;

    invoke(fromWho: Character, toWho: Character, gm: GameMaster): boolean {
        let amount = this.numberAmount(fromWho);
        // Like PullEffect, negative 'amount' pushes the source away from
        // the target
        if(amount < 0) {
            amount = -amount;
            let tmp = fromWho;
            fromWho = toWho;
            toWho = tmp;
        }
        let vector = Point.subtract(fromWho.loc, toWho.loc).fastNormalized();

        for(let i=0; i < amount; i++) {
            let oldPos = Point.fromLoc(toWho.loc);
            let newPos = {x: oldPos.x + vector.x, y: oldPos.y + vector.y};
            let moved = gm.moveCharacter(toWho, newPos);
            if(!moved) return true;
            this.tookEffect = true;

            if(this.follow) {
                moved = gm.moveCharacter(fromWho, oldPos);
                if(!moved) return true;
            }
        }
        return true;
    }
}

@registerEffect
class SwapPlacesEffect extends Effect {
    invoke(fromWho: Character, toWho: Character, gm: GameMaster): boolean {
        // Turns out the only difference between regular move and forcemove
        // is that regular move prevents the swapping I want to do
        gm.forceMoveCharacter(fromWho, toWho.loc);
        return true;
    }
}

@registerEffect
class RawDamageEffect extends Effect {
    invoke(fromWho: Character, toWho: Character, gm: GameMaster): boolean {
        let amount = this.numberAmount(fromWho);
        gm.damageCharacter(fromWho, toWho, Dice.fixed(amount));
        this.tookEffect = true;
        return true;
    }
}

@registerEffect
class BurstDamageEffect extends Effect {
    public radius: number;
    public damage: string;
    public numTargets: number;
    public includeOrigin: boolean;
    public friendlyFire: boolean;

    constructor(template: IEffectTemplate) {
        super(template);
    }

    protected setDefaults() {
        super.setDefaults();
        this.numTargets = 0;
        this.includeOrigin = true;
        this.friendlyFire = false;
    }

    invoke(fromWho: Character, toWho: Character, gm: GameMaster): boolean {
        let candidates = [...gm.tilesInBurst(toWho.loc, this.radius)].filter(
            (tile: Tile) => {
                if(!tile.character) return false;
                if(!this.includeOrigin && Point.sameAs(toWho.loc, Point.fromLoc(tile))) return false;
                if(!this.friendlyFire && tile.character == fromWho) return false;
                if(this.immuneFaction === tile.character.faction) return false;

                return true;
            }
        ).map((tile: Tile) => tile.character);
        let chosen = [];
        if(this.numTargets == 0) {
            chosen = candidates;
        } else {
            while(chosen.length < this.numTargets && candidates.length > 0) {
                let idx = ROT.RNG.getUniformInt(0,candidates.length-1);
                chosen.push(candidates[idx]);
                candidates.splice(idx, 1);
            }
        }
        if(chosen.length > 0) {
            let parser = new DamageSpecParser();
            let dmg = parser.act(this.damage, fromWho, null, toWho, gm);
            for(let char of chosen) {
                gm.damageCharacter(fromWho, char, dmg);
                this.tookEffect = true;
            }
        }
        return false;
    }

}

@registerEffect
class PullEffect extends Effect {
    invoke(fromWho: Character, toWho: Character, gm: GameMaster): boolean {
        let amount = this.numberAmount(fromWho);
        // Negative 'amount' does an in-soviet-russia, pulling the
        // source to the target
        if(amount < 0) {
            amount = -amount;
            let tmp = fromWho;
            fromWho = toWho;
            toWho = tmp;
        }
        let vector = Point.subtract(fromWho.loc, toWho.loc).fastNormalized();

        for(let i=0; i < amount; i++) {
            let oldPos = Point.fromLoc(toWho.loc);
            let newPos = {x: oldPos.x - vector.x, y: oldPos.y - vector.y};
            let moved = gm.moveCharacter(toWho, newPos);
            if(!moved) return true;
            this.tookEffect = true;
        }
        return true;
    }
}

@registerEffect
class ZoneEffect extends Effect {
    public radius: number;
    public zonePassives: IEffectTemplate[];

    constructor(template: IEffectTemplate) {
        super(template);
    }

    invoke(fromWho: Character, toWho: Character, gm: GameMaster): boolean {
        let zone = new Zone({
            center: this.srcLoc,
            radius: this.radius,
            passiveEffects: this.zonePassives,
            faction: fromWho.faction,
        });
        gm.addZone(zone);
        return true;
    }
}

@registerEffect
class EnergyDamageEffect extends Effect {
    constructor(template: IEffectTemplate) {
        super(template);
    }

    invoke(fromWho: Character, toWho: Character, gm: GameMaster): boolean {
        toWho.energy -= this.numberAmount(fromWho);
        return true;
    }
}

@registerEffect
class ContagiousEffect extends Effect {
    public radius: number;

    constructor(template: IEffectTemplate) {
        super(template);
    }

    invoke(fromWho: Character, toWho: Character, gm: GameMaster): boolean {
        for(let tile of gm.tilesInBurst(toWho.loc, this.radius)) {
            let char = tile.character;
            if(!char || char === toWho) continue;
            if(this.immuneFaction === char.faction) continue;
            for(let condition of toWho.conditions) {
                let dispTo = toWho.displayName || toWho.name;
                let dispCond = condition.displayName || condition.name;
                let broadcastMsg = `${dispTo} is affected by ${dispCond}`;
                if(fromWho !== toWho) fromWho.message(broadcastMsg);
                fromWho.broadcast(gm, broadcastMsg);
                char.message(`You are affected by ${dispCond}`);
                char.broadcast(gm, broadcastMsg);
                this.tookEffect = true;
                char.receiveCondition(condition);
            }
        }

        return true;
    }
}

@registerEffect
class SpawnCreatureEffect extends Effect {
    creatures: ICharacterTemplate[];
    radius: number;

    invoke(fromWho: Character, toWho: Character, gm: GameMaster): boolean {
        let candidates = [...gm.tilesInBurst(toWho.loc, this.radius)].filter(
            (tile: Tile) => {
                return tile.canSpawnFriendly();
            }
        )

        let toSpawn = [...this.creatures];
        let spawned = 0;
        candidates = ROT.RNG.shuffle(candidates);
        while(toSpawn.length > 0 && candidates.length > 0) {
            let creature = new Character(toSpawn.shift());
            gm.spawner.prepareEnemy(creature);
            creature.faction = toWho.faction;
            if(toWho.faction === Faction.Player) {
                creature.broadcasts = true;
            }
            gm.spawner.spawnMonsterAt(creature, candidates.pop());
            spawned += 1;
        }
        return spawned > 0;
    }

    protected setDefaults() {
        super.setDefaults();
        this.radius = 1;
    }
}

@registerEffect
export class BestowTempHpEffect extends Effect {
    public invoke(fromWho: Character, toWho: Character, gm: GameMaster): boolean {
        let tempAmount = this.numberAmount(fromWho);
        toWho.grantTempHp(Dice.fixed(tempAmount));
        toWho.message(`You gained ${tempAmount} temporary HP!`);
        return true;
    }
}

@registerEffect
export class TeleportEffect extends Effect {
    invoke(fromWho: Character, toWho: Character, gm: GameMaster): boolean {
        gm.forceMoveCharacter(fromWho, this.srcLoc);
        return true;
    }
}

@registerEffect
export class TeleportOtherEffect extends Effect {
    invoke(fromWho: Character, toWho: Character, gm: GameMaster): boolean {
        let dest = gm.map.randomSpawnableTile();
        if(dest) {
            gm.forceMoveCharacter(toWho, dest);
        }
        return !!dest;
    }
}