import * as ROT from 'rot-js';

import {ICharacterTemplate} from "../interfaces";
import {Duration} from "../common";
import {CONDITIONS} from "../effects/condition";
// Colors from https://xkcd.com/color/rgb/
export const MONSTERS: { [key:string]: ICharacterTemplate } = {
    Kobold: {
        char: 'k',
        name: 'Kobold',
        baseMaxHp: 5,
        hpPerLevel: 1,
        description: "A scrappy lizard",
        fgcolor: ROT.Color.fromString("#42b395"), // Greeny blue
    },
    Elf: {
        char: 'e',
        name: 'Elf',
        baseMaxHp: 3,
        hpPerLevel: 2,
        description: "The old-school murdery kind of elf",
        behaviorName: 'DefaultRangedBehavior',
        fgcolor: ROT.Color.fromString("#c87606"), // dirty orange
    },
    Orc: {
        char: 'o',
        name: 'Orc',
        baseMaxHp: 8,
        hpPerLevel: 3,
        description: "It's an orc, pretty straightforward",
        fgcolor: ROT.Color.fromString("#048243"), // jungle green
        baseAc: 12,
    },
    Wolf: {
        char: 'w',
        name: 'Wolf',
        baseMaxHp: 3,
        hpPerLevel: 2,
        description: "Speedy but weak individually, wolves are deadly in packs",
        fgcolor: ROT.Color.fromString("#7f7053"), // grey brown
        baseMovementSpeed: 7,
    },
    Imp: {
        char: 'i',
        name: 'Imp',
        baseMaxHp: 1,
        hpPerLevel: 0,
        description: "Shoots homing bolts from afar",
        fgcolor: ROT.Color.fromString("#9f2305"), // burnt red
        behaviorName: 'DefaultRangedBehavior',
        abilityNames: ['ImpLightning'],
        defaultRangedAbilityName: 'ImpLightning',
    },
    Goblin: {
        char: 'g',
        name: 'Goblin',
        baseMaxHp: 1,
        hpPerLevel: 0,
        description: "Yuck, goblins",
        fgcolor: ROT.Color.fromString("#728639"),  // khaki green
        abilityNames: ['MinionStrike'],
        defaultMeleeAbilityName: 'MinionStrike',
    },
    Rat: {
        char: 'r',
        name: 'Rat',
        baseMaxHp: 1,
        hpPerLevel: 0,
        description: "Rats",
        fgcolor: ROT.Color.fromString("#653700"),  // brown
        abilityNames: ['MinionStrike'],
        defaultMeleeAbilityName: 'MinionStrike',
        baseMovementSpeed: 6,
    },
    // T2
    Spider: {
        char: 's',
        name: 'Spider',
        displayName: "Giant Spider",
        baseMaxHp: 5,
        hpPerLevel: 1,
        description: "Spits webs and tries to eat you",
        fgcolor: ROT.Color.fromString("#703be7"), // bluish purple
        abilityNames: ['MonsterStrike', 'Web'],  // BossBehavior ignores basic powers
        behaviorName: 'BossBehavior',
    },
    Hobgoblin: {
        char: 'h',
        name: 'Hobgoblin',
        baseMaxHp: 6,
        hpPerLevel: 2,
        description: "Yuck, bigger goblins",
        fgcolor: ROT.Color.fromString("#728639"),  // khaki green, same as goblin
        baseAc: 11,
    },
    Dwarf: {
        char: 'd',
        name: "Dwarf",
        baseMaxHp: 9,
        hpPerLevel: 4,
        description: "These are not friendly dwarves",
        fgcolor: ROT.Color.fromString("#137e6d"), // blue green
        baseAc: 12,
        baseMovementSpeed: 4,
    },
    DwarfArcher: {
        char: 'd',
        name: "DwarfArcher",
        displayName: "Dwarf Archer",
        baseMaxHp: 5,
        hpPerLevel: 2,
        description: "A dwarf with a crossbow",
        behaviorName: 'DefaultRangedBehavior',
        fgcolor: ROT.Color.fromString("#3f9b0b"), // grass green
        baseMovementSpeed: 4,
    },
    DwarfWizard: {
        char: 'd',
        name: "DwarfWizard",
        displayName: "Dwarf Wizard",
        baseMaxHp: 1,
        hpPerLevel: 1,
        description: "Oh no who taught them magic!?",
        abilityNames: ['Fright', 'ArcaneBolt', 'Sleep'],
        behaviorName: 'BossBehavior',
        fgcolor: ROT.Color.fromString("#0652ff"), // electric blue
        baseMovementSpeed: 4,
    },
    Cultist: {
        char: "c",
        name: "Cultist",
        baseMaxHp: 6,
        hpPerLevel: 0,
        description: "Worshippers of the Labyrinth itself",
        abilityNames: ['BasicHeal', 'MonsterRanged'],
        behaviorName: 'BossBehavior',
        fgcolor: ROT.Color.fromString("#db4bda"), // pink purple
    },
    Ogre: {
        char: 'O',
        name: 'Ogre',
        baseMaxHp: 12,
        hpPerLevel: 3,
        description: "As big as it is ugly, and it's very ugly",
        fgcolor: ROT.Color.fromString("#9c6d57"), // brownish
        baseAc: 13,
        baseMovementSpeed: 2,
    },
    OgreMage: {
        char: 'O',
        name: 'OgreMage',
        displayName: "Ogre Mage",
        baseMaxHp: 9,
        hpPerLevel: 3,
        description: "As smart as it is ugly, and it's very ugly",
        fgcolor: ROT.Color.fromString("#0652ff"), // electric blue
        baseAc: 11,
        abilityNames: ['BeamOfImpedance', 'Haste', 'ZoneOfTorpor'],
        behaviorName: 'BossBehavior',
        baseMovementSpeed: 2,
    },
    // T3
    Troll: {
        char: 'T',
        name: 'Troll',
        baseMaxHp: 10,
        hpPerLevel: 3,
        description: "Scrawls offensive writings on the walls",
        fgcolor: ROT.Color.fromString("#96f97b"), // light green
        baseAc: 11,
        conditions: [
            {
                name: 'TrollRegen',
                beginningOfTurnTemplates: [
                    {
                        amount: 1,
                        name: 'HealingEffect',
                    }
                ],
                duration: Duration.AlwaysOn,
            }
        ]
    },
    Werewolf: {
        char: 'W',
        name: 'Werewolf',
        baseMaxHp: 9,
        hpPerLevel: 3,
        description: "There wolf!",
        fgcolor: ROT.Color.fromString("#7f7053"), // grey brown (same as wolves)
        baseMovementSpeed: 7,
    },
    Griffin: {
        char: 'f',
        name: 'Griffin',
        baseMaxHp: 4,
        hpPerLevel: 3,
        description: "How can it fly in here!?",
        fgcolor: ROT.Color.fromString("#f5bf03"), // golden
        baseMovementSpeed: 8,
    },
    Giant: {
        char: 'G',
        name: "Giant",
        baseMaxHp: 14,
        hpPerLevel: 4,
        description: "'Giant' is an understatement",
        fgcolor: ROT.Color.fromString("#a0025c"), // deep magenta
        baseAc: 14,
        baseMovementSpeed: 1,
        conditions: [
            {
                name: 'GiantSwing',
                passiveEffectTemplates: [{
                    name: "InflictionEffect",
                    inflictedCondition: CONDITIONS.Prone,
                }],
                duration: Duration.AlwaysOn,
            }
        ]
    },
    Vampire: {
        char: 'V',
        name: 'Vampire',
        baseMaxHp: 8,
        hpPerLevel: 1,
        description: "The world is a vampire",
        fgcolor: ROT.Color.fromString("#770001"), // blood, of course
        baseAc: 13,
        baseMovementSpeed: 6,
        conditions: [ CONDITIONS.Vampirism ],
    }
}