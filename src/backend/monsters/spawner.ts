import * as ROT from 'rot-js';

import {Energy, Targets} from "../common";
import {Character} from "../character";
import {MONSTERS} from "./nonboss";
import {Faction, SpawnTarget, Weightable, XY} from "../interfaces";
import {BossBehavior, MeleeCombatBehavior} from "../behavior";
import {BOSSES, CLASSES} from "../classes";
import {applyVariant, VARIANTS} from "./variants";
import {ABILITIES} from "../abilities/ability";

const T1_MONSTERS = {
    "Kobold": 50,
    "Goblin": 45,
    "Rat": 35,
    "Orc": 30,
    "Wolf": 30,
    "Spider": 20,
    "Elf": 10,
    "Imp": 15,
}

const T2_MONSTERS = {
    "Hobgoblin": 30,
    "Dwarf": 25,
    "DwarfArcher": 10,
    "DwarfWizard": 8,
    "Cultist": 10,
    "Ogre": 15,
    "OgreMage": 5,
}

const T3_MONSTERS = {
    "Troll": 20,
    "Werewolf": 25,
    "Griffin": 20,
    "Giant": 5,
    "Vampire": 15,
}

export const ALL_MONSTER_SPAWNS = {
    ...T1_MONSTERS,
    ...T2_MONSTERS,
    ...T3_MONSTERS,
};

export const SpawnPoolByTier = [
    T1_MONSTERS,
    T2_MONSTERS,
    T3_MONSTERS,
]

const VARIANT_POOL = {
    "Zombie": 50,
    "BossFighter": 50,
    "BossWizard": 50,
    "BossCleric": 50,
    "BossRogue": 50,
}

export class Spawner {

    public tickNum: number;
    public tierNum: number;
    public spawnLimit: number;
    public spawnPeriod: number;
    public variantChance: number;

    public spawnPool: Weightable;
    public variantPool: Weightable;

    constructor(protected target:SpawnTarget, targetTier: number) {
        this.tickNum = 0;
        this.variantChance = 0.15;
        this.spawnPool = T1_MONSTERS;
        this.variantPool = VARIANT_POOL;
        this.spawnLimit = 15;
        this.spawnPeriod = Energy.TURN;
        this.tierNum = 1;
        this.tierUpTo(targetTier);
    }

    public adoptNextTier(): boolean {
        let nextTierNum = this.tierNum + 1;
        let nextMonsters = SpawnPoolByTier[nextTierNum - 1];
        let nextPool = {...this.spawnPool};
        if(nextMonsters) {
            Object.assign(nextPool, nextMonsters);
        }
        this.spawnPool = nextPool;
        this.tierNum = nextTierNum;
        return true;
    }

    protected tierUpTo(targetTier: number) {
        while(this.tierNum < targetTier) {
            this.adoptNextTier();
        }
    }

    public createMonster(): Character {
        let name = ROT.RNG.getWeightedValue(this.spawnPool);
        let template = Object.assign({}, MONSTERS[name]);

        let chooseVariant = ROT.RNG.getUniform() < this.variantChance;
        if(chooseVariant) {
            let variantName = ROT.RNG.getWeightedValue(this.variantPool);
            let variantTemplate = VARIANTS[variantName];

            if(variantTemplate.variantBossName) {
                if(!this.target.canChallengeBoss(variantTemplate.variantBossName)) {
                    variantTemplate = null;
                }
            }
            if(variantTemplate) {
                template = applyVariant(template, variantTemplate);
            }
        }

        if(!template.behaviorName) {
            template.behaviorName = "MeleeCombatBehavior";
        }

        let monster = new Character(template);
        if(monster.behaviorName === 'BossBehavior') {
            // Boss behavior means default attacks may not exist,
            // and won't be picked even if they do.  But we need a fallback:
            monster.gainAbility("BasicMeleeAttack");
            monster.gainAbility("BasicRangedAttack");
        }
        return this.prepareEnemy(monster);
    }

    public createBoss(named: string): Character {
        let templateName = BOSSES[named];
        if(!templateName) throw(`No boss named ${named}`);
        let template = CLASSES[templateName];
        if(!template) throw(`No class named ${templateName}`);
        let result = new Character(template);
        result.name = named;
        this.prepareBoss(result);
        return this.prepareEnemy(result);
    }

    public prepareBoss(monster: Character): Character {
        for(let abilityName of monster.classAbilityNames) {
            let proposed = ABILITIES.newFromName(abilityName);
            if(proposed.tier <= this.tierNum) {
                let ability = monster.gainAbility(abilityName);
                if (ability.isMelee()) {
                    monster.assignBetterMelee(ability);
                } else if (ability.isRanged()) {
                    monster.assignBetterRanged(ability);
                }
            }
        }
        monster.setBehavior(new BossBehavior());
        return monster;
    }

    public prepareEnemy(monster: Character): Character {
        // Factionize correctly
        monster.faction = Faction.Enemy;

        // Level up to match the player
        while(monster.level < this.target.player.level) {
            monster.levelUp(1);
        }

        // roll initiative
        let init = ROT.RNG.getUniformInt(1, 10);
        monster.energy = -init;
        return monster;
    }

    public createAndSpawnMonster(force = false): Character {
        let alreadySpawned = this.target.spawnCount;
        if(!force && alreadySpawned >= this.spawnLimit) return null;

        let queryable = this.target.map;
        let loc = queryable.randomSpawnableTile();
        if(!loc) return null;

        let monster = this.createMonster();

        return this.spawnMonsterAt(monster, loc);
    }

    public spawnMonsterAt(monster: Character, where: XY): Character {
        monster.loc = where;
        if(!monster.behavior) {
            monster.setBehavior(new MeleeCombatBehavior());  // gm set by addCharacter
        }
        this.target.addCharacter(monster);
        return monster;
    }

    public tick(numTurns = 1): number {
        let started = this.tickNum;
        while(numTurns > 0) {
            numTurns -= 1;
            this.tickNum += 1;

            if(this.tickNum % this.spawnPeriod === 0) {
                this.createAndSpawnMonster();
            }
        }
        return this.tickNum - started;
    }
}